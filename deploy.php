<?php
namespace Deployer;

use Deployer\Task\Context;

require 'recipe/laravel.php';
require 'recipe/rollbar.php';

set('build_path', '.build');
set('build_archive', '.build.tgz');

set('default_stage', 'local');

// Project name
set('application', 'zelkys');

// Project repository
set('repository', 'git@github.com:erik-ropez/smart-promo.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', [
    'resources/assets/fonts/dompdf',
]);

set('allow_anonymous_stats', false);

// Roolbar

set('rollbar_token', '6dc6cc01df2142bc8ea48f8d9c6d24c9');
set('rollbar_username', '');

// Hosts

localhost()
->stage('local');

host('zelkys-admin.letstalkremote.com')
    ->stage('development')
    ->set('deploy_path', '~/public_html')
    ->user('zelkysadmin');

host('ec2-18-218-242-60.us-east-2.compute.amazonaws.com')
    ->stage('production')
    ->set('deploy_path', '/var/www/backend-admin.zelkys.com')
    ->user('ubuntu');

// Tasks

task('build:git', function () {
    run('rm -rf {{build_path}}');
    run('mkdir {{build_path}}');
    run('git archive --format=tar --prefix={{build_path}}/ HEAD | tar xf -');
})->local();

task('build:composer', function () {
    run('cd {{build_path}} && composer install'); // --no-dev
})->local();

task('build:npm', function () {
    $stage = 'local';
    if (input()->hasArgument('stage')) {
        $stage = input()->getArgument('stage');
    }
    run("cd {{build_path}} && npm install && MIX_APP_ENV=$stage npm run prod && rm -rf node_modules");
})->local();

task('build:compress', function () {
    run('tar -zcvf {{build_archive}} {{build_path}}');
    run('rm -rf {{build_path}}');
})->local();

task('build', ['build:git', 'build:composer', 'build:npm', 'build:compress']);

task('builoy', ['build', 'deploy']);

task('deploy:update_code', function () {
    $host = Context::get()->getHost();
    $hostName = $host->getHostname();
    $user = $host->getUser();
    $port = $host->getPort();
    $port = $port ? $port : 22;
    runLocally("scp -P $port {{build_archive}} $user@$hostName:{{release_path}}", ['timeout' => 600]);
    run('tar --strip-components=1 -zxf {{release_path}}/{{build_archive}} -C {{release_path}}');
    run('rm {{release_path}}/{{build_archive}}');
    run('rm {{release_path}}/{{build_path}} -rf');
});

task('deploy:vendors', function () {});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

// Cache routes after config cache.

after('artisan:config:cache', 'artisan:route:cache');

// Rollbar

after('deploy', 'rollbar:notify');
