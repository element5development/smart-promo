<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Tymon\JWTAuth\Http\Parser\AuthHeaders;
use Tymon\JWTAuth\Http\Parser\QueryString;
use Tymon\JWTAuth\Http\Parser\InputSource;
use Tymon\JWTAuth\Http\Parser\RouteParams;
use Tymon\JWTAuth\Http\Parser\Cookies;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $headerParser = new AuthHeaders;
        $headerParser->setHeaderName('X-Authorization');

        $this->app['tymon.jwt.parser']->setChain([
            $headerParser,
            new QueryString,
            new InputSource,
            new RouteParams,
            new Cookies(config('jwt.decrypt_cookies')),
        ]);
    }
}
