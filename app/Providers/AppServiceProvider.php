<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Voucher;
use App\Card;
use App\Setting;
use App\Capability;
use App;
use Config;
use App\Services\Cards\CardProviderInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configSettings();

        $this->configCapabilities();

        Schema::defaultStringLength(191);

        Relation::morphMap([
            'voucher' => Voucher::class,
            'card' => Card::class,
        ]);
    }

    private function configSettings()
    {
        if (App::runningInConsole()) {
            try {
                foreach (Setting::all() as $key => $setting) {
                    Config::set('settings.'.$setting->key, $setting->value);
                }
            } catch (\Exception $e) {}
        }
    }

    private function configCapabilities()
    {
        try {
            foreach (Capability::enabled()->pluck('key') as $key) {
                Config::set("capabilities.{$key}", true);
            }
        } catch (\Exception $e) {}
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CardProviderInterface::class, config('services.card_provider.class'));
    }
}
