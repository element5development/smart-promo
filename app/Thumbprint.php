<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Thumbprint extends Model
{
    use CrudTrait;

    protected $fillable = ['uri', 'key', 'secret', 'thumbprint', 'reserved_at', 'use_count'];
}
