<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class NewAdminNotification extends Notification
{
    public $password;

    public function __construct($password)
    {
        $this->password = $password;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Your new admin account on ' . config('app.name'))
            ->greeting('Hello!')
            ->line('Here’s the details for your new admin account on ' . config('app.name') . ':')
            ->line("Username: {$notifiable->email}")
            ->line("Password: {$this->password}")
            ->line("Please finalize your login here:")
            ->action('Go to admin panel', route('backpack'))
            ->line('We look forward to working with you.');
    }
}
