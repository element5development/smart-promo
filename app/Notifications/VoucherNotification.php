<?php

namespace App\Notifications;

use Image;

class VoucherNotification extends ContentNotification
{
    public $voucher;
    public $code;

    public function __construct($voucher, $code)
    {
        $this->voucher = $voucher;
        $this->code = $code;
    }

    public function toMail($notifiable)
    {
        $this->data['voucher_number'] = $this->voucher->number;
        $this->data['voucher_code'] = $this->code;
        $this->data['voucher_pdf'] = $this->voucher->pdfUrl();
        $this->data['amount'] = number_format($this->voucher->cost(), 2);
        $this->data['first_name'] = $notifiable->first_name;
        $this->data['last_name'] = $notifiable->last_name;
        $this->data['email'] = $notifiable->email;
        
        $this->data['voucher_qr_code'] = $this->voucher->qrCodePath();

        $mailMessage = $this->contentMessage(
            $notifiable,
            config('settings.voucher_email_subject'),
            config('settings.voucher_email_template')
        );
        
        return $mailMessage->attach($this->voucher->pdfPath(), ['as' => 'Voucher.pdf']);
    }
}
