<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class PromotionNotification extends ContentNotification
{
    protected $promotion;

    protected $type;

    protected $specific_channel;

    public function __construct($promotion, $preliminary = false, $specific_channel = null)
    {
        $this->promotion = $promotion;
        $this->type = $preliminary ? 'preliminary' : 'promoted';
        $this->specific_channel = $specific_channel;
    }

    public function via($notifiable)
    {
        $channels = ['database'];

        if ($this->promotion->{"{$this->type}_email_subject"} && $this->promotion->{"{$this->type}_email_template"}) {
            $channels[] = 'mail';
        }

        if ($this->promotion->{"{$this->type}_sms_template"}) {
            $channels[] = TwilioChannel::class;
        }

        if ($this->specific_channel) {
            if (in_array($this->specific_channel, $channels)) {
                return [$this->specific_channel];
            } else {
                return [];
            }
        }

        return $channels;
    }

    public function toMail($notifiable)
    {
        $this->setData($notifiable);

        return $this->contentMessage(
            $notifiable,
            $this->promotion->{"{$this->type}_email_subject"},
            $this->promotion->{"{$this->type}_email_template"}
        );
    }

    public function toTwilio($notifiable)
    {
        $this->setData($notifiable);

        $content = view(['template' => $this->promotion->{"{$this->type}_sms_template"}], $this->data)->render();

        $content = html_entity_decode($content, ENT_QUOTES | ENT_HTML401);

        return (new TwilioSmsMessage())->content($content);
    }

    private function setData($notifiable)
    {
        $this->data['first_name'] = $notifiable->user->first_name;
        $this->data['last_name'] = $notifiable->user->last_name;
        $this->data['card_number'] = $notifiable->barcode;
    }

    public function toArray($notifiable)
    {
        return [];
    }

    public function toDatabase($notifiable)
    {
        return [
            'promotion_id' => $this->promotion->id,
        ];
    }
}
