<?php

namespace App\Notifications;

class OptinNotification extends ContentNotification
{
    public function toMail($notifiable)
    {
        $this->data['optin_url'] = route('optin', $notifiable->verification_token);

        return $this->contentMessage(
            $notifiable,
            config('settings.optin_email_subject'),
            config('settings.optin_email_template')
        );
    }
}
