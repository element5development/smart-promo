<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Html2Text\Html2Text;
use App\Notifications\MessageMock;

class ContentNotification extends Notification
{
    protected $data = [];

    public function via($notifiable)
    {
        return ['mail'];
    }

    protected function contentMessage($notifiable, $subject, $template)
    {
        $htmlTemplate = $this->getHtmlTemplate($template);

        $plainText = $this->getPlainText($template);

        $layoutTemplate = config('settings.email_layout_template');

        $email = $notifiable->email;

        return (new MailMessage)
            ->subject($subject)
            ->view(
                [['template' => $htmlTemplate], ['template' => '{!! $plainText !!}']],
                array_merge($this->data, compact('layoutTemplate', 'plainText', 'email'))
            );
    }

    private function getHtmlTemplate($template)
    {
        $htmlTemplate = <<<EOD
@extends(['template' => \$layoutTemplate])
@section('content')
{$template}
@endsection
EOD;

        return $htmlTemplate;
    }

    private function getPlainText($template)
    {
        $message = new MessageMock;

        $content = view(['template' => $template], array_merge($this->data, compact('message')))->render();

        libxml_use_internal_errors(true);
        $plainText = Html2Text::convert($content);
        libxml_use_internal_errors(false);

        return $plainText;
    }
}
