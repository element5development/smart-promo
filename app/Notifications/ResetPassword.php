<?php

namespace App\Notifications;

class ResetPassword extends ContentNotification
{
    public $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function toMail($notifiable)
    {
        $this->data['first_name'] = $notifiable->first_name;
        $this->data['last_name'] = $notifiable->last_name;
        $this->data['reset_url'] = empty(config('settings.email_password_url')) ?
            $this->defaultVerificationUrl() :
            $this->customVerificationUrl($notifiable);

        return $this->contentMessage(
            $notifiable,
            config('settings.password_email_subject'),
            config('settings.password_email_template')
        );
    }

    private function defaultVerificationUrl()
    {
        return url(config('app.url') . route('password.reset', $this->token, false));
    }

    private function customVerificationUrl($notifiable)
    {
        return sprintf(config('settings.email_password_url'), $this->token);
    }
}
