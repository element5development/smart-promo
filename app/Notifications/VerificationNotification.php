<?php

namespace App\Notifications;

class VerificationNotification extends ContentNotification
{
    public function toMail($notifiable)
    {
        $this->data['user'] = $notifiable;

        $this->data['verification_url'] = empty(config('settings.email_verification_url')) ?
            $this->defaultVerificationUrl($notifiable) :
            $this->customVerificationUrl($notifiable);

        $this->data['first_name'] = $notifiable->first_name;
        $this->data['last_name'] = $notifiable->last_name;

        return $this->contentMessage(
            $notifiable,
            config('settings.verification_email_subject'),
            config('settings.verification_email_template')
        );
    }

    private function defaultVerificationUrl($notifiable)
    {
        return route('email-verification.check', $notifiable->verification_token) . '?email=' . urlencode($notifiable->email);
    }

    private function customVerificationUrl($notifiable)
    {
        $url = sprintf(config('settings.email_verification_url'), $notifiable->verification_token);
        $url .= strpos($url, '?') ? '&' : '?';
        $url .= 'email=' . urlencode($notifiable->email);
        return $url;
    }
}
