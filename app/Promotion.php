<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model
{
    use SoftDeletes;
    use CrudTrait;

    const FREQUENCY_TYPE = 'frequency';
    const TIME_SINCE_LAST_VISIT_TYPE = 'time_since_last_visit';
    const SPECIFIC_DATE_TYPE = 'specific_date';
    const BIRTHDAY_TYPE = 'birthday';

    const Types = [self::FREQUENCY_TYPE, self::TIME_SINCE_LAST_VISIT_TYPE, self::SPECIFIC_DATE_TYPE, self::BIRTHDAY_TYPE];
    const FrequencyRanges = ['day', 'week', 'month'];
    const TimeRanges = ['day', 'week', 'month'];

    protected $fillable = [
        'name', 'description', 'type', 'type_options', 'product_id',
        'start_at', 'expire_at', 'valid_for_range', 'valid_for',
        'preliminary_email_subject', 'preliminary_email_template', 'preliminary_sms_template',
        'preliminary_days',
        'promoted_email_subject', 'promoted_email_template', 'promoted_sms_template',
        'frequency_range', 'frequency_period', 'frequency', 'time_range', 'time_since_last_visit',
        'specific_month', 'specific_day', 'birthday_start_date', 'birthday_end_date',
        'email_before_range', 'email_before',
    ];

    protected $dates = ['start_at', 'expire_at', 'deleted_at'];

    protected $casts = [
        'type_options' => 'array',
    ];

    protected $typeOptions = [
        'frequency_range', 'frequency_period', 'frequency', 'time_range', 'time_since_last_visit',
        'specific_month', 'specific_day', 'birthday_start_date', 'birthday_end_date',
        'preliminary_days',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_promotions')
            ->withPivot('user_id', 'promotion_id', 'card_id', 'promoted_at', 'promoted_balance', 'expire_at', 'is_expired')
            ->withTimestamps();
    }

    public function getAttribute($attribute)
    {
        if (in_array($attribute, $this->typeOptions)) {
            return isset($this->type_options[$attribute]) ? $this->type_options[$attribute] : null;
        }
        return parent::getAttribute($attribute);
    }

    public function setAttribute($attribute, $value)
    {
        if (in_array($attribute, $this->typeOptions)) {
            $type_options = $this->type_options;
            $type_options[$attribute] = $value;
            $this->type_options = $type_options;
        } else {
            parent::setAttribute($attribute, $value);
        }
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    
    public function productName()
    {
        return $this->product ? $this->product->product_name : null;
    }

    public function typeTitle()
    {
        return trans('promotion.' . $this->type);
    }

    public function validForDate($fromDate)
    {
        return (new Carbon($fromDate))->modify('+' . $this->valid_for . ' ' . $this->valid_for_range);
    }

    public function timeSinceDate($fromDate)
    {
        return (new Carbon($fromDate))->modify('-' . $this->type_options['time_since_last_visit'] . ' ' . $this->type_options['time_range']);
    }

    public function frequencyFromDate($fromDate)
    {
        return (new Carbon($fromDate))->modify('-' . $this->type_options['frequency_period'] . ' ' . $this->type_options['frequency_range']);
    }
}
