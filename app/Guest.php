<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class Guest extends Model
{
    use Notifiable;

    const EMBED_PROPERTIES = [
        'holder_title', 'holder_firstname', 'holder_surname', 'address1', 'address2', 'address3',
        'postal_code', 'mobile_phone', 'phone', 'email', 'dob',
        'registered_date', 'member_since', 'last_modified'
    ];

    protected $fillable = ['guest_id', 'location_id', 'optin_sent_at',
        'holder_title', 'holder_firstname', 'holder_surname', 'address1', 'address2', 'address3',
        'postal_code', 'mobile_phone', 'phone', 'email', 'dob',
        'registered_date', 'member_since', 'last_modified'
    ];

    protected $dates = ['optin_sent_at'];

    public function cards()
    {
        return $this->belongsToMany(Card::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function saveVerificationToken()
    {
        if (empty($this->verification_token)) {
            $this->verification_token = hash_hmac('sha256', Str::random(40), config('app.key'));
            $this->save();
        }
    }
}
