<?php

namespace App\Http\Requests\Admin;

use App\Services\Cards\CardProviderInterface;

class VoucherClaimRequest extends FormRequest
{
    protected $cardProvider;

    public function __construct(CardProviderInterface $cardProvider)
    {
        $this->cardProvider = $cardProvider;
    }

    public function rules()
    {
        return [
            'code' => 'required|digits:6',
            'card_number' => array_merge(['required'], $this->cardProvider->cardNumberValidationRules()),
            'package_id' => 'required|array',
            'package_id.*' => 'required|exists:voucher_packages,id',
        ];
    }
}
