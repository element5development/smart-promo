<?php

namespace App\Http\Requests\Admin;

use App\Rules\ProductArray;
use App\Rules\EqualProducts;

class PackageCrudRequest extends FormRequest
{
    public function rules()
    {
        $rules = ['title' => 'required'];

        if (config('smartpromo.location_at_purchase')) {
            $rules['products'] = ['array', 'required', new ProductArray, new EqualProducts];
        } else {
            $rules['products'] = 'required|exists:products,id';
        }

        return $rules;
    }
}
