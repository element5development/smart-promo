<?php

namespace App\Http\Requests\Admin;

use Backpack\PermissionManager\app\Http\Requests\UserStoreCrudRequest;
use Auth;

class AdminStoreCrudRequest extends UserStoreCrudRequest
{
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }
}
