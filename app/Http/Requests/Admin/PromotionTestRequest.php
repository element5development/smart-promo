<?php

namespace App\Http\Requests\Admin;

class PromotionTestRequest extends FormRequest
{
    public function rules()
    {
        return [
            'type' => 'required',
            'email' => 'nullable|required_if:type,preliminary_email,promoted_email|string|email|max:255',
            'phone' => 'nullable|required_if:type,preliminary_sms,promoted_sms|string|max:255',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
        ];
    }
}
