<?php

namespace App\Http\Requests\Admin;

class LocationCrudRequest extends FormRequest
{
    public function rules()
    {
        return [
            'title' => 'required',
            'uri' => 'required|url',
            'key' => 'required',
            'secret' => 'required',
            'thumbprints_json' => 'required',
        ];
    }
}
