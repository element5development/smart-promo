<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest as FormRequestBase;
use Auth;

class FormRequest extends FormRequestBase
{
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }
}
