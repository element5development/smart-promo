<?php

namespace App\Http\Requests\Admin;

use Backpack\Settings\app\Http\Requests\SettingRequest as BaseSettingRequest;
use Auth;

class SettingRequest extends BaseSettingRequest
{
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }
}
