<?php

namespace App\Http\Requests\Admin;

use Illuminate\Http\Request;

class PromotionUpdateCrudRequest extends PromotionStoreCrudRequest
{
    public function rules(Request $request)
    {
        $rules = parent::rules($request);
        unset($rules['type']);
        return $rules;
    }
}
