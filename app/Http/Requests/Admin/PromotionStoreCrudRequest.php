<?php

namespace App\Http\Requests\Admin;

use Illuminate\Validation\Rule;
use App\Promotion;
use Illuminate\Http\Request;

class PromotionStoreCrudRequest extends FormRequest
{
    public function rules(Request $request)
    {
        $rules = [
            'name' => 'required',
            'description' => '',
            'product_id' => 'required|exists:products,id',
            'start_at' => 'nullable|date',
            'expire_at' => 'nullable|date',
            'valid_for_range' => ['required', Rule::in(Promotion::TimeRanges)],
            'valid_for' => 'required|numeric|min:1',
            'promoted_email_subject' => 'required_without:promoted_sms_template|required_with:promoted_email_template',
            'promoted_email_template' => 'required_without:promoted_sms_template|required_with:promoted_email_subject',
            'promoted_sms_template' => 'required_without_all:promoted_email_subject,promoted_email_template',
            'type' => ['required', Rule::in(Promotion::Types)],
        ];

        switch ($request->get('type')) {
            case 'frequency':
                $rules['frequency_range'] = ['required', Rule::in(Promotion::FrequencyRanges)];
                $rules['frequency_period'] = 'required_if:type,frequency|numeric|min:1';
                $rules['frequency'] = 'required_if:type,frequency|numeric|min:1';
                break;
            case 'time_since_last_visit':
                $rules['time_range'] = ['required', Rule::in(Promotion::TimeRanges)];
                $rules['time_since_last_visit'] = 'required|numeric|min:1';
                break;
            case 'specific_date':
                $rules['specific_month'] = 'required|numeric|min:1|max:12';
                $rules['specific_day'] = 'required|numeric|min:1|max:31';
                $this->addPreliminaryFields($rules);
                break;
            case 'birthday':
                $rules['birthday_start_date'] = 'required|date';
                $rules['birthday_end_date'] = 'required|date';
                $this->addPreliminaryFields($rules);
                break;
        }
        
        return $rules;
    }

    private function addPreliminaryFields(&$rules)
    {
        $rules['preliminary_days'] = 'nullable|numeric|min:1';
        if (!empty($this->get('preliminary_days'))) {
            $rules['preliminary_email_subject'] = 'required_without:preliminary_sms_template|required_with:preliminary_email_template';
            $rules['preliminary_email_template'] = 'required_without:preliminary_sms_template|required_with:preliminary_email_subject';
            $rules['preliminary_sms_template'] = 'required_without_all:preliminary_email_subject,preliminary_email_template';
        }
    }
}
