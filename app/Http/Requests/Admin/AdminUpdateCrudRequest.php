<?php

namespace App\Http\Requests\Admin;

use Backpack\PermissionManager\app\Http\Requests\UserUpdateCrudRequest;
use Auth;

class AdminUpdateCrudRequest extends UserUpdateCrudRequest
{
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }
}
