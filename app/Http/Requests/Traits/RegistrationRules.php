<?php

namespace App\Http\Requests\Traits;

use App\Rules\Password as PasswordRule;
use Illuminate\Validation\Rule;

trait RegistrationRules
{
    public function registrationRules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                'confirmed',
                Rule::unique('users')->ignore(true, 'is_shadow'),
            ],
            'password' => ['required', 'confirmed', new PasswordRule],
            'birthdate' => 'required|date',
            'gender' => 'required|in:m,f'
        ];
    }
}
