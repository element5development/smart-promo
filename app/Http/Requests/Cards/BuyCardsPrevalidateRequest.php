<?php

namespace App\Http\Requests\Cards;

use App\Rules\PackageArray;
use Illuminate\Http\Request;
use App\Http\Requests\PaymentRequest;

class BuyCardsPrevalidateRequest extends PaymentRequest
{
    public function rules(Request $request)
    {
        $rules = array_merge(parent::rules($request), [
            'package' => ['array', 'required', new PackageArray],
            'package.*' => 'required|numeric|min:0',
        ]);

        if (config('smartpromo.location_at_purchase')) {
            $rules['location_id'] = 'required|exists:locations,id';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'package.required' => 'At least one package is required',
        ];
    }
}
