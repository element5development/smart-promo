<?php

namespace App\Http\Requests\Cards;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Requests\AuthFormRequest;

class AddCardRequest extends AuthFormRequest
{
    public function rules(Request $request)
    {
        return [
            'number' => [
                'required',
                Rule::unique('cards')->where(function ($query) use ($request) {
                    return $query
                        ->whereNotNull('user_id')
                        ->where('user_id', '!=', $request->user()->id);
                })
            ],
            'cvv' => 'required',
        ];
    }
}
