<?php

namespace App\Http\Requests\Cards;

use Illuminate\Http\Request;
use App\Http\Requests\Traits\RegistrationRules;
use Illuminate\Validation\Rule;

class BuyCardsRequest extends BuyCardsPrevalidateRequest
{
    use RegistrationRules;

    public function rules(Request $request)
    {
        $rules = parent::rules($request);

        if (!$request->user()) {
            if (config('smartpromo.register_with_purchase')) {
                $rules = array_merge($rules, $this->registrationRules());
            } else {
                $rules['email'] = 'required|string|email|max:255';
            }
        }

        return $rules;
    }
}
