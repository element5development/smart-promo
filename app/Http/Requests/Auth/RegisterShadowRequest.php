<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\RegistrationRules;

class RegisterShadowRequest extends FormRequest
{
    use RegistrationRules;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = $this->registrationRules();
        
        unset($rules['email']);
        unset($rules['first_name']);
        unset($rules['last_name']);
        
        $rules = array_merge($rules, [
            'shadow_token' => 'required',
            'phone' => 'required',
            'preferred_location_id' => 'required|exists:locations,id',
        ]);

        return $rules;
    }
}
