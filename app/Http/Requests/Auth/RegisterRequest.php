<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\RegistrationRules;

class RegisterRequest extends FormRequest
{
    use RegistrationRules;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return $this->registrationRules();
    }
}
