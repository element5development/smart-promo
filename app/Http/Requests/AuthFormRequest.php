<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest as FormRequestBase;
use Auth;

class AuthFormRequest extends FormRequestBase
{
    public function authorize()
    {
        return Auth::check();
    }
}
