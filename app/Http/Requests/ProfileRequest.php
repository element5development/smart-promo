<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Http\Requests\Traits\RegistrationRules;

class ProfileRequest extends FormRequest
{
    use RegistrationRules;

    public function authorize()
    {
        return true;
    }

    public function rules(Request $request)
    {
        $user = $request->user();

        $rules = $this->registrationRules();
        
        unset($rules['password']);
        
        $rules = array_merge($rules, [
            'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
            'preferred_location_id' => 'nullable|exists:locations,id',
        ]);

        return $rules;
    }
}
