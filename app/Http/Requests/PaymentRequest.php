<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PaymentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(Request $request)
    {
        return [
            'cc_number' => 'required|digits_between:4,16',
            'cc_expiration_year' => 'required|digits:4',
            'cc_expiration_month' => 'required|numeric|min:1|max:12|digits_between:1,2',
            'cc_code' => 'required|digits:3',
            'cc_first_name' => 'required',
            'cc_last_name' => 'required',
            'billing_address' => 'required',
            'billing_city' => 'required',
            'billing_state' => 'required',
            'billing_zip' => 'required',
            'billing_country' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'cc_number' => 'number',
            'cc_expiration_year' => 'year',
            'cc_expiration_month' => 'month',
            'cc_code' => 'cvv',
            'cc_first_name' => 'first name',
            'cc_last_name' => 'last name',
        ];
    }
}
