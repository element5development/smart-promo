<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Backpack\Base\app\Http\Middleware\Admin as BaseAdmin;

class Admin extends BaseAdmin
{
    public function handle($request, Closure $next, $guard = 'admin')
    {
        config(['auth.defaults.guard' => $guard]);
        return parent::handle($request, $next, $guard);
    }
}
