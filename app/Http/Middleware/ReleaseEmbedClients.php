<?php

namespace App\Http\Middleware;

use Closure;
use App\Lib\Embed\EmbedWrapper;

class ReleaseEmbedClients
{
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate()
    {
        EmbedWrapper::releaseAllClients();
    }
}
