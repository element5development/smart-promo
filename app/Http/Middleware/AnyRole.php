<?php

namespace App\Http\Middleware;

use Closure;

class AnyRole
{
    public function handle($request, Closure $next, ...$roles)
    {
        if (!$request->user()->hasAnyRole($roles)) {
            return redirect()->route('backpack.dashboard');
        }
        return $next($request);
    }
}
