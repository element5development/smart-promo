<?php

namespace App\Http\Middleware;

use Closure;

class BrowserCache
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $response->header("pragma", "private");
        $response->header("Cache-Control", " private, max-age=86400");

        return $response;
    }
}
