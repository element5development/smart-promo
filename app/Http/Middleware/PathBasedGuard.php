<?php

namespace App\Http\Middleware;

use Closure;

class PathBasedGuard
{
    public function handle($request, Closure $next)
    {
        if (starts_with($request->path(), config('backpack.base.route_prefix'))) {
            config(['auth.defaults.guard' => 'admin']);
        }

        return $next($request);
    }
}
