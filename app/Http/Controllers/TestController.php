<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voucher;
use PDF;

class TestController extends Controller
{
    public function pdfVoucher()
    {
        $data = $this->getVoucherData();
        $content = view(['template' => config('settings.voucher_pdf_template')], $data)->render();
        $pdf = PDF::loadView(['template' => config('settings.pdf_layout_template')], compact('content'));
        return $pdf->stream();
    }
    
    public function pdfVoucherHtml()
    {
        $data = $this->getVoucherData();
        $content = view(['template' => config('settings.voucher_pdf_template')], $data)->render();
        return view(['template' => config('settings.pdf_layout_template')], compact('content'));
    }
    
    public function emailVoucher()
    {
        $voucher = Voucher::first();
        $notification = new App\Notifications\VoucherNotification($voucher, '123456');
        $message = $notification->toMail(App\User::first());
        return $message->render();
    }
     
    private function getVoucherData()
    {
        $voucher = Voucher::latest()->first();
    
        $data = [
            'voucher_number' => $voucher->number,
            'voucher_code' => '123456',
            'voucher_pdf' => $voucher->pdfUrl(),
            'voucher_qr_code' => $voucher->qrCodePath(),
            'amount' => $voucher->cost(),
            'first_name' => $voucher->user->first_name,
            'last_name' => $voucher->user->last_name,
            'email' => $voucher->user->email,
        ];
    
        return $data;
    }
}
