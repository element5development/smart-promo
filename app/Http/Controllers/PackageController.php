<?php

namespace App\Http\Controllers;

use App\Package;

class PackageController extends Controller
{
    public function index()
    {
        if (config('smartpromo.location_at_purchase')) {
            return Package::enabled()->with('products')->get();
        } else {
            $packages = Package::enabled()->get();
            foreach ($packages as $package) {
                $package->append('product');
            }
            return $packages;
        }
    }

    public function show($package)
    {
        if (config('smartpromo.location_at_purchase')) {
            return Package::enabled()->whereId($package)->with('products')->firstOrFail();
        } else {
            return Package::enabled()->whereId($package)->firstOrFail()->append('product');
        }
    }
}
