<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\TrackingVisit;
use Carbon\Carbon;

class TrackingVisitCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(TrackingVisit::class);
        $this->crud->setRoute('admin/tracking-visit');
        $this->crud->setEntityNameStrings('tracking visit', 'tracking visits');
        $this->crud->enableAjaxTable();
        $this->crud->denyAccess(['create', 'delete', 'edit']);
        $this->crud->removeAllButtons();
        $this->crud->enableExportButtons();
        $this->crud->disableSearching = true;

        $this->crud->query =
            TrackingVisit::with('vouchers')->with('vouchers.packages')->with('vouchers.user')->with('tracking_source');

        $this->crud->setColumns([
            ['name' => 'row_number', 'label' => '#', 'type' => 'row_number'],
            ['name' => 'user_id', 'label' => 'User', 'type' => 'select_link', 'function_name' => 'user', 'attribute' => 'name', 'path' => 'member'],
            ['name' => 'email', 'label' => 'Email', 'type' => 'model_function', 'function_name' => 'userEmail'],
            ['name' => 'created_at', 'label' => 'Date', 'type' => 'datetime'],
            ['name' => 'tracking_source', 'label' => 'Source', 'type' => 'model_function', 'function_name' => 'trackingSource'],
            ['name' => 'status', 'label' => 'Status', 'type' => 'model_function', 'function_name' => 'statusLabel'],
            ['name' => 'play_card', 'label' => 'Swipe Card #', 'type' => 'model_function', 'function_name' => 'playCardNumber'],
        ]);

        $this->crud->addFilter(
            ['type' => 'date_range', 'name' => 'from_to', 'label'=> 'Date range'],
            false,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', Carbon::parse($dates->to)->endOfDay());
            }
        );
    }

    public function search()
    {
        $this->crud->row_number = ($this->request->input('start') ?: 0) + 1;
        return parent::search();
    }
}
