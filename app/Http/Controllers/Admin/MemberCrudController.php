<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\User;
use DB;
use App\Http\Controllers\Admin\Traits\ShowOnlyDefinedColumns;

class MemberCrudController extends CrudController
{
    use ShowOnlyDefinedColumns;

    public function setup()
    {
        $this->crud->setModel(User::class);
        $this->crud->setRoute('admin/member');
        $this->crud->setEntityNameStrings('customer', 'customers');
        $this->crud->enableAjaxTable();

        $subQuery = User::select(DB::raw("
                users.id,
                COUNT(cards.id) card_count,
                IFNULL(SUM(cards.balance_play_value_currency), 0) as balance_play_value_currency,
                IFNULL(SUM(cards.balance_play_value_points), 0) as balance_play_value_points,
                IFNULL(SUM(cards.balance_play_bonus_currency), 0) as balance_play_bonus_currency,
                IFNULL(SUM(cards.balance_play_bonus_points), 0) as balance_play_bonus_points,
                IFNULL(MIN(cards.balance_retrieved_at), '') as balance_retrieved_at
            "))
            ->leftJoin('cards', 'cards.user_id', '=', 'users.id')
            ->groupBy('users.id');
    
        $this->crud->query = User::select(DB::raw('
                users.id,
                users.first_name,
                users.last_name,
                users.email,
                users.created_at, 
                users.is_shadow,
                a.card_count,
                a.balance_play_value_currency,
                a.balance_play_value_points,
                a.balance_play_bonus_currency,
                a.balance_play_bonus_points,
                a.balance_retrieved_at
            '))
            ->leftJoin(DB::raw('(' . $subQuery->toSql() . ') a'), 'a.id', '=', 'users.id');

        $this->crud->denyAccess(['create', 'update', 'delete']);
        $this->crud->allowAccess(['show', 'check_balance']);
        $this->crud->addButtonFromView('line', 'check_balance', 'check_balance', 'end');

        $this->crud->setColumns([
            ['name' => 'first_name', 'label' => 'First name', 'type' => 'text'],
            ['name' => 'last_name', 'label' => 'Last name', 'type' => 'text'],
            ['name' => 'email', 'label' => 'Email', 'type' => 'text'],
            ['name' => 'card_count', 'label' => 'Cards', 'type' => 'text'],
            ['name' => 'balance_play_value_currency', 'label' => 'Play value', 'type' => 'currency_and_points', 'points' => 'balance_play_value_points'],
            ['name' => 'balance_play_bonus_currency', 'label' => 'Play bonus', 'type' => 'currency_and_points', 'points' => 'balance_play_bonus_points'],
            ['name' => 'balance_retrieved_at', 'label' => 'Balance retrieved', 'type'  => 'datetime'],
            ['name' => 'created_at', 'label' => 'Created', 'type'  => 'datetime',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere(DB::raw("users.created_at LIKE '%?%'", $searchTerm));
                }
            ],
            ['name' => 'is_shadow', 'label' => 'Shadow?', 'type'  => 'boolean'],
        ]);
    }

    public function show($id)
    {
        $this->crud->addColumn(['name' => 'phone', 'label' => 'Mobile phone', 'type' => 'text'])->afterColumn('last_name');
        $this->crud->addColumn(['name' => 'address', 'label' => 'Address', 'type' => 'text'])->afterColumn('email');
        $this->crud->addColumn(['name' => 'city', 'label' => 'City', 'type' => 'text'])->afterColumn('address');
        $this->crud->addColumn(['name' => 'state', 'label' => 'State', 'type' => 'text'])->afterColumn('city');
        $this->crud->addColumn(['name' => 'postcode', 'label' => 'Postcode', 'type' => 'text'])->afterColumn('state');
        $this->crud->addColumn(['name' => 'birthdate', 'label' => 'Birthdate', 'type' => 'date'])->afterColumn('postcode');
        $this->crud->addColumn(['name' => 'preferred_location_id', 'label' => 'Preferred location', 'type' => 'select_link', 'entity' => 'preferred_location', 'attribute' => 'title', 'path' => 'location'])->afterColumn('birthdate');
     
        $this->crud->addColumns([
            ['name' => 'subscribed', 'label' => 'Subscribed?', 'type' => 'check'],
            ['name' => 'last_visit_at', 'label' => 'Last visit', 'type' => 'datetime'],
        ]);

        return self::showOnlyDefinedColumns($id);
    }

    public function check_balance(User $user)
    {
        foreach ($user->cards as $card) {
            $card->retrieveBalance();
        }

        \Alert::success('Actual balance retrieved')->flash();

        return redirect()->back();
    }
}
