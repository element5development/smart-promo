<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\TrackingSource;
use DB;
use Carbon\Carbon;

class TrackingSummaryCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(TrackingSource::class);
        $this->crud->setRoute('admin/tracking-summary');
        $this->crud->setEntityNameStrings('tracking summary', 'tracking summary');
        $this->crud->enableAjaxTable();
        $this->crud->denyAccess(['create', 'delete', 'edit']);
        $this->crud->removeAllButtons();
        $this->crud->enableExportButtons();
        $this->crud->disableSearching = true;

        $this->crud->query = $this->getQuery();

        $this->crud->setColumns([
            ['name' => 'row_number', 'label' => '#', 'type' => 'row_number'],
            ['name' => 'source', 'label' => 'Source'],
            ['name' => 'visitors', 'label' => 'Visitors'],
            ['name' => 'registered', 'label' => 'Registered'],
            ['name' => 'claimed', 'label' => 'Claimed'],
            ['name' => 'conversion', 'label' => 'Conversion'],
        ]);

        $this->crud->addFilter(
            ['type' => 'date_range', 'name' => 'from_to', 'label'=> 'Date range'],
            false,
            function ($value) {
                $dates = json_decode($value);
                $dates->to = Carbon::parse($dates->to)->endOfDay();
                $this->crud->query = $this->getQuery($dates->from, $dates->to);
            }
        );
    }

    private function getQuery($from = null, $to = null)
    {
        return
            TrackingSource::select(DB::raw("
                tracking_sources.source,
                COUNT(DISTINCT tracking_visits.id) visitors,
                COUNT(DISTINCT registered.id) registered,
                COUNT(DISTINCT claimed.id) claimed,
                CASE WHEN COUNT(DISTINCT registered.id) > 0 THEN COUNT(DISTINCT claimed.id) * 100 / COUNT(DISTINCT registered.id) ELSE 0 END conversion
            "))
            ->leftJoin('tracking_visits', function ($join) use ($from, $to) {
                $join->on('tracking_visits.tracking_source_id', '=', 'tracking_sources.id');
                $this->applyDateRange($join, 'tracking_visits.created_at', $from, $to);
            })
            ->leftJoin(DB::raw('tracking_visits registered'), function ($join) use ($from, $to) {
                $join->on('registered.tracking_source_id', '=', 'tracking_sources.id');
                $join->whereNotNull('registered.registered_at');
                $this->applyDateRange($join, 'registered.registered_at', $from, $to);
            })
            ->leftJoin(DB::raw('tracking_visits claimed'), function ($join) use ($from, $to) {
                $join->on('claimed.tracking_source_id', '=', 'tracking_sources.id');
                $join->whereNotNull('claimed.claimed_at');
                $this->applyDateRange($join, 'claimed.claimed_at', $from, $to);
            })
            ->groupBy('tracking_sources.source')
            ->havingRaw('COUNT(DISTINCT tracking_visits.id) > 0 OR COUNT(DISTINCT registered.id) OR COUNT(DISTINCT claimed.id)');
    }

    private function applyDateRange($join, $column, $from, $to)
    {
        if ($from) {
            $join->where($column, '>=', $from);
        }
        if ($to) {
            $join->where($column, '<=', $to);
        }
    }

    public function search()
    {
        $this->crud->row_number = ($this->request->input('start') ?: 0) + 1;
        return parent::search();
    }
}
