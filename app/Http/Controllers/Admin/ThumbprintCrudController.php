<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Thumbprint;

class ThumbprintCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Thumbprint::class);
        $this->crud->setRoute("admin/thumbprint");
        $this->crud->setEntityNameStrings('thumbprint', 'thumbprints');

        $this->crud->denyAccess(['edit', 'delete']);

        $this->crud->removeAllButtons();

        $this->crud->setColumns([
            ['name' => 'uri', 'label' => 'Base URL'],
            ['name' => 'key', 'label' => 'Key'],
            ['name' => 'secret', 'label' => 'Secret'],
            ['name' => 'thumbprint', 'label' => 'Thumbprint'],
            ['name' => 'use_count', 'label' => 'Use count'],
            ['name' => 'updated_at', 'label' => 'Updated'],
            ['name' => 'reserved_at', 'label' => 'Reserved'],
        ]);
    }
}