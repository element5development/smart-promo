<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\Admin\AdminStoreCrudRequest as StoreRequest;
use App\Http\Requests\Admin\AdminUpdateCrudRequest as UpdateRequest;
use App\Notifications\NewAdminNotification;
use Spatie\Permission\Models\Role;

class AdminCrudController extends CrudController
{
    private const MAINTAINER_ROLE = 'maintainer';

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel(config('backpack.permissionmanager.user_model'));
        $this->crud->setEntityNameStrings(trans('backpack::permissionmanager.user'), trans('backpack::permissionmanager.users'));
        $this->crud->setRoute(config('backpack.base.route_prefix').'/user');
        $this->crud->enableAjaxTable();

        $isMaintainer = $this->request->user()->hasRole(self::MAINTAINER_ROLE);
        if (!$isMaintainer) {
            $this->crud->query->whereDoesntHave('roles', function ($q) {
                $q->where('name', self::MAINTAINER_ROLE);
            });
        }

        // Columns.
        $this->crud->setColumns([
            [
                'name'  => 'name',
                'label' => trans('backpack::permissionmanager.name'),
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => trans('backpack::permissionmanager.email'),
                'type'  => 'email',
            ],
            [ // n-n relationship (with pivot table)
               'label'     => trans('backpack::permissionmanager.roles'), // Table column heading
               'type'      => 'select_multiple',
               'name'      => 'roles', // the method that defines the relationship in your Model
               'entity'    => 'roles', // the method that defines the relationship in your Model
               'attribute' => 'name', // foreign key attribute that is shown to user
               'model'     => config('laravel-permission.models.role'), // foreign key model
            ]
        ]);

        $roleQuery = $isMaintainer ? Role::query() : Role::where('name', '!=', self::MAINTAINER_ROLE);

        // Fields
        $this->crud->addFields([
            [
                'name'  => 'name',
                'label' => trans('backpack::permissionmanager.name'),
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => trans('backpack::permissionmanager.email'),
                'type'  => 'email',
            ],
            [
                'name'  => 'password',
                'label' => trans('backpack::permissionmanager.password'),
                'type'  => 'password',
            ],
            [
                'name'  => 'password_confirmation',
                'label' => trans('backpack::permissionmanager.password_confirmation'),
                'type'  => 'password',
            ],
            [
                'label'     => trans('backpack::permissionmanager.roles'),
                'type'      => 'checklist_from_query',
                'name'      => 'roles',
                'entity'    => 'roles',
                'attribute' => 'name',
                'model'     => config('laravel-permission.models.role'),
                'query'     => $roleQuery,
                'pivot'     => true,
            ],
            [
                'name' => 'rules_description',
                'type' => 'view',
                'view' => 'admin.partial.user_form_roles',
                'is_maintainer' => $isMaintainer,
            ]
        ]);

        $roleOptions = $roleQuery->pluck('name', 'id')->toArray();
        $this->crud->addFilter(['name' => 'role', 'type' => 'dropdown', 'label'=> 'Role'], $roleOptions, function ($value) {
            $this->crud->addClause('whereHas', 'roles', function ($q) use ($value) {
                $q->where('roles.id', $value);
            });
        });
    }

    /**
     * Store a newly created resource in the database.
     *
     * @param StoreRequest $request - type injection used for validation using Requests
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $password = $request->input('password');

        $this->handlePasswordInput($request);

        $response = parent::storeCrud($request);

        $this->crud->entry->notify(new NewAdminNotification($password));

        return $response;
    }

    /**
     * Update the specified resource in the database.
     *
     * @param UpdateRequest $request - type injection used for validation using Requests
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request)
    {
        $this->handlePasswordInput($request);

        return parent::updateCrud($request);
    }

    /**
     * Handle password input fields.
     *
     * @param CrudRequest $request
     */
    protected function handlePasswordInput($request)
    {
        // Remove fields not present on the user.
        $request->request->remove('password_confirmation');

        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', bcrypt($request->input('password')));
        } else {
            $request->request->remove('password');
        }
    }
}
