<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\Admin\PackageCrudRequest;
use App\Package;
use App\Product;
use App\Location;
use App\Lib\Embed\EmbedWrapper;
use GuzzleHttp\Command\Exception\CommandClientException;
use Illuminate\Http\Request;

class PackageCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Package::class);
        $this->crud->setRoute("admin/package");
        $this->crud->setEntityNameStrings('package', 'packages');

        $this->crud->setColumns([
            ['name'  => 'title', 'label' => 'Title', 'type' => 'text'],
            ['name'  => 'subtitle', 'label' => 'Subtitle', 'type' => 'text'],
            ['name'  => 'is_enabled', 'label' => 'Is enabled?', 'type' => 'onoff',
                'handler_url' => url('admin/package/enable-disable')],
        ]);

        $this->crud->addField(['name'  => 'title', 'label' => 'Title', 'type' => 'text']);
        $this->crud->addField(['name'  => 'subtitle', 'label' => 'Subtitle', 'type' => 'text']);

        if (config('smartpromo.location_at_purchase')) {
            $this->crud->addField(['name'  => 'products', 'label' => 'Products', 'type' => 'package_products',
                'entity' => 'products', 'model' => Product::class, 'pivot' => true]);
        } else {
            $this->crud->addField(['name' => 'products', 'label' => 'Product', 'type' => 'select_from_array',
                'options' => Product::whereNull('location_id')->pluck('product_name', 'id')->toArray(),
                'entity' => 'products', 'pivot' => true]);
        }

        $this->crud->addFilter(
            ['type' => 'simple', 'name' => 'enabled', 'label'=> 'Enabled'],
            false,
            function() {
                $this->crud->addClause('where', 'is_enabled', true);
            }
        );
    
        $this->crud->addFilter(
            ['type' => 'simple', 'name' => 'disabled', 'label'=> 'Disabled'],
            false,
            function() {
                $this->crud->addClause('where', 'is_enabled', false);
            }
        );
    }

    public function edit($id)
    {
        $entry = $this->crud->getEntry($id);
        $this->crud->update_fields['products']['value'] = $entry->product()->id;

        return parent::edit($id);
    }

    public function store(PackageCrudRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(PackageCrudRequest $request)
    {
        return parent::updateCrud();
    }

    public function enableDisable(Request $request)
    {
        $package = Package::findOrFail($request->input('id'));
        
        if ($request->has('on')) {
            if (!$package->hasValidProducts()) {
                return ['error' => 'Package is missing product definition. Please edit before enabling.'];
            }
        }

        $package->is_enabled = $request->has('on');
        $package->save();
    }
}
