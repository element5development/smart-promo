<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Transaction;
use App\User;
use App\Http\Controllers\Admin\Traits\ShowOnlyDefinedColumns;

class TransactionCrudController extends CrudController
{
    use ShowOnlyDefinedColumns;

    public function setup()
    {
        $this->crud->setModel(Transaction::class);
        $this->crud->setRoute('admin/transaction');
        $this->crud->setEntityNameStrings('payment', 'payments');
        $this->crud->enableAjaxTable();

        $this->crud->denyAccess(['create', 'update', 'delete']);
        $this->crud->allowAccess(['show']);

        $this->crud->removeButton('update');
        $this->crud->removeButton('revisions');
        
        $this->crud->setColumns([
            ['name' => 'transactionable', 'label' => 'Payment object', 'type' => 'morph_to'],
            ['name' => 'amount', 'label' => 'Amount', 'type' => 'text'],
            ['name' => 'user_id', 'label' => 'User', 'type' => 'select_link', 'entity' => 'user', 'attribute' => 'name', 'path' => 'member'],
            ['name' => 'transaction_id', 'label' => 'External transaction ID', 'type' => 'text'],
            ['name' => 'account_type', 'label' => 'Card type', 'type' => 'text'],
            ['name' => 'cc_number', 'label' => 'Last card number digits', 'type' => 'text'],
            ['name' => 'created_at', 'label' => 'Created', 'type'  => 'datetime'],
        ]);
    }

    public function show($id)
    {
        $this->crud->addColumn(['name' => 'response_data', 'label' => 'Response data', 'type'  => 'array']);

        return self::showOnlyDefinedColumns($id);
    }
}
