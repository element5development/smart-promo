<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Card;
use App\User;
use App\Http\Controllers\Admin\Traits\ShowOnlyDefinedColumns;
use Illuminate\Http\Request;

class CardCrudController extends CrudController
{
    use ShowOnlyDefinedColumns;

    public function setup()
    {
        $this->crud->setModel(Card::class);
        $this->crud->setRoute('admin/card');
        $this->crud->setEntityNameStrings('card', 'cards');
        $this->crud->enableAjaxTable();

        $this->crud->query = Card::select('cards.*')->leftJoin('users', 'users.id', '=', 'cards.user_id');

        $this->crud->denyAccess(['create', 'update', 'delete']);
        $this->crud->allowAccess(['show']);

        $this->crud->removeButton('update');
        $this->crud->removeButton('revisions');

        $this->crud->setColumns([
            ['name' => 'number', 'label' => 'Card number', 'type' => 'text'],
            ['name' => 'user_name', 'label' => 'User', 'type' => 'select_link', 'entity' => 'user', 'attribute' => 'name', 'path' => 'member'],
            ['name' => 'user_email', 'label' => 'Email', 'type' => 'select', 'entity' => 'user', 'attribute' => 'email'],
            ['name' => 'balance_play_value_currency', 'label' => 'Play value', 'type' => 'currency_and_points', 'points' => 'balance_play_value_points'],
            ['name' => 'balance_play_bonus_currency', 'label' => 'Play bonus', 'type' => 'currency_and_points', 'points' => 'balance_play_bonus_points'],
            ['name' => 'balance_retrieved_at', 'label' => 'Balance retrieved', 'type'  => 'datetime'],
        ]);

        $this->crud->addFilter(
            ['type' => 'simple', 'name' => 'registered', 'label'=> 'Registered'],
            false,
            function () {
                $this->crud->addClause('whereNotNull', 'user_id');
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'user_id',
                'type' => 'select2_ajax',
                'label'=> 'Member',
                'placeholder' => 'Type member name or email',
            ],
            route('admin.card.ajax-user-options'),
            function ($value) {
                $this->crud->addClause('where', 'user_id', $value);
            }
        );
    }

    public function show($id)
    {
        return self::showOnlyDefinedColumns($id);
    }

    public function userOptions(Request $request)
    {
        return User::where(\DB::raw("CONCAT(first_name, ' ', last_name)"), 'like', '%'.$request->term.'%')
            ->orWhere('email', 'like', '%'.$request->term.'%')
            ->select('id', \DB::raw("CONCAT(first_name, ' ', last_name, ' <', email, '>') name_email"))
            ->pluck('name_email', 'id');
    }
}
