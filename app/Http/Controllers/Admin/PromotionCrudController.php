<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\Admin\PromotionStoreCrudRequest;
use App\Http\Requests\Admin\PromotionUpdateCrudRequest;
use App\Http\Requests\Admin\PromotionTestRequest;
use App\Promotion;
use App\Product;
use App\User;
use App\Card;
use Artisan;
use Faker\Factory as FakerFactory;
use App\Notifications\PromotionNotification;
use Illuminate\Http\Request;
use NotificationChannels\Twilio\TwilioChannel;

class PromotionCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Promotion::class);
        $this->crud->setRoute("admin/promotion");
        $this->crud->setEntityNameStrings('promotion', 'promotion');

        $this->crud->allowAccess(['run', 'test']);

        $this->crud->addButtonFromView('line', 'run', 'run', 'end');
        $this->crud->addButtonFromView('line', 'test', 'test', 'end');

        $this->crud->setColumns([
            ['name' => 'name', 'label' => 'Name', 'type' => 'text'],
            ['name' => 'type', 'label' => 'Type', 'type' => 'model_function', 'function_name' => 'typeTitle'],
            ['name' => 'product_id', 'label' => 'Product', 'type' => 'model_function', 'function_name' => 'productName'],
            ['name' => 'start_at', 'label' => 'Start', 'type' => 'date'],
            ['name' => 'expire_at', 'label' => 'Expire', 'type' => 'date'],
            ['name' => 'is_enabled', 'label' => 'Enabled?', 'type' => 'onoff', 'handler_url' => url('admin/promotion/enable-disable')]
        ]);

        $autocompleteOptions = ['first_name' => 'First name', 'last_name' => 'Last name', 'card_number' => 'Card number'];
        $placeholderItems = ['first_name', 'last_name', 'card_number'];

        $months = array_map(function ($x) { return date('F', mktime(0, 0, 0, $x, 1)); }, range(1, 12));

        $typeOptions = $this->arrayToHash(Promotion::Types);
        $bonusProductOptions = $this->getBonusProducts();

        $this->crud->addFields([
            ['tab' => 'General', 'name' => 'name', 'label' => 'Name', 'type' => 'text'],
            ['tab' => 'General', 'name' => 'description', 'label' => 'Description', 'type' => 'textarea'],
        ]);

        $this->crud->addField(['tab' => 'General', 'name' => 'type', 'label' => 'Type', 'type' => 'select_from_array_switcher',
            'options' => $typeOptions, 'class_prefix' => 'promotion-type-', 'attributes' => ['id' => 'promotionTypeSelect']],
            'create');

        $this->crud->addField(['tab' => 'General', 'name' => 'type', 'label' => 'Type', 'type' => 'select_from_array_switcher',
            'options' => $typeOptions, 'class_prefix' => 'promotion-type-',
            'attributes' => ['id' => 'promotionTypeSelect', 'readonly' => true, 'disabled' => true]],
            'update');

        $this->crud->addFields([
            ['tab' => 'General', 'name' => 'apply-classes-on-tabs', 'type' => 'view', 'view' => 'admin.promotion_tab_classes'],
            // Frequency custom fields
            ['tab' => 'General', 'name' => 'frequency_range', 'label' => 'Frequency range', 'type' => 'select_from_array',
                'options' => $this->arrayToHash(Promotion::FrequencyRanges),
                'wrapperAttributes' => ['class' => 'form-group col-md-12 promotion-type-frequency']],
            ['tab' => 'General', 'name' => 'frequency_period', 'label' => 'Frequency period', 'type' => 'number',
                'wrapperAttributes' => ['class' => 'form-group col-md-12 promotion-type-frequency']],
            ['tab' => 'General', 'name' => 'frequency', 'label' => 'Frequency', 'type' => 'number',
                'wrapperAttributes' => ['class' => 'form-group col-md-12 promotion-type-frequency']],

            // Time since custom fields
            ['tab' => 'General', 'name' => 'time_range', 'label' => 'Time range', 'type' => 'select_from_array',
                'options' => $this->arrayToHash(Promotion::TimeRanges),
                'wrapperAttributes' => ['class' => 'form-group col-md-12 promotion-type-time_since_last_visit']],
            ['tab' => 'General', 'name' => 'time_since_last_visit', 'label' => 'Time ago', 'type' => 'number',
                'wrapperAttributes' => ['class' => 'form-group col-md-12 promotion-type-time_since_last_visit']],

            // Specific date custom fields
            ['tab' => 'General', 'name' => 'specific_month', 'label' => 'Specific month', 'type' => 'select_from_array',
                'options' => array_combine(range(1, 12), $months),
                'wrapperAttributes' => ['class' => 'form-group col-md-12 promotion-type-specific_date']],
            ['tab' => 'General', 'name' => 'specific_day', 'label' => 'Specific day', 'type' => 'select_from_array',
                'options' => array_combine(range(1, 31), range(1, 31)),
                'wrapperAttributes' => ['class' => 'form-group col-md-12 promotion-type-specific_date']],

            // Birthday custom fields
            ['tab' => 'General', 'name' => 'birthday_range', 'label' => 'Birthday range', 'type' => 'date_range',
                'start_name' => 'birthday_start_date', 'end_name' => 'birthday_end_date',
                'start_default' => date('c'), 'end_default' => date('c'),
                'date_range_options' => ["locale" => ["format" => "MM/DD"]],
                'wrapperAttributes' => ['class' => 'form-group col-md-12 promotion-type-birthday']],

            // Specific date and birthday custom fields
            ['tab' => 'General', 'name' => 'preliminary_days', 'label' => 'Notification days before', 'type' => 'number',
                'wrapperAttributes' => ['class' => 'form-group col-md-12 promotion-type-specific_date promotion-type-birthday']],

            ['tab' => 'General', 'name' => 'valid_for_range', 'label' => 'Valid for range', 'type' => 'select_from_array',
                'options' => $this->arrayToHash(Promotion::TimeRanges)],
            ['tab' => 'General', 'name' => 'valid_for', 'label' => 'Valid for', 'type' => 'number'],

            ['tab' => 'General', 'name' => 'product_id', 'label' => 'Bonus product', 'type' => 'select2_from_array',
                'options' => $bonusProductOptions, 'allows_null' => false],

            ['tab' => 'General', 'name' => 'start_at', 'label' => 'Start', 'type' => 'date_picker',
                'date_picker_options' => ['clearBtn' => true],
                'wrapperAttributes' => ['class' => 'form-group col-md-12 promotion-type-time_since_last_visit promotion-type-frequency']],
            ['tab' => 'General', 'name' => 'expire_at', 'label' => 'Expire', 'type' => 'date_picker',
                'date_picker_options' => ['clearBtn' => true],
                'wrapperAttributes' => ['class' => 'form-group col-md-12 promotion-type-time_since_last_visit promotion-type-frequency']],

            ['tab' => 'Notification email', 'name' => 'preliminary_email_description', 'type' => 'custom_html',
                'value' => '<i><strong>*</strong> Notification email is sent only for <strong>specific date</strong> and <strong>birthday</strong> types of promotions.</i>'],
            ['tab' => 'Notification email', 'name' => 'preliminary_email_subject', 'label' => 'Email subject', 'type' => 'text'],
            ['tab' => 'Notification email', 'name' => 'preliminary_email_template', 'label' => 'Email template',
                'type' => 'ckeditor_min', 'placeholder_items' => $placeholderItems],

            ['tab' => 'Notification SMS', 'name' => 'preliminary_sms_description', 'type' => 'custom_html',
                'value' => '<i><strong>*</strong> Notification SMS is sent only for <strong>specific date</strong> and <strong>birthday</strong> types of promotions.</i>'],
            ['tab' => 'Notification SMS', 'name' => 'preliminary_sms_template', 'label' => 'SMS template',
                'type' => 'ckeditor_min', 'placeholder_items' => $placeholderItems, 'plain_text' => true],
            ['tab' => 'Notification SMS', 'name' => 'preliminary_sms_counter',
                'type' => 'ckeditor_sms_counter', 'ckeditor' => 'preliminary_sms_template'],

            ['tab' => 'Promotional email', 'name' => 'promoted_email_subject', 'label' => 'Email subject', 'type' => 'text'],
            ['tab' => 'Promotional email', 'name' => 'promoted_email_template', 'label' => 'Email template',
                'type' => 'ckeditor_min', 'placeholder_items' => $placeholderItems],

            ['tab' => 'Promotional SMS', 'name' => 'promoted_sms_template', 'label' => 'SMS template',
                'type' => 'ckeditor_min', 'placeholder_items' => $placeholderItems, 'plain_text' => true],
            ['tab' => 'Promotional SMS', 'name' => 'promoted_sms_counter',
                'type' => 'ckeditor_sms_counter', 'ckeditor' => 'promoted_sms_template'],
        ]);

        $this->crud->addFilter(['name' => 'type', 'type' => 'dropdown', 'label'=> 'Type'], $typeOptions, function ($value) {
            $this->crud->addClause('where', 'type', $value);
        });

        $this->crud->addFilter(['name' => 'product_id', 'type' => 'dropdown', 'label'=> 'Product'], $bonusProductOptions, function ($value) {
            $this->crud->addClause('where', 'product_id', $value);
        });
    }

    private function arrayToHash($array)
    {
        foreach ($array as $value) {
            $hash[$value] = trans('promotion.' . $value);
        }
        return $hash;
    }

    private function getBonusProducts()
    {
        return Product::whereCost(0)->where('play_bonus_added', '>', 0)->pluck('product_name', 'id')->toArray();
    }

    public function store(PromotionStoreCrudRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(PromotionUpdateCrudRequest $request)
    {
        return parent::updateCrud();
    }

    public function run($id)
    {
        $exitCode = Artisan::call('promo:run', ['promotion' => $id]);
        
        if ($exitCode) {
            \Alert::error('Promotion run with errors, exit code: ' . $exitCode)->flash();
        } else {
            \Alert::success('Promotion run successfully')->flash();
        }

        return redirect()->back();
    }

    public function testForm(Promotion $promotion)
    {
        $this->crud->hasAccessOrFail('test');

        $this->data['entry'] = $promotion;
        $this->data['types'] = [
            'preliminary_email' => 'Notification email',
            'preliminary_sms' => 'Notification SMS',
            'promoted_email' => 'Promotional email',
            'promoted_sms' => 'Promotional SMS'
        ];
        $this->data['crud'] = $this->crud;
        $this->data['title'] = 'Test'.' '.$this->crud->entity_name;
        $this->data['faker'] = FakerFactory::create();

        return view('admin.promotion_test', $this->data);
    }

    public function testPost(Promotion $promotion, PromotionTestRequest $request)
    {
        $this->crud->hasAccessOrFail('test');

        $card = new Card(['number' => $request->card_number]);
        $card->user = new User($request->only('email', 'phone', 'first_name', 'last_name'));
        
        list($stage, $channel) = explode('_', $request->type, 2);
        $isPreliminary = $stage == 'preliminary';
        $notificationChannel = $channel == 'email' ? 'mail' : TwilioChannel::class;
        $target = $channel == 'email' ? $request->email : $request->phone;

        $card->notify(new PromotionNotification($promotion, $isPreliminary, $notificationChannel));

        \Alert::success("Test {$channel} sent to " . $target)->flash();
        return redirect()->back();
    }

    public function enableDisable(Request $request)
    {
        $promotion = Promotion::findOrFail($request->input('id'));
        $promotion->is_enabled = $request->has('on');
        $promotion->save();
    }
}
