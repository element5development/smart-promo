<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\Admin\LocationCrudRequest;
use App\Location;
use App\Http\Controllers\Admin\Traits\ShowOnlyDefinedColumns;

class LocationCrudController extends CrudController
{
    use ShowOnlyDefinedColumns;

    public function setup()
    {
        $this->crud->setModel(Location::class);
        $this->crud->setRoute("admin/location");
        $this->crud->setEntityNameStrings('location', 'locations');

        $this->crud->allowAccess(['show']);

        $this->crud->setColumns([
            ['name' => 'title', 'label' => 'Title', 'type' => 'text'],
        ]);

        $this->crud->addFields([
            ['tab' => 'General', 'name' => 'title', 'label' => 'Title', 'type' => 'text'],
            ['tab' => 'Embed server', 'name' => 'uri', 'label' => 'Base URL', 'type' => 'url'],
            ['tab' => 'Embed server', 'name' => 'key', 'label' => 'Key', 'type' => 'text'],
            ['tab' => 'Embed server', 'name' => 'secret', 'label' => 'Secret', 'type' => 'text'],
            ['tab' => 'Embed server', 'name' => 'thumbprints_json', 'label' => 'Thumbprints', 'type' => 'table',
                'entity_singular' => 'thumbprint',
                'columns' => [
                    'thumbprint' => 'Value',
                ],
                'min' => 1,
            ],
        ]);
    }

    public function show($id)
    {
        $this->crud->setColumns([
            ['name' => 'title', 'label' => 'Title'],
            ['name' => 'title', 'label' => 'Title'],
            ['name' => 'uri', 'label' => 'Base URL'],
            ['name' => 'key', 'label' => 'Key'],
            ['name' => 'secret', 'label' => 'Secret'],
            ['name' => 'thumbprints', 'label' => 'Thumbprints', 'type' => 'array']
        ]);

        return self::showOnlyDefinedColumns($id);
    }

    public function store(LocationCrudRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(LocationCrudRequest $request)
    {
        return parent::updateCrud();
    }
}