<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\Admin\VoucherClaimRequest;
use App\Voucher;
use Hash;
use App\Lib\Embed\EmbedWrapper;
use GuzzleHttp\Command\Exception\CommandClientException;
use GuzzleHttp\Command\Exception\CommandException;
use Illuminate\Validation\ValidationException;
use App\Services\UserService;
use App\Http\Controllers\Admin\Traits\ShowOnlyDefinedColumns;
use App\Location;
use App\Services\PurchaseService;
use App\Services\Cards\CardProviderInterface;
use Carbon\Carbon;
use Auth;

class VoucherCrudController extends CrudController
{
    use ShowOnlyDefinedColumns;

    public function setup()
    {
        $this->crud->setModel("App\Voucher");
        $this->crud->setRoute("admin/voucher");
        $this->crud->setEntityNameStrings('voucher', 'vouchers');

        $this->crud->denyAccess(['create', 'update', 'delete']);
        $this->crud->allowAccess(['show', 'claim']);

        if (!config('smartpromo.disable_admin_claim')) {
            $this->crud->addButtonFromView('line', 'claim', 'claim', 'end');
        }

        if (Auth::user()->hasRole('superadmin')) {
            $this->crud->addColumn([
                'name' => 'user_id', 'label' => 'User', 'type' => 'select_link', 'entity' => 'user', 'attribute' => 'name', 'path' => 'member'
            ]);
        } else {
            $this->crud->addColumn([
                'name' => 'user_id', 'label' => 'User', 'type' => 'select', 'entity' => 'user', 'attribute' => 'name'
            ]);
        }

        $this->crud->addColumns([
            ['name' => 'number', 'label' => 'Order confirmation', 'type' => 'text'],
            ['name' => 'qty', 'label' => 'Qty', 'type' => 'text'],
            ['name' => 'is_claimed', 'label' => 'Is claimed?', 'type' => 'nullable_boolean', 'options' => [2 => 'Partial']],
        ]);

        $this->crud->addFields([
            ['name' => 'packages', 'label' => 'Packages', 'type' => 'table'],
        ]);

        $this->crud->addFilter(
            ['name' => 'is_claimed', 'type' => 'dropdown', 'label'=> 'Claimed'],
            [true => 'Yes', false => 'No', 'null' => 'In progress'],
            function ($value) {
                if ($value === 'null') {
                    $this->crud->addClause('whereNull', 'is_claimed');
                } else {
                    $this->crud->addClause('where', 'is_claimed', $value);
                }
            }
        );
    }

    public function claimForm(Voucher $voucher)
    {
        $this->crud->hasAccessOrFail('claim');

        $this->data['entry'] = $voucher;
        $this->data['crud'] = $this->crud;
        $this->data['title'] = 'Claim'.' '.$this->crud->entity_name;

        return view('admin.claim', $this->data);
    }

    public function claimPost(
        Voucher $voucher,
        VoucherClaimRequest $request,
        UserService $userService,
        PurchaseService $purchaseService,
        CardProviderInterface $cardProvider
    ) {
        $this->crud->hasAccessOrFail('claim');

        $packages = $voucher->unclaimedPackages()->whereIn('voucher_packages.id', $request->package_id);
        
        if (!$packages->exists()) {
            throw ValidationException::withMessages([
                'package_id' => ['Something went wrong...'],
            ]);
        }

        $isCorrectCode = (config('smartpromo.encode_voucher_code') && Hash::check($request->code, $voucher->code)) ||
            (!config('smartpromo.encode_voucher_code') && $request->code == $voucher->code);

        if ($isCorrectCode) {
            try {
                if ($cardProvider->cardExists($request->card_number)) {
                    throw new \Exception('Card with provided number is already registered');
                }

                $card = $voucher->user->cards()->create([
                    'number' => $request->card_number,
                    'user_order' => $voucher->user->cards()->max('user_order') + 1,
                    'location_id' => $voucher->location_id,
                    'barcode' => '',
                ]);

                $packageCounts = (clone $packages)
                    ->groupBy('packages.id')
                    ->select('packages.id', \DB::raw('COUNT(*) AS qty'))
                    ->pluck('qty', 'id')
                    ->toArray();
                $products = $purchaseService->packagesToProducts($packageCounts);

                $cardProvider->registerUserWithProducts($voucher->user, $card, $products, $voucher->location_id);

                $userService->addFirstBonus($voucher->user);

                $cardProvider->syncCardDetails($card);

                \DB::table('voucher_packages')
                    ->whereIn('id', $request->package_id)
                    ->update(['card_id' => $card->id]);

                if ($voucher->unclaimedPackages()->count()) {
                    $voucher->is_claimed = null;
                    $voucher->save();
                    \Alert::success('Package claimed successfully')->flash();
                    return redirect()->back()->withInput($request->only('code'));
                } else {
                    $voucher->is_claimed = true;
                    $voucher->save();

                    $voucher->tracking_visits()->whereNull('claimed_at')->update(['claimed_at' => Carbon::now()]);

                    if (config('smartpromo.verification_email_after_claim')) {
                        $userService->sendVerificationEmail($voucher->user);
                    }

                    \Alert::success('Voucher claimed successfully')->flash();
                    return redirect($this->crud->route);
                }
            } catch (CommandClientException $e) {
                $json = json_decode($e->getResponse()->getBody()->getContents());
                $errorMessage = $json->error;
            } catch (\Exception $e) {
                \Log::error($e);
                $errorMessage = $e->getMessage();
            }
        } else {
            $errorMessage = 'Code doesn\'t match';
        }

        $request->flash();
        \Alert::error($errorMessage);
        return $this->claimForm($voucher, $request);
    }

    public function show($id)
    {
        $this->crud->addColumn(['name' => 'transaction_id', 'label' => 'Transaction ID', 'type' => 'model_function', 'function_name' => 'transactionId']);
        $this->crud->addColumn(['name' => 'last_4_digits_of_cc', 'label' => 'Last 4 # of CC', 'type' => 'model_function', 'function_name' => 'cardDetails']);
        $this->crud->addColumn(['name' => 'packages', 'label' => 'Cards', 'type' => 'cards']);

        return self::showOnlyDefinedColumns($id);
    }
}