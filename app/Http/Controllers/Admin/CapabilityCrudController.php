<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Capability;
use Illuminate\Http\Request;

class CapabilityCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        $this->crud->setModel(Capability::class);
        $this->crud->setEntityNameStrings('capability', 'capabilities');
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/capability');
        $this->crud->denyAccess(['create', 'delete', 'edit']);
        $this->crud->removeAllButtons();

        $this->crud->setColumns([
            ['name'  => 'name', 'label' => 'Name'],
            ['name'  => 'is_enabled', 'label' => 'Is enabled?', 'type' => 'onoff',
                'handler_url' => url('admin/capability/enable-disable')],
            ['name'  => 'description', 'label' => 'Description'],
        ]);
    }

    public function enableDisable(Request $request)
    {
        $capability = Capability::findOrFail($request->input('id'));
        $capability->is_enabled = $request->has('on');
        $capability->save();
    }
}
