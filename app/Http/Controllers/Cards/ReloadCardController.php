<?php

namespace App\Http\Controllers\Cards;

use App\Package;
use App\Transaction;
use App\Http\Requests\Cards\ReloadCardRequest;
use App\Lib\Embed\EmbedWrapper;
use App\Lib\Anet\AnetCharger;
use App\Lib\Anet\CreditCard;
use App\Lib\Anet\BillingAddress;
use App\Http\Controllers\Controller;
use App\Card;
use App\Location;
use App\Services\PurchaseService;
use App\Services\Cards\CardProviderInterface;

class ReloadCardController extends Controller
{
    public function reload(
        $barcode,
        ReloadCardRequest $request,
        PurchaseService $purchaseService,
        CardProviderInterface $cardProvider
    ) {
        if (config('smartpromo.location_at_purchase')) {
            $locationId = $request->location_id;
        } else {
            $locationId = null;
        }

        $amount = $purchaseService->calculateAmount($request->package, $locationId);

        try {
            $cardDetails = $cardProvider->cardDetails($request->barcode);
            $cardNumber = $cardDetails['id'];

            if ($user = $request->user()) {
                $card = $user->cards()->whereNumber($cardNumber)->firstOrFail();
            } elseif (!($card = Card::whereNumber($cardNumber)->first())) {
                $card = Card::create([
                    'number' => $cardNumber,
                    'barcode' => $cardDetails['barcode'],
                ]);
            }

            $transactionData = AnetCharger::charge(
                CreditCard::fromRequest($request),
                BillingAddress::fromRequest($request),
                $user ? $user->email : null,
                $amount
            );

            $card->transactions()->create(Transaction::creationData($user, $transactionData, $amount, $request->cc_number));

            $products = $purchaseService->packagesToProducts($request->package);

            $cardProvider->addProductsToCard($card, $products, $locationId);

            $card->retrieveBalance();
        } catch (\Exception $e) {
            \Log::error($e);
            return ['error' => $e->getMessage()];
        }
    }
}
