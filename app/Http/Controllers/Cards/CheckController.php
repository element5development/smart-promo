<?php

namespace App\Http\Controllers\Cards;

use App\Http\Requests\Cards\CheckReloadRequest;
use App\Http\Requests\Cards\CheckBalanceRequest;
use App\Lib\Embed\EmbedWrapper;
use GuzzleHttp\Command\Exception\CommandClientException;
use Illuminate\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Cards\RegisterCardRequest;
use App\Services\UserService;

class CheckController extends Controller
{
    public function checkReload(CheckReloadRequest $request)
    {
        return $this->check($request, true);
    }

    public function checkBalance(CheckBalanceRequest $request)
    {
        return $this->check($request, true);
    }

    private function check($request, $cvvCheck)
    {
        $embed = EmbedWrapper::obtainClient();

        if ($cvvCheck) {
            try {
                $embed->tryVerifyCvc($request->number, $request->cvv);
            } catch (\Exception $e) {
                throw ValidationException::withMessages([
                    'cvv' => [$e->getMessage()]
                ]);
            }
        }
        
        try {
            $cardDetails = $embed->tryCardDetails($request->number);
        } catch (CommandClientException $e) {
            $json = json_decode($e->getResponse()->getBody()->getContents());
            throw ValidationException::withMessages([
                'number' => [$json->error]
            ]);
        }

        return $cardDetails->toArray();
    }

    public function checkRegister(RegisterCardRequest $request, UserService $userService)
    {
        $userService->validateAdd($request->number, $request->cvv);
    }
}
