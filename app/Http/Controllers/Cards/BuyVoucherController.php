<?php

namespace App\Http\Controllers\Cards;

use App\Http\Requests\Cards\BuyCardsRequest;
use App\Http\Requests\Cards\BuyCardsPrevalidateRequest;
use App\Voucher;
use App\Package;
use App\Location;
use App\User;
use Hash;
use App\Lib\Anet\AnetCharger;
use App\Lib\Anet\CreditCard;
use App\Lib\Anet\BillingAddress;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\VoucherService;
use App\Services\UserService;
use App\Services\PurchaseService;

class BuyVoucherController extends Controller
{
    public function prevalidate(BuyCardsPrevalidateRequest $request)
    {
    }
    
    public function submit(BuyCardsRequest $request, VoucherService $voucherService, UserService $userService, PurchaseService $purchaseService)
    {
        $user = $request->user();

        if (config('smartpromo.location_at_purchase')) {
            $locationId = $request->location_id;
        } else {
            $locationId = null;
        }

        $amount = $purchaseService->calculateAmount($request->package, $locationId);

        try {
            $transactionData = AnetCharger::charge(
                CreditCard::fromRequest($request),
                BillingAddress::fromRequest($request),
                $user ? $user->email : $request->email,
                $amount
            );
        } catch (\Exception $e) {
            \Log::warning($e);
            return ['error' => $e->getMessage()];
        }
        
        if (!$user) {
            if (config('smartpromo.register_with_purchase')) {
                $user = $userService->register($request->only(array_keys($request->registrationRules())));
            } else {
                if (!($user = User::whereEmail($request->email)->first())) {
                    $user = $userService->registerShadow([
                        'email' => $request->email,
                        'first_name' => $request->cc_first_name,
                        'last_name' => $request->cc_last_name,
                        'address' => $request->billing_address,
                        'city' => $request->billing_city,
                        'state' => $request->billing_state,
                        'postcode' => $request->billing_zip,
                    ]);
                }
            }
        }

        $voucher = $voucherService->create($user, $request->package, $code, $locationId, $transactionData, $amount, $request->cc_number);

        $pdf = $voucher->pdfUrl();

        $shadow_token = $user->is_shadow ? base64_encode($user->password) : null;

        return compact('voucher', 'code', 'pdf', 'shadow_token');
    }

    public function resend(Request $request, VoucherService $voucherService)
    {
        if ($voucher = Voucher::whereNumber($request->get('number'))->first()) {
            if (config('smartpromo.encode_voucher_code')) {
                if (Hash::check($request->get('code'), $voucher->code)) {
                    $code = $request->get('code');
                } else {
                    $code = null;
                }
            } else {
                $code = $voucher->code;
            }

            if ($code) {
                $voucherService->sendEmail($voucher, $code);
            }
        }
        return 'true';
    }
}
