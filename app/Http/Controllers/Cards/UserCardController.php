<?php

namespace App\Http\Controllers\Cards;

use Illuminate\Http\Request;
use App\Http\Requests\Cards\AddCardRequest;
use Illuminate\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Services\UserService;

class UserCardController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();

        $primary_card = $user->primary_card();

        $cards = $user->cards()->where('is_archived', false);

        if ($primary_card) {
            $cards->orderBy(\DB::raw("id != {$primary_card->id}"))->orderBy('user_order');
        }

        $cards = $cards->get();

        $isBalanceExpired = false;

        foreach ($cards as $card) {
            if ($card->isBalanceExpired()) {
                if ($request->get('retrieve_balance') == 'true') {
                    $card->retrieveBalance();
                } else {
                    $card->is_balance_expired = true;
                }
            }

            $card->is_primary = $primary_card && $primary_card->id == $card->id;
        }

        return $cards;
    }

    public function add(AddCardRequest $request, UserService $userService)
    {
        try {
            return $userService->addCard($request->user(), $request->number, $request->cvv);
        } catch (ValidationException $e) {
            throw $e;
        } catch (\Exception $e) {
            \Log::error($e);
            $errorMessage = $e->getMessage();
        }
        return ['error' => $errorMessage];
    }

    public function primary($id, Request $request)
    {
        $user = $request->user();
        $card = $user->cards()->findOrFail($id);

        $user->primary_card_id = $id;
        $user->save();
    }

    public function archive($id, Request $request)
    {
        $user = $request->user();
        if ($user->primary_card_id == $id) {
            $user->primary_card_id = null;
            $user->save();
        }

        $request->user()->cards()->whereId($id)->update(['is_archived' => true]);
    }
}
