<?php

namespace App\Http\Controllers;

use App\Product;

class ProductController extends Controller
{
    public function firstBonus()
    {
        return Product::firstBonusProduct();
    }
}
