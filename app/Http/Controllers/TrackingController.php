<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visit;
use App\TrackingSource;
use App\TrackingVisit;

class TrackingController extends Controller
{
    public function visit(Request $request)
    {
        $source = $request->get('source') ?? '';
        $trackingSource = TrackingSource::firstOrCreate(compact('source'));

        if ($request->get('visit')) {
            $trackingVisit = TrackingVisit::fromCookie($request->get('visit'));
            if ($trackingVisit && strlen($trackingSource->source) && $trackingVisit->tracking_source_id != $trackingSource->id) {
                $trackingVisit = null;
            } else {
                $trackingVisit->touch();
            }
        } else {
            $trackingVisit = null;
        }

        if (!$trackingVisit) {
            $trackingVisit = TrackingVisit::create(['tracking_source_id' => $trackingSource->id]);
        }

        $visit = $trackingVisit->getCookie();

        return compact('visit');
    }
}
