<?php

namespace App\Http\Controllers;

use App\Location;

class LocationController extends Controller
{
    public function index()
    {
        return Location::all();
    }
}
