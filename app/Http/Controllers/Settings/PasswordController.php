<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Hash;

class PasswordController extends Controller
{
    /**
     * Update the user's password.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'current_password' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);


        if (Hash::check($request->current_password, $request->user()->password)) {
            $request->user()->update([
                'password' => bcrypt($request->password)
            ]);
        } else {
            throw ValidationException::withMessages([
                'current_password' => [trans('passwords.wrong_password')],
            ]);
        }
    }
}
