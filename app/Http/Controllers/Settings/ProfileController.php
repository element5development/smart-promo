<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Lib\Embed\EmbedWrapper;
use App\Guest;
use App\User;
use Newsletter;
use App\Services\Cards\EmbedCardProvider;
use Validator;

class ProfileController extends Controller
{
    public function update(ProfileRequest $request, EmbedCardProvider $embedCardProvider)
    {
        $user = $request->user();

        $user->fill($request->all());

        if ($user->isDirty('email') || ($request->has('email_confirmation') && !empty($request->email_confirmation))) {
            Validator::validate($request->only('email', 'email_confirmation'), [
                'email' => 'confirmed',
            ]);
        }

        $unsubscribed = $user->isDirty('subscribed') && !$user->subscribed;
        
        if (config('smartpromo.enable_newsletter_subscribe') && $user->isDirty('email') || $unsubscribed) {
            Newsletter::unsubscribe($user->email);
        }

        if ($user->isDirty(['first_name', 'last_name', 'address', 'phone', 'email'])) {
            $details = [
                'holder_firstname' => $user->first_name,
                'holder_surname' => $user->last_name,
                'address1' => $user->address,
                'mobile_phone' => $user->phone,
                'email' => $user->email,
            ];

            $cards = $user->cards('cards')->with('guests.location')->get();
            foreach ($cards as $card) {
                if ($card->location_id) {
                    $embedCardProvider->addGuestDetails($card, $user, $card->location_id);
                }
            }
        }

        if (config('smartpromo.enable_newsletter_subscribe') && $user->isDirty(['email', 'first_name', 'last_name', 'subscribed']) && $user->subscribed) {
            Newsletter::subscribeOrUpdate($user->email, ['FNAME' => $user->first_name, 'LNAME' => $user->last_name]);
        }

        $user->save();

        return $user;
    }

    public function unsubscribe(Request $request)
    {
        if ($request->has('email')) {
            if ($user = User::whereEmail($request->get('email'))->first()) {
                $user->subscribed = false;
                $user->save();
            }
        }
    }
}
