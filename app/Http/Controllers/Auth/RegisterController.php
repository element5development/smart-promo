<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Package;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jrean\UserVerification\Traits\VerifiesUsers;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\RegisterShadowRequest;
use App\Http\Requests\Auth\RegisterPromotionRequest;
use App\Services\UserService;
use App\Services\VoucherService;
use Jrean\UserVerification\Facades\UserVerification as UserVerificationFacade;
use Jrean\UserVerification\Exceptions\UserNotFoundException;
use Jrean\UserVerification\Exceptions\UserIsVerifiedException;
use Jrean\UserVerification\Exceptions\TokenMismatchException;
use App\Lib\Intercard\IntercardWrapper;
use Illuminate\Validation\ValidationException;
use App\TrackingVisit;
use Carbon\Carbon;

class RegisterController extends Controller
{
    use VerifiesUsers;

    protected $redirectTo = '/home';

    protected $redirectIfVerified = '/account-verification/verified';
    protected $redirectAfterVerification = '/account-verification/success';
    protected $redirectIfVerificationFails = '/account-verification/failure';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(RegisterRequest $request, UserService $userService)
    {
        return $userService->register($request->all());
    }

    public function resend(Request $request, UserService $userService)
    {
        if ($user = User::whereEmail($request->get('email'))->whereVerified(false)->first()) {
            $userService->sendVerificationEmail($user);
        }
    }

    public function shadow(RegisterShadowRequest $request, UserService $userService)
    {
        return $userService->burnShadow($request->all());
    }

    public function promotion(RegisterPromotionRequest $request, UserService $userService, VoucherService $voucherService)
    {
        if (config('smartpromo.encode_voucher_code')) {
            throw new \Exception('Platform must be configured to disable voucher code hashing');
        }

        $intercard = IntercardWrapper::obtainClient();
        
        $response = $intercard->memberByEmail(['email' => $request->email]);
        if (isset($response['Id'])) {
            throw ValidationException::withMessages([
                'email' => ['Member with this email already exists'],
            ]);
        }

        if (User::whereEmail($request->email)->exists()) {
            return response()->json(['error' => 'already_submitted']);
        }

        $user = $userService->registerShadow([
            'email' => $request->email,
            'first_name' => $request->first_name,
            'last_name' => '',
        ]);

        $packages = [Package::newUserPromotionPackage()->id => 1];

        $voucher = $voucherService->create($user, $packages, $code);

        if ($trackingVisit = TrackingVisit::fromCookie($request->get('visit'))) {
            $trackingVisit->vouchers()->attach($voucher->id);
            if (!$trackingVisit->registered_at) {
                $trackingVisit->registered_at = Carbon::now();
                $trackingVisit->save();
            }
        }

        return response()->json(true);
    }

    public function promotionResend(Request $request, VoucherService $voucherService)
    {
        if ($user = User::whereEmail($request->email)->first()) {
            if ($voucher = $user->vouchers()->first()) {
                $voucherService->sendEmail($voucher, $voucher->code);
                return response()->json(true);
            }
        }
        return response()->json(false);
    }

    public function emailVerification($token, Request $request)
    {
        if (!$this->validateRequest($request)) {
            return $this->failedVerificationResponse();
        }

        try {
            $user = UserVerificationFacade::process($request->input('email'), $token, $this->userTable());
        } catch (UserNotFoundException $e) {
            return $this->failedVerificationResponse();
        } catch (TokenMismatchException $e) {
            return $this->failedVerificationResponse();
        } catch (UserIsVerifiedException $e) {
        }

        return response()->json(true);
    }

    private function failedVerificationResponse()
    {
        return response()->json(['error' => 'verification-failed'], 422);
    }
}
