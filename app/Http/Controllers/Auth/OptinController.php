<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\Auth\OptinRequest;
use App\Guest;
use Auth;
use App\Services\UserService;

class OptinController extends Controller
{
    public function guest($token)
    {
        return Guest::whereVerificationToken($token)->firstOrFail();
    }

    public function optin(OptinRequest $request, UserService $userService)
    {
        $guest = Guest::whereVerificationToken($request->token)->firstOrFail();

        $user = $userService->register(array_merge($request->all(), ['email' => $guest->email]), true);

        $token = $this->guard()->login($user);
        $this->guard()->setToken($token);

        $token = (string) $this->guard()->getToken();
        $expiration = $this->guard()->getPayload()->get('exp');

        return [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $expiration - time(),
        ];
    }

    protected function guard()
    {
        return Auth::guard();
    }
}
