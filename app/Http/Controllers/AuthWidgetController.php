<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthWidgetController extends Controller
{
    protected $guard;

    public function signInOutLink(Request $request)
    {
        return $this->guestOrAuthView($request, 'widget.sign-in-out-link');
    }

    public function authClassSwitch(Request $request)
    {
        return $this->guestOrAuthView($request, 'widget.auth-class-switch');
    }

    private function guestOrAuthView($request, $view)
    {
        $authenticated = $this->guard()->check();

        $view = view($view, compact('authenticated'));

        return response($view)->header('Content-Type', 'application/javascript');
    }

    public function logout(Request $request)
    {
        try {
            $this->guard()->logout();
        } catch (JWTException $e) {}

        return redirect('/sign-in');
    }

    protected function guard()
    {
        if (empty($this->guard)) {
            $this->guard = Auth::guard();
        }
        return $this->guard;
    }
}
