<?php

namespace App\Http\Controllers;

use App\Setting;

class SettingController extends Controller
{
    public function __invoke()
    {
        return Setting::where('key', 'LIKE', 'shared.%')->get()->mapWithKeys(function ($item) {
            list(, $key) = explode('.', $item['key'], 2);
            return [$key => $item['value']];
        });
    }
}
