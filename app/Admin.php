<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Backpack\CRUD\CrudTrait;
use Spatie\Permission\Traits\HasRoles;
use Gravatar;

class Admin extends Authenticatable
{
    use Notifiable;
    use CrudTrait;
    use HasRoles;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function getAvatarAttribute()
    {
        return Gravatar::fallback('https://placehold.it/160x160/00a65a/ffffff/&text='.$this->name[0])->get($this->email);
    }
}
