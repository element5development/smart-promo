<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Package;

class PackageArray implements Rule
{
    public function passes($attribute, $value)
    {
        $enabled_package_ids = Package::enabled()->pluck('id')->toArray();

        $positive = false;
        foreach ($value as $package_id => $count) {
            if (!in_array($package_id, $enabled_package_ids)) {
                return false;
            }
            if ($count > 0) {
                $positive = true;
            }
        }
        return $positive;
    }

    public function message()
    {
        return 'At least one :attribute is required.';
    }
}
