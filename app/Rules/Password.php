<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Password implements Rule
{
    const MIN_LENGTH = 8;

    public function passes($attribute, $value)
    {
        return
            strlen($value) >= self::MIN_LENGTH &&
            preg_match('/[A-Z]/', $value) &&
            preg_match('/[a-z]/', $value) &&
            preg_match('/[0-9]/', $value) &&
            preg_match('/[^A-Za-z0-9]/', $value);
    }

    public function message()
    {
        return ':Attribute must be at least 8 characters, include one upper case letter, one lower case letter, one number and one special character.';
    }
}
