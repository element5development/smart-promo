<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Location;
use App\Product;

class ProductArray implements Rule
{
    public function passes($attribute, $value)
    {
        foreach (Location::all() as $location) {
            if (!isset($value[$location->id]) || Product::findOrFail($value[$location->id])->location_id != $location->id) {
                return false;
            }
        }

        return true;
    }

    public function message()
    {
        return 'All :attribute are required.';
    }
}
