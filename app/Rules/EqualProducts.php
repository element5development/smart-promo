<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Product;

class EqualProducts implements Rule
{
    public function passes($attribute, $value)
    {
        $groupCount = Product::whereIn('id', array_values($value))
            ->groupBy(['cost', 'play_bonus_added'])
            ->select('cost')
            ->get()
            ->count();

        return $groupCount == 1;
    }

    public function message()
    {
        return 'All :attribute must be equal in cost and bonus.';
    }
}
