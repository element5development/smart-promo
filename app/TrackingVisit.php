<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use JWT;

class TrackingVisit extends Model
{
    use CrudTrait;
    
    protected $fillable = ['tracking_source_id'];

    public function tracking_source()
    {
        return $this->belongsTo(TrackingSource::class);
    }

    public function vouchers()
    {
        return $this->belongsToMany(Voucher::class, 'tracking_visit_vouchers');
    }

    public static function fromCookie($token)
    {
        try {
            $payload = JWT::decode($token);
            return TrackingVisit::find($payload['sub']);
        } catch (\Exception $e) {
        }
    }

    public function getCookie()
    {
        return JWT::encode(['sub' => $this->id]);
    }

    public function user()
    {
        if ($voucher = $this->vouchers()->first()) {
            return $voucher->user;
        }
    }

    public function userEmail()
    {
        if ($user = $this->user()) {
            return $user->email;
        }
    }

    public function trackingSource()
    {
        if ($trackingSource = $this->tracking_source) {
            return $trackingSource->source;
        }
    }

    public function statusLabel()
    {
        if ($this->claimed_at) {
            return 'Claimed';
        } elseif ($this->registered_at) {
            return 'Registered';
        } else {
            return 'Visit';
        }
    }

    public function playCardNumber()
    {
        if ($voucher = $this->vouchers()->first()) {
            if ($voucher->is_claimed) {
                if ($package = $voucher->packages()->first()) {
                    if ($card = Card::find($package->pivot->card_id)) {
                        return $card->number;
                    }
                }
            }
        }
    }
}
