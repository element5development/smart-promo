<?php

namespace App\Services;

use App\Voucher;
use App\Package;
use Hash;
use PDF;
use App\Notifications\VoucherNotification;
use App\Services\Cards\CardProviderInterface;
use QRCode;

class VoucherService
{
    protected $cardProvider;

    public function __construct(CardProviderInterface $cardProvider)
    {
        $this->cardProvider = $cardProvider;
    }

    public function create($user, $packages, &$code, $locationId = null, $transactionData = null, $amount = null, $ccNumber = null)
    {
        $number = $this->cardProvider->generateVoucherNumber();
        $code = $this->generateCode(6);

        $voucher = Voucher::create([
            'user_id' => $user->id,
            'location_id' => $locationId,
            'number' => $number,
            'code' => config('smartpromo.encode_voucher_code') ? Hash::make($code) : $code,
            'qty' => 0,
        ]);

        if ($transactionData) {
            $voucher->createTransaction($transactionData, $amount, $ccNumber);
        }

        $this->setVoucherPackages($voucher, $packages);

        $this->generateQRCode($voucher);

        $this->generatePdf($voucher, $code);

        $this->sendEmail($voucher, $code);

        return $voucher;
    }

    public function sendEmail($voucher, $code)
    {
        try {
            $voucher->user->notify(new VoucherNotification($voucher, $code));
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    private function setVoucherPackages($voucher, $packages)
    {
        foreach ($packages as $package_id => $count) {
            $voucher->packages()->attach(array_fill(0, $count, $package_id));
        }

        $voucher->update(['qty' => $voucher->packages()->count()]);
    }

    public function generateQRCode($voucher)
    {
        
        QRCode::text($voucher->number)->setMargin(1)->setOutfile($voucher->qrCodePath())->png();
    }

    public function generatePdf($voucher, $code)
    {
        $data = [
            'voucher_number' => $voucher->number,
            'voucher_code' => $code,
            'voucher_pdf' => $voucher->pdfUrl(),
            'voucher_qr_code' => $voucher->qrCodePath(),
            'amount' => $voucher->cost(),
            'first_name' => $voucher->user->first_name,
            'last_name' => $voucher->user->last_name,
            'email' => $voucher->user->email,
        ];

        $content = view(['template' => config('settings.voucher_pdf_template')], $data)->render();

        $pdf = PDF::loadView(['template' => config('settings.pdf_layout_template')], compact('content'));

        $pdf->save($voucher->pdfPath());
    }

    private function generateCode($length)
    {
        return str_pad(mt_rand(0, str_repeat(9, $length)), $length, '0', STR_PAD_LEFT);
    }
}
