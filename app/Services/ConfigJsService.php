<?php

namespace App\Services;

use Illuminate\Http\Request;

class ConfigJsService
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function get()
    {
        $locale = app()->getLocale();

        $pathMap = [
            'sign_in' => '/sign-in',
            'register' => '/playcard-registration',
            'register_shadow' => '/finish-registration',
            'buy_cards' => '/playcard-purchase',
            'reload_card' => '/playcards/:barcode/reload',
            'my_cards' => '/my-playcards',
        ];

        if ($this->request->has('path_map')) {
            $customPathMap = json_decode($this->request->path_map, true);
            if (is_array($customPathMap)) {
                $pathMap = array_merge($pathMap, $customPathMap);
            } else {
                throw new \Exception('path_map must be an array');
            }
        }

        return [
            'appName' => config('app.name'),
            'locale' => $locale,
            'translations' => json_decode(file_get_contents(resource_path("lang/{$locale}.json")), true),
            'baseUri' => url('/'),
            'pathMap' => $pathMap,
            'locationAtPurchase' => config('smartpromo.location_at_purchase'),
            'registerWithPurchase' => config('smartpromo.register_with_purchase'),
        ];
    }
}
