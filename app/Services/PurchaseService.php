<?php

namespace App\Services;

use App\Package;
use App\Location;
use App\Lib\Embed\EmbedWrapper;
use App\Services\Cards\CardProviderInterface;

class PurchaseService
{
    public function __construct(CardProviderInterface $cardProvider)
    {
        $this->cardProvider = $cardProvider;
    }

    public function calculateAmount($packageCounts, $locationId = null)
    {
        $value = $tax = 0;

        foreach ($packageCounts as $packageId => $count) {
            $product = $this->getPackageProduct($packageId, $locationId);
            $value += $product->cost * $count;
            $tax += $product->tax * $count;
        }

        return $value + $tax;
    }

    private function getPackageProduct($packageId, $locationId)
    {
        $products = Package::findOrFail($packageId)->products();
        if ($locationId) {
            return $products->whereLocationId($locationId)->firstOrFail();
        } else {
            return $products->whereNull('products.location_id')->firstOrFail();
        }
    }

    public function packagesToProducts($packages, $locationId = null)
    {
        $products = [];
        foreach ($packages as $packageId => $count) {
            $product = $this->getPackageProduct($packageId, $locationId);
            array_push($products, ...array_fill(0, $count, $product));
        }
        return $products;
    }

    public function addPackagesToCard($card, $packages, $locationId = null)
    {
        $products = [];
        foreach ($packages as $packageId => $count) {
            $product = $this->getPackageProduct($packageId, $locationId);
            array_push($products, ...array_fill(0, $count, $product));
        }

        $locationProviderConfig = $locationId ? Location::find($locationId)->getEmbedConfig() : null;

        $this->cardProvider->addProductsToCard($card, $products, $locationProviderConfig);
    }
}
