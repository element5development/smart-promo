<?php

namespace App\Services\Promotions;

class TimeSinceLastVisitPromotionService extends PromotionService
{
    public function getEligibleUsers($promotion, $date, $preliminary = false)
    {
        $users = $this->usersWithSamePromotions($promotion, $preliminary)
            ->whereDoesntHave('cards.visits', function ($query) use ($promotion, $date) {
                $query->where('visits.visit_at', '>', $promotion->timeSinceDate($date));
            })
            ->whereHas('cards.visits', function ($query) use ($promotion, $date) {
                $query->where('visits.visit_at', '<', $promotion->timeSinceDate($date));
            })
            ->get();

        return $this->yieldEligibleUsers($promotion, $this->excludeUsersWithSamePromotionAfterLastVisit($users, $promotion), $date);
    }

    private function excludeUsersWithSamePromotionAfterLastVisit($users, $promotion)
    {
        $remainingUsers = [];

        foreach ($users as $user) {
            $excludeUser = false;

            if ($lastVisit = $user->visits()->latest()->first()) {
                $isPromotionAppliedAfterLastVisit = $user->promotions()
                    ->where('promotions.id', $promotion->id)
                    ->where('user_promotions.promoted_at', '>', $lastVisit->visit_at)
                    ->exists();

                if ($isPromotionAppliedAfterLastVisit) {
                    $excludeUser = true;
                }
            }

            if (!$excludeUser) {
                $remainingUsers[] = $user;
            }
        }

        return $remainingUsers;
    }
}
