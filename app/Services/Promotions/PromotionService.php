<?php

namespace App\Services\Promotions;

use App\User;

abstract class PromotionService
{
    abstract public function getEligibleUsers($promotion, $date, $preliminary = false);

    public function getPreliminaryDays($promotion, $date)
    {
        return false;
    }

    protected function usersWithSameTypePromotions($promotion, $preliminary = false)
    {
        return $this->usersWithPromotion($preliminary, function ($query) use ($promotion) {
            $query->where('promotions.type', $promotion->type);
        });
    }
    
    protected function usersWithSamePromotions($promotion, $preliminary = false)
    {
        return $this->usersWithPromotion($preliminary, function ($query) use ($promotion) {
            $query->where('promotions.id', $promotion->id);
        });
    }

    private function usersWithPromotion($preliminary, $callback)
    {
        return User::with(['promotions' => function ($query) use ($preliminary, $callback) {
            $callback($query);
            if (!$preliminary) {
                $query->where('user_promotions.is_preliminary', false);
            }
        }]);
    }

    protected function yieldEligibleUsers($promotion, $users, $date, $promotionValidCallback = null)
    {
        foreach ($users as $user) {
            if ($user->cards()->count()) {
                $excludeUser = false;

                $isUserSubscribedToPromotion =
                    (!empty($promotion->promoted_email_template) && $user->subscribed) ||
                    (!empty($promotion->promoted_sms_template) && $user->sms_subscribed);

                if ($isUserSubscribedToPromotion) {
                    foreach ($user->promotions as $promotion) {
                        $promotionStillValid = $promotion->pivot->expire_at > $date;

                        if (!$promotionStillValid || $promotionValidCallback) {
                            $promotionStillValid = call_user_func($promotionValidCallback, $promotion);
                        }
                        
                        if ($promotionStillValid) {
                            $excludeUser = true;
                        }
                    }
                } else {
                    $excludeUser = true;
                }

                if (!$excludeUser) {
                    yield $user;
                }
            }
        }
    }
}
