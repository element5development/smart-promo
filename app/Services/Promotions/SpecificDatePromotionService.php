<?php

namespace App\Services\Promotions;

use Carbon\Carbon;

class SpecificDatePromotionService extends PromotionService
{
    public function getPreliminaryDays($promotion, $date)
    {
        if ($days = $promotion->type_options['preliminary_days']) {
            $specificDate = $this->getSpecificDate($promotion)->addDays(-$days);
            if ($specificDate == $date) {
                return $days;
            }
        }
    }

    public function getEligibleUsers($promotion, $date, $preliminary = false)
    {
        $specificDate = $this->getSpecificDate($promotion);

        if ($date == $specificDate) {
            $users = $this->usersWithSamePromotions($promotion, $preliminary)->get();

            return $this->yieldEligibleUsers($promotion, $users, $date, function ($cardPromotion) use ($specificDate) {
                return Carbon::parse($cardPromotion->pivot->promoted_at) == $specificDate;
            });
        }
    }

    private function getSpecificDate($promotion)
    {
        return Carbon::create(null, $promotion->type_options['specific_month'], $promotion->type_options['specific_day'], 0);
    }
}
