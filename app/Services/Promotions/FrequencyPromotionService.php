<?php

namespace App\Services\Promotions;

class FrequencyPromotionService extends PromotionService
{
    public function getEligibleUsers($promotion, $date, $preliminary = false)
    {
        $users = $this->usersWithSamePromotions($promotion, $preliminary)
            ->whereHas('cards.visits', function ($query) use ($promotion, $date) {
                $query->where('visits.visit_at', '>', $promotion->frequencyFromDate($date));
            }, '>=', $promotion->type_options['frequency'])
            ->get();
        
        return $this->yieldEligibleUsers($promotion, $users, $date);
    }
}
