<?php

namespace App\Services\Promotions;

use Carbon\Carbon;

class BirthdayPromotionService extends PromotionService
{
    public function getPreliminaryDays($promotion, $date)
    {
        if ($days = $promotion->type_options['preliminary_days']) {
            return $days;
        }
    }

    public function getEligibleUsers($promotion, $date, $preliminary = false)
    {
        $birthdayStartDate = Carbon::parse($promotion->type_options['birthday_start_date']);
        $birthdayEndDate = Carbon::parse($promotion->type_options['birthday_end_date']);

        $birthdayStartDate->year = $date->year;
        if ($birthdayStartDate > $date) {
            $birthdayStartDate->addYear(-1);
        }
        $birthdayEndDate->year = $birthdayStartDate->year;
        if ($birthdayStartDate > $birthdayEndDate) {
            $birthdayEndDate->addYear(1);
        }

        if ($birthdayStartDate <= $date && $date <= $birthdayEndDate) {
            $users = $this->usersWithSameTypePromotions($promotion, $preliminary)
                ->whereRaw('MONTH(birthdate) = MONTH(?) AND DAY(birthdate) = DAY(?)', [$date, $date])
                ->get();

            return $this->yieldEligibleUsers($promotion, $users, $date, function ($cardPromotion) use ($date) {
                return Carbon::parse($cardPromotion->pivot->promoted_at)->diffInYears($date) <= 0;
            });
        }
    }
}
