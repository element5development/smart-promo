<?php

namespace App\Services;

use App\Notifications\VerificationNotification;
use App\User;
use App\Product;
use Illuminate\Auth\Events\Registered;
use UserVerification;
use Newsletter;
use App\Services\Cards\CardProviderInterface;
use App\Services\Cards\EmbedCardProvider;
use App\Lib\Embed\EmbedWrapper;
use App\Card;
use App\Guest;
use Illuminate\Validation\ValidationException;
use GuzzleHttp\Command\Exception\CommandClientException;

class UserService
{
    public function __construct(CardProviderInterface $cardProvider, EmbedCardProvider $embedCardProvider)
    {
        $this->cardProvider = $cardProvider;
        $this->embedCardProvider = $embedCardProvider;
    }

    public function register($data, $verified = false)
    {
        $data['password'] = bcrypt($data['password']);
        $data['subscribed'] = true;
        $data['sms_subscribed'] = true;
        $data['is_shadow'] = false;
        $user = User::updateOrCreate(['email' => $data['email']], $data);

        event(new Registered($user));

        if ($verified) {
            $user->verified = true;
            $user->save();
        } else {
            UserVerification::generate($user);
            $this->sendVerificationEmail($user);
        }

        if (config('smartpromo.enable_newsletter_subscribe')) {
            Newsletter::subscribeOrUpdate($user->email, ['FNAME' => $user->first_name, 'LNAME' => $user->last_name]);
        }

        if (isset($data['register_card'])) {
            try {
                $this->addCard($user, $data['register_card_number'], $data['register_card_cvv']);
            } catch (\Exception $e) {
            }
        }

        return $user;
    }

    public function registerShadow($data)
    {
        $password = str_random();

        $data = array_merge([
            'password' => bcrypt($password),
            'address' => '',
            'city' => '',
            'state' => '',
            'postcode' => '',
            'phone' => '',
            'subscribed' => true,
            'sms_subscribed' => false,
            'is_shadow' => true,
        ], $data);

        $user = User::create($data);

        event(new Registered($user));

        return $user;
    }

    public function burnShadow($data)
    {
        $user = User::wherePassword(base64_decode($data['shadow_token']))->firstOrFail();

        unset($data['shadow_token']);
        $data['password'] = bcrypt($data['password']);
        $data['subscribed'] = true;
        $data['sms_subscribed'] = true;
        $data['is_shadow'] = false;
        
        $user->fill($data);
        $user->save();

        event(new Registered($user));

        UserVerification::generate($user);
        $this->sendVerificationEmail($user);

        if (config('smartpromo.enable_newsletter_subscribe')) {
            Newsletter::subscribeOrUpdate($user->email, ['FNAME' => $user->first_name, 'LNAME' => $user->last_name]);
        }

        return $user;
    }

    public function sendVerificationEmail($user)
    {
        $user->notify(new VerificationNotification());
    }

    public function addFirstBonus($user)
    {
        if ($product = Product::firstBonusProduct()) {
            if ($user->cards()->count() == 1) {
                $this->cardProvider->addProductsToCard($user->cards()->first(), [$product]);
            }
        }
    }

    public function addCard($user, $number, $cvv)
    {
        $cardDetails = $this->validateAdd($number, $cvv, $user);

        $card = Card::firstOrNew(['number' => $cardDetails['id']]);
        $card->user_id = $user->id;
        $card->user_order = $user->cards()->max('user_order') + 1;
        $card->is_archived = false;
        $card->barcode = $cardDetails['barcode'];

        if (!$card->location_id) {
            $card->location_id = $user->preferred_location_id;
        }

        $card->save();

        $this->addFirstBonus($user);
        
        if ($card->location_id) {
            $this->embedCardProvider->addGuestDetails($card, $user, $card->location_id);
        }

        $this->addRemainingCards($card, $user);

        $card->retrieveBalance();
    }

    public function validateAdd($number, $cvv, $user = null)
    {
        $embed = EmbedWrapper::obtainClient();

        try {
            $embed->tryVerifyCvc($number, $cvv);
        } catch (\Exception $e) {
            throw ValidationException::withMessages([
                'cvv' => [$e->getMessage()]
            ]);
        }
        
        try {
            $cardDetails = $embed->tryCardDetails($number);
        } catch (CommandClientException $e) {
            $json = json_decode($e->getResponse()->getBody()->getContents());
            throw ValidationException::withMessages([
                'number' => [$json->error]
            ]);
        }

        if ($card = Card::whereNumber($cardDetails['id'])->first()) {
            if ($card->user_id && !$card->is_archived) {
                if ($user != null && $card->user_id == $user->id) {
                    throw ValidationException::withMessages([
                        'number' => ['You have already added this Swipe Card']
                    ]);
                } else {
                    throw ValidationException::withMessages([
                        'number' => ['This card is already added under different profile']
                    ]);
                }
            }
        }

        if ($cardDetails['guest']) {
            if ($guest = Guest::whereGuestId($cardDetails['guest']['id'])->first()) {
                $query = $guest->cards()->whereNotNull('user_id');

                if ($user) {
                    $query->where('user_id', '!=', $user->id);
                }

                if ($query->exists()) {
                    throw ValidationException::withMessages([
                        'number' => ['Other cards of same guest are already added under different profile']
                    ]);
                }
            }
        }

        return $cardDetails;
    }

    private function addRemainingCards($card, $user)
    {
        foreach ($card->guests()->get() as $guest) {
            $cards = $guest->cards()->whereNull('user_id');

            if ($cards->count()) {
                $update = [
                    'user_id' => $user->id,
                    'user_order' => $user->cards()->max('user_order')
                ];
    
                foreach ($cards->get() as $card) {
                    $update['user_order']++;
                    $card->update($update);
                }
            }
        }
    }
}
