<?php

namespace App\Services\Cards;

use App\Services\Cards\CardProviderInterface;
use App\Lib\Intercard\IntercardWrapper;
use App\Package;

class IntercardCardProvider implements CardProviderInterface
{
    public function cardExists($number)
    {
        $intercard = IntercardWrapper::obtainClient();

        // try {
        //     $embed->cardDetails(['id' => $request->card_number, 'skip_global_card_check' => 'true']);
        //     return true;
        // } catch (CommandClientException $e) {}

        return false;
    }

    public function cardDetails($number)
    {
        $intercard = IntercardWrapper::obtainClient();

        // try {
        //     $embed->cardDetails(['id' => $request->card_number, 'skip_global_card_check' => 'true']);
        //     return true;
        // } catch (CommandClientException $e) {}

        return false;
    }

    public function addProductsToCard($card, $products, $locationId = null)
    {
        // try {
        //     $embed = $locationId ?
        //         EmbedWrapper::obtainClient(Location::find($locationId)->getEmbedConfig()) :
        //         EmbedWrapper::obtainClient();

        //     try {
        //         $embed->saleCancel();
        //     } catch (\Exception $e) {}

        //     foreach ($products as $product) {
        //         $cartData = $embed->saleAddItem([
        //             'product_id' => $product->product_id,
        //             'product_type' => $product->product_type,
        //             'card_id' => $card->number,
        //         ]);
        //     }
        //     $tenderAmount = $cartData['total_inc_tax']['value'];
    
        //     $embed->saleTender(['tenders' => [[
        //         'tender_amount' => $tenderAmount,
        //         'tender_type' => EmbedWrapper::CreditCardTenderType,
        //     ]]]);
        // } catch (CommandClientException $e) {
        //     $json = json_decode($e->getResponse()->getBody()->getContents());
        //     throw new \Exception($json->error);
        // }
    }

    public function registerUserWithProducts($user, $card, $products, $locationId = null)
    {
        // $this->addProductsToCard($card, $products, $locationId);

        // if ($locationId) {
        //     $guestService->addGuestDetails($card, $user, $locationId);
        // }
    }

    public function syncCardDetails($card)
    {
        // $embed = EmbedWrapper::obtainClient();

        // $cardDetails = $embed->tryCardDetails($card->number);
        
        // $card->balance_retrieved_at = Carbon::now();
        // $card->balance_play_total_currency = $cardDetails['play_total']['currency'];
        // $card->balance_play_total_points = $cardDetails['play_total']['points'];
        // $card->balance_play_value_currency = $cardDetails['play_value']['currency'];
        // $card->balance_play_value_points = $cardDetails['play_value']['points'];
        // $card->balance_play_bonus_currency = $cardDetails['play_bonus']['currency'];
        // $card->balance_play_bonus_points = $cardDetails['play_bonus']['points'];

        // $card->tickets = $cardDetails['etickets'];

        // $card->barcode = $cardDetails['barcode'];

        // $card->save();
    }

    public function cardNumberValidationRules()
    {
        return ['digits:3'];
    }

    public function generateVoucherNumber()
    {
        $intercard = IntercardWrapper::obtainClient();

        $barcodes = $intercard->getBarcodes([
            'barcodepromoid' => Package::newUserPromotionPackage()->product->product_id
        ]);
        
        return $barcodes['DC_BarcodedPromotionNumbersBatchListing'][0]['barcode'];
    }
}
