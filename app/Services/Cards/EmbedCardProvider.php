<?php

namespace App\Services\Cards;

use App\Services\Cards\CardProviderInterface;
use App\Lib\Embed\EmbedWrapper;
use App\Guest;
use App\Location;
use Carbon\Carbon;
use GuzzleHttp\Command\Exception\CommandClientException;

class EmbedCardProvider implements CardProviderInterface
{
    public function cardExists($number)
    {
        $embed = EmbedWrapper::obtainClient();

        try {
            $embed->cardDetails(['id' => $number, 'skip_global_card_check' => 'true']);
            return true;
        } catch (CommandClientException $e) {}

        return false;
    }

    public function cardDetails($number)
    {
        $embed = EmbedWrapper::obtainClient();
        return $embed->cardDetails(['id' => $number, 'skip_global_card_check' => 'true']);
    }

    public function addProductsToCard($card, $products, $locationId = null)
    {
        try {
            $embed = $locationId ?
                EmbedWrapper::obtainClient(Location::find($locationId)->getEmbedConfig()) :
                EmbedWrapper::obtainClient();

            try {
                $embed->saleCancel();
            } catch (\Exception $e) {}

            foreach ($products as $product) {
                $cartData = $embed->saleAddItem([
                    'product_id' => $product->product_id,
                    'product_type' => $product->product_type,
                    'card_id' => $card->number,
                ]);
            }
            $tenderAmount = $cartData['total_inc_tax']['value'];
    
            $embed->saleTender(['tenders' => [[
                'tender_amount' => $tenderAmount,
                'tender_type' => EmbedWrapper::CreditCardTenderType,
            ]]]);
        } catch (CommandClientException $e) {
            $json = json_decode($e->getResponse()->getBody()->getContents());
            throw new \Exception($json->error);
        }
    }

    public function registerUserWithProducts($user, $card, $products, $locationId = null)
    {
        $this->addProductsToCard($card, $products, $locationId);

        if ($locationId) {
            $this->addGuestDetails($card, $user, $locationId);
        }
    }

    public function syncCardDetails($card)
    {
        $embed = EmbedWrapper::obtainClient();

        $cardDetails = $embed->tryCardDetails($card->number);
        
        $card->balance_retrieved_at = Carbon::now();
        $card->balance_play_total_currency = $cardDetails['play_total']['currency'];
        $card->balance_play_total_points = $cardDetails['play_total']['points'];
        $card->balance_play_value_currency = $cardDetails['play_value']['currency'];
        $card->balance_play_value_points = $cardDetails['play_value']['points'];
        $card->balance_play_bonus_currency = $cardDetails['play_bonus']['currency'];
        $card->balance_play_bonus_points = $cardDetails['play_bonus']['points'];

        $card->tickets = $cardDetails['etickets'];

        $card->barcode = $cardDetails['barcode'];

        $card->save();
    }

    public function addGuestDetails($card, $user, $locationId)
    {
        $details = [
            'holder_firstname' => $user->first_name,
            'holder_surname' => $user->last_name,
            'address1' => $user->address,
            'mobile_phone' => $user->phone,
            'email' => $user->email,
        ];

        $embed = EmbedWrapper::obtainClient(Location::find($locationId));
        $embed->addGuestDetails(array_merge(['id' => $card->number], $details));

        $cardDetails = $embed->tryCardDetails($card->number);

        $guest = $this->getUpdatedGuest($cardDetails['guest'], $locationId);

        $card->guests()->syncWithoutDetaching([$guest->id]);
    }

    public function getUpdatedGuest($guestDetails, $locationId)
    {
        $guest = Guest::firstOrNew(['location_id' => $locationId, 'guest_id' => $guestDetails['id']]);
        $guest->fill(array_intersect_key($guestDetails, array_flip(Guest::EMBED_PROPERTIES)));
        $guest->save();
        return $guest;
    }

    public function cardNumberValidationRules()
    {
        return ['digits:19'];
    }

    public function generateVoucherNumber()
    {
        return str_pad(mt_rand(0, str_repeat(9, 10)), 10, '0', STR_PAD_LEFT);
    }
}
