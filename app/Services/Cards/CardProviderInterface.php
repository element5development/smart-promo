<?php

namespace App\Services\Cards;

interface CardProviderInterface
{
    public function cardExists($number);

    public function cardDetails($number);

    public function addProductsToCard($card, $products, $locationId = null);

    public function syncCardDetails($card);

    public function cardNumberValidationRules();

    public function generateVoucherNumber();
}
