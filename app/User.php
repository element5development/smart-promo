<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Backpack\CRUD\CrudTrait;
use Image;
use Storage;
use App\Notifications\ResetPassword;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use CrudTrait;

    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'gender', 'address', 'city', 'state', 'postcode',
        'phone', 'birthdate', 'picture', 'preferred_location_id', 'subscribed', 'sms_subscribed',
        'is_shadow', 'country',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['name'];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getCardCountAttribute()
    {
        return $this->attributes['card_count'] ?? $this->cards()->count();
    }

    public function getBalancePlayValueCurrencyAttribute()
    {
        return $this->attributes['balance_play_value_currency'] ?? $this->cards()->sum('balance_play_value_currency');
    }

    public function getBalancePlayValuePointsAttribute()
    {
        return $this->attributes['balance_play_value_points'] ?? $this->cards()->sum('balance_play_value_points');
    }

    public function getBalancePlayBonusCurrencyAttribute()
    {
        return $this->attributes['balance_play_bonus_currency'] ?? $this->cards()->sum('balance_play_bonus_currency');
    }

    public function getBalancePlayBonusPointsAttribute()
    {
        return $this->attributes['balance_play_bonus_points'] ?? $this->cards()->sum('balance_play_bonus_points');
    }

    public function getBalanceRetrievedAtAttribute()
    {
        return $this->attributes['balance_retrieved_at'] ?? $this->cards()->min('balance_retrieved_at');
    }

    public function getLastVisitAtAttribute()
    {
        return $this->attributes['last_visit_at'] ?? $this->visits()->max('last_at');
    }

    public function preferred_location()
    {
        return $this->belongsTo(Location::class);
    }

    public function vouchers()
    {
        return $this->hasMany(Voucher::class);
    }

    public function cards()
    {
        return $this->hasMany(Card::class);
    }

    public function visits()
    {
        return $this->hasManyThrough(Visit::class, Card::class);
    }

    public function guests()
    {
        return Guest::join('cards', 'cards.guest_id', '=', 'guests.id')->where('cards.user_id', $this->id)->select('guests.*');
    }

    public function promotions()
    {
        return $this->belongsToMany(Promotion::class, 'user_promotions')
            ->withPivot('user_id', 'promotion_id', 'card_id', 'promoted_at', 'promoted_balance', 'expire_at', 'is_expired')
            ->withTimestamps();
    }

    public function primary_card()
    {
        if ($this->primary_card_id) {
            if ($card = $this->cards()->whereId($this->primary_card_id)->first()) {
                return $card;
            }
        }
        
        return $this->cards()->where('is_archived', false)->latest('balance_play_total_points')->first();
    }

    public function setPictureAttribute($value)
    {
        if ($value == null) {
            Storage::disk('profile_pictures')->delete($this->image);
            $this->attributes['picture'] = null;
        }
        if (starts_with($value, 'data:image')) {
            $image = Image::make($value)->fit(450, 450);
            $filename = md5($value . time()) . '.jpg';
            Storage::disk('profile_pictures')->put($filename, $image->stream());
            $this->attributes['picture'] = $filename;
        }
    }

    public function getPictureAttribute()
    {
        return $this->attributes['picture'] ? Storage::disk('profile_pictures')->url($this->attributes['picture']) : null;
    }

    public function routeNotificationForTwilio()
    {
        return $this->phone;
    }
}
