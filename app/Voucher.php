<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Voucher extends Model
{
    use CrudTrait;

    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'number', 'code', 'location_id',
        'package_id', 'qty', 'address', 'email', 'phone', 'transaction_id',
    ];

    protected $hidden = [
        'code',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function packages()
    {
        return $this->belongsToMany(Package::class, 'voucher_packages')->withPivot('id', 'qty', 'card_id')->withTimestamps();
    }

    public function distinctCards()
    {
        return $this->belongsToMany(Card::class, 'voucher_packages')->distinct();
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function transactions()
    {
        return $this->morphMany(Transaction::class, 'transactionable');
    }

    public function tracking_visits()
    {
        return $this->belongsToMany(TrackingVisit::class, 'tracking_visit_vouchers');
    }

    public function createTransaction($transactionData, $amount, $ccNumber)
    {
        $creationData = Transaction::creationData($this->user, $transactionData, $amount, $ccNumber);
        $transaction = $this->transactions()->create($creationData);
        $this->update(['transaction_id' => $transaction->id]);
    }

    public function locationTitle()
    {
        return $this->location ? $this->location->title : null;
    }

    public function transactionId()
    {
        return $this->transaction ? $this->transaction->transaction_id : null;
    }

    public function cardDetails()
    {
        return $this->transaction ? $this->transaction->account_type . ' *' . $this->transaction->cc_number : null;
    }
    
    public function claimedPackages()
    {
        return $this->packages()->whereNotNull('voucher_packages.card_id');
    }
    
    public function unclaimedPackages()
    {
        return $this->packages()->whereNull('voucher_packages.card_id');
    }

    public function pdfPath()
    {
        return storage_path('app/public/vouchers/' . $this->number . '.pdf');
    }

    public function qrCodePath()
    {
        return storage_path('app/public/voucher_qr_codes/' . $this->number . '.png');
    }

    public function pdfUrl()
    {
        return url('storage/vouchers/' . $this->number . '.pdf');
    }

    public function cost()
    {
        return $this->transaction ? $this->transaction->amount : 0;
    }
}
