<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use App\Lib\Embed\EmbedWrapper;
use App\User;
use App\Guest;
use Illuminate\Notifications\Notifiable;

class Card extends Model
{
    use Notifiable;
    use CrudTrait;

    protected $fillable = ['user_id', 'user_order', 'guest_id', 'number', 'barcode', 'location_id'];

    protected $dates = ['balance_retrieved_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function guests()
    {
        return $this->belongsToMany(Guest::class);
    }

    public function packages()
    {
        return $this->belongsToMany(Package::class, 'voucher_packages')->withPivot('id', 'qty', 'voucher_id')->withTimestamps();
    }

    public function visits()
    {
        return $this->hasMany(Visit::class);
    }

    public function transactions()
    {
        return $this->morphMany(Transaction::class, 'transactionable');
    }

    public function isBalanceExpired()
    {
        return is_null($this->balance_retrieved_at) || $this->balance_retrieved_at->diffInMinutes() > config('cache.expire.card_balance_retrieval');
    }

    public function retrieveBalance()
    {
        $embed = EmbedWrapper::obtainClient();
        
        $cardDetails = $embed->tryCardDetails($this->number);

        $this->setBalance($cardDetails);

        $this->save();
    }

    public function setBalance($cardDetails)
    {
        $this->balance_retrieved_at = Carbon::now();
        $this->balance_play_total_currency = $cardDetails['play_total']['currency'];
        $this->balance_play_total_points = $cardDetails['play_total']['points'];
        $this->balance_play_value_currency = $cardDetails['play_value']['currency'];
        $this->balance_play_value_points = $cardDetails['play_value']['points'];
        $this->balance_play_bonus_currency = $cardDetails['play_bonus']['currency'];
        $this->balance_play_bonus_points = $cardDetails['play_bonus']['points'];
        $this->tickets = $cardDetails['etickets'];
    }

    public function routeNotificationForMail()
    {
        return $this->user->email;
    }

    public function routeNotificationForTwilio()
    {
        return $this->user->phone;
    }
}
