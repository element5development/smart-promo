<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Package extends Model
{
    use SoftDeletes;
    use CrudTrait;

    protected $fillable = ['title', 'subtitle', 'is_enabled'];

    public function scopeEnabled($query)
    {
        return $query->where('is_enabled', true);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function product()
    {
        return $this->products()->whereNull('products.location_id')->first();
    }

    public function getProductAttribute()
    {
        return $this->product();
    }

    public function hasValidProducts()
    {
        if (config('settings.location_at_purchase')) {
            foreach (Location::all() as $location) {
                if (!$this->products()->where('products.location_id', $location->id)->exists()) {
                    return false;
                }
            }
        } else {
            if (!$this->products()->whereNull('products.location_id')->exists()) {
                return false;
            }
        }
        return true;
    }

    public static function newUserPromotionPackage()
    {
        return self::find(config('settings.new_user_promotion_package_id'));
    }
}
