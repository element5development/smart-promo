<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Location extends Model
{
    use SoftDeletes;
    use CrudTrait;

    protected $fillable = ['title', 'uri', 'key', 'secret', 'thumbprints', 'thumbprints_json'];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'thumbprints' => 'array',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->setThumbprintsFromJson();
        });
        self::updating(function ($model) {
            $model->setThumbprintsFromJson();
        });
        self::saving(function ($model) {
            $model->setThumbprintsFromJson();
        });
    }

    public function getEmbedConfig()
    {
        $config = array_intersect_key(
            $this->attributes,
            array_flip(['uri', 'key', 'secret'])
        );
        
        $config['thumbprints'] = $this->thumbprints;
        
        return $config;
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function getThumbprintsJsonAttribute()
    {
        $thumbprints = [];
        foreach ($this->thumbprints as $thumbprint) {
            $thumbprints[] = ['thumbprint' => $thumbprint];
        }
        return json_encode($thumbprints);
    }

    public function setThumbprintsFromJson()
    {
        if (isset($this->attributes['thumbprints_json'])) {
            $json = $this->attributes['thumbprints_json'];
            if (is_string($json)) {
                $json = json_decode($json, true);
            }

            $thumbprints = [];
            foreach ($json as $thumbprint) {
                $thumbprints[] = $thumbprint['thumbprint'];
            }

            $this->attributes['thumbprints'] = json_encode($thumbprints);

            unset($this->attributes['thumbprints_json']);
        }
    }
}
