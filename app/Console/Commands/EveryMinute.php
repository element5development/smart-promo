<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class EveryMinute extends Command
{
    protected $signature = 'every:minute';

    protected $description = 'Run minutely routines.';

    public function handle()
    {
        if (config('capabilities.intercard')) {
            $this->call('intercard:import:redeemed');
        }
    }
}
