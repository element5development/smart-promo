<?php

namespace App\Console\Commands;

use App\Location;
use App\Package;
use App\Product;
use App\Voucher;
use App\Guest;
use App\Card;
use App\User;
use App\Lib\Intercard\IntercardWrapper;
use Carbon\Carbon;
use App\Services\UserService;

class IntercardImportRedeemedBarcodes extends BasePeriodImport
{
    protected $signature = 'intercard:import:redeemed';

    protected $description = 'Import redeemed barcodes';

    private $intercard;

    public function __construct(UserService $userService)
    {
        parent::__construct('import_from_date', 'redeemed_barcodes_imported_to_date');

        $this->userService = $userService;
    }

    public function handle()
    {
        if (!config('capabilities.intercard')) {
            throw new \Exception('This command is not supported');
        }

        $this->initDates();

        $this->intercard = IntercardWrapper::obtainClient();

        $from = Carbon::create($this->fromDate->year, $this->fromDate->month, $this->fromDate->day, $this->fromDate->hour, $this->fromDate->minute);
        $to = Carbon::create($this->toDateTime->year, $this->toDateTime->month, $this->toDateTime->day, $this->toDateTime->hour, $this->toDateTime->minute + 1);
        $redeemedBarcodes = $this->intercard->getRedeemedBarcodes([
            'barcodepromoid' => Package::newUserPromotionPackage()->product->product_id,
            'from' => $from->toDateTimeString(),
            'to' => $to->toDateTimeString(),
        ]);

        if (count($redeemedBarcodes)) {
            $this->info('New redeemeded barcodes: ' . count($redeemedBarcodes));

            foreach ($redeemedBarcodes as $redeemedBarcode) {
                $voucher = Voucher::whereIsClaimed(false)->whereNumber($redeemedBarcode['Barcode'])->first();
                if ($voucher) {
                    $this->info("Claiming voucher #{$voucher->id} with barcode {$voucher->number}");
                    $this->claimVoucher($voucher, $redeemedBarcode['Account'], $this->intercard);
                    $this->info('Voucher claimed');
                }
            }
        }

        $this->finishImport();
    }

    private function claimVoucher($voucher, $accountNumber)
    {
        $member = $this->intercard->getMemberByAccountNumber(['account_number' => $accountNumber]);
        if (isset($member['Id'])) {
            $this->info('Member already associated');
            if ($member['Email'] == $voucher->user->email) {
                $user = $voucher->user;
            } else {
                $this->info('Associated member has different email');

                if (!($user = User::whereEmail($member['Email'])->first())) {
                    $user = $this->userService->registerShadow([
                        'email' => $member['Email'],
                        'first_name' => $member['FirstName'],
                        'last_name' => $member['LastName'],
                        'phone' => $member['PhoneNumber'],
                    ]);
                }

                $voucher->claimer_user_id = $user->id;
            }
        } else {
            $this->info('Creating new member and associating with card');

            $user = $voucher->user;

            $member = $this->intercard->memberByEmail(['email' => $user->email]);

            if (!isset($member['Id'])) {
                $bod = $user->birthdate ? Carbon::parse($user->birthdate) : Carbon::today();
                $this->intercard->oneStepNewMember(['user' => [
                    'Email' => $user->email,
                    'Firstname' => $user->first_name,
                    'LastName' => $user->last_name,
                    'PhoneNumber' => $user->phone,
                    'City' => $user->city,
                    'StateProvince' => $user->state,
                    'PostalCode' => $user->postcode,
                    'DateOfBirth' => $bod->toDateString(),
                ]]);

                $member = $this->intercard->memberByEmail(['email' => $user->email]);
            }

            $this->intercard->addCard([
                'customer_id' => $member['Id'],
                'account_number' => $accountNumber,
            ]);
        }

        $card = Card::firstOrNew(['number' => $accountNumber], ['barcode' => '']);
        $card->fill([
            'user_id' => $user->id,
            'user_order' => $user->cards()->max('user_order') + 1,
        ]);
        $card->save();

        $guest = Guest::firstOrNew(['guest_id' => $member['Id']]);
        $guest->fill([
            'holder_firstname' => $member['FirstName'],
            'holder_surname' => $member['LastName'],
            'address1' => $member['StreetAdd1'],
            'address2' => $member['StreetAdd2'],
            'address3' => $member['StreetAdd3'],
            'postal_code' => $member['PostalCode'],
            'mobile_phone' => $member['PhoneNumber'],
            'email' => $member['Email'],
            'dob' => Carbon::parse($member['DateOfBirth']),
        ]);
        $guest->save();

        $card->guests()->syncWithoutDetaching([$guest->id]);

        $voucher->is_claimed = true;
        $voucher->save();

        \DB::table('voucher_packages')
            ->where('voucher_id', $voucher->id)
            ->update(['card_id' => $card->id]);

        $voucher->tracking_visits()->whereNull('claimed_at')->update(['claimed_at' => Carbon::now()]);

        if ($user->subscribed) {
            $this->info('Sending verification email');
            $this->userService->sendVerificationEmail($user);
        }
    }
}
