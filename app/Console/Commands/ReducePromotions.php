<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Card;
use App\User;
use App\Lib\Embed\EmbedWrapper;
use GuzzleHttp\Command\Exception\CommandClientException;

class ReducePromotions extends LoggedCommand
{
    protected $signature = 'promo:reduce';

    protected $description = 'Take back bonus points if not used during expiraton period.';

    public function handle()
    {
        if (!config('capabilities.promotions')) {
            throw new \Exception('This command is not supported');
        }
        
        try {
            $today = Carbon::today();

            $users = User::with(['promotions' => function ($query) use ($today) {
                $query->where('user_promotions.is_expired', false)->where('user_promotions.expire_at', '<=', $today);
            }])->get();

            foreach ($users as $user) {
                foreach ($user->promotions as $promotion) {
                    $card = Card::find($promotion->pivot->card_id);

                    $this->info('Promotion "' . $promotion->name . '" expired for card ' . $card->number);

                    if (!$card->visits()->where('visit_at', '>=', $promotion->pivot->promoted_at)->exists()) {
                        $this->info('Promotion not used! Removing ' . $promotion->pivot->promoted_balance);
                        
                        $removeResult = $this->removeBalanceFromCard(
                            $card,
                            $promotion->pivot->promoted_balance,
                            $promotion->pivot->id,
                            'Promotion expired'
                        );
                        
                        if ($removeResult) {
                            $card->retrieveBalance();
                        } else {
                            continue;
                        }
                    }

                    $promotion->pivot->is_expired = true;
                    $promotion->pivot->save();
                }
            }
        } catch (\Exception $e) {
            \Log::error($e);
            $this->error($e);
        } finally {
            EmbedWrapper::releaseAllClients();
        }
    }

    private function removeBalanceFromCard($card, $balance, $reference_id, $comment)
    {
        $embed = EmbedWrapper::obtainClient();

        $response = $embed->cardBalanceReversal(['reverse' => [
            'card_id' => $card->number,
            'bonus_reverse' => $balance,
            'reference_id' => $reference_id,
            'comment' => $comment,
            'cashier_name' => config('settings.cashier_name'),
        ]]);

        $success = $response['reverse']['result'] === 'success';

        if (!$success) {
            $this->error($response['reverse']['fail_reason']);
        }

        return $success;
    }
}
