<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Daily extends Command
{
    protected $signature = 'daily';

    protected $description = 'Run daily routines: import cards and guests, send opt-in emails, issue new and reduce expired promotions.';

    public function handle()
    {
        if (config('capabilities.embed')) {
            $this->info('Embed import');
            $this->call('embed:import');
        }

        if (config('capabilities.intercard')) {
            $this->info('Intercard import');
            $this->call('intercard:import');
        }

        if (config('capabilities.opt_in')) {
            $this->info('Opt-in emails');
            $this->call('promo:optin');
        }

        if (config('capabilities.promotions')) {
            $this->info('Run promotions');
            $this->call('promo:run');
            $this->info('Expired promotions');
            $this->call('promo:reduce');
        }
    }
}
