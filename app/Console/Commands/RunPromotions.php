<?php

namespace App\Console\Commands;

use App\Promotion;
use Carbon\Carbon;
use App\Notifications\PromotionNotification;
use App\Card;
use App\User;
use App\Lib\Embed\EmbedWrapper;
use GuzzleHttp\Command\Exception\CommandClientException;
use App\Services\Cards\CardProviderInterface;

class RunPromotions extends LoggedCommand
{
    protected $signature = 'promo:run {promotion? : The ID of the promotion}';

    protected $description = 'Run promotions, promote eligible cards and send notifications.';

    private $today;

    public function __construct()
    {
        parent::__construct();
        $this->today = Carbon::today();
    }

    public function handle(CardProviderInterface $cardProvider)
    {
        if (!config('capabilities.promotions')) {
            throw new \Exception('This command is not supported');
        }
        
        try {
            $promotions = $this->getPromotions();

            foreach ($promotions as $promotion) {
                if ($promotion->product->cost > 0) {
                    throw new \Exception('Promotion has product with cost greater than zero');
                }

                $class = 'App\\Services\\Promotions\\' . studly_case($promotion->type) . 'PromotionService';
                $promotionTypeService = new $class();

                if ($users = $promotionTypeService->getEligibleUsers($promotion, $this->today)) {
                    $this->promoteUsers($users, $promotion, $cardProvider);
                }

                if ($preliminaryDays = $promotionTypeService->getPreliminaryDays($promotion, $this->today)) {
                    $promotionDate = (clone $this->today)->addDays($preliminaryDays);
                    if ($users = $promotionTypeService->getEligibleUsers($promotion, $promotionDate, true)) {
                        $this->prenotifyUsers($users, $promotion, $promotionDate);
                    }
                }
            }
        } catch (\Exception $e) {
            \Log::error($e);
            $this->error($e);
        }
    }

    private function getPromotions()
    {
        $query = Promotion::where(function ($query) {
            $query->whereNull('start_at')->orWhere('start_at', '<=', $this->today);
        })
        ->where(function ($query) {
            $query->whereNull('expire_at')->orWhere('expire_at', '>=', $this->today);
        });

        $promotionId = $this->argument('promotion');
        if ($promotionId) {
            $query = $query->where('id', $promotionId);
        } else {
            $query = $query->where('is_enabled', true);
        }

        return $query->get();
    }

    private function promoteUsers($users, $promotion, $cardProvider)
    {
        $expireAt = $promotion->validForDate($this->today);

        foreach ($users as $user) {
            $this->info('User #' . $user->id . ' eligible for promotion "' . $promotion->name . '"');
            if ($card = $user->primary_card()) {
                $this->info('Primary card: ' . $card->number);
                
                $cardProvider->addProductsToCard($card, [$promotion->product]);

                $preliminaryPromotion = $user->promotions()
                    ->wherePromotionId($promotion->id)
                    ->wherePivot('is_preliminary', true)
                    ->wherePivot('promoted_at', $this->today)
                    ->first();

                if ($preliminaryPromotion) {
                    $preliminaryPromotion->pivot->fill([
                        'is_preliminary' => false,
                        'expire_at' => $expireAt,
                        'promoted_balance' => $promotion->product->play_bonus_added,
                        'card_id' => $card->id
                    ]);
                    $preliminaryPromotion->pivot->save();
                } else {
                    $user->promotions()->attach($promotion, [
                        'promoted_at' => $this->today,
                        'expire_at' => $expireAt,
                        'promoted_balance' => $promotion->product->play_bonus_added,
                        'card_id' => $card->id
                    ]);
                }

                $card->retrieveBalance();

                $card->notify(new PromotionNotification($promotion));
            } else {
                $this->info('User doesn\'t have primary card');
            }
        }
    }

    private function prenotifyUsers($users, $promotion, $promotionDate)
    {
        foreach ($users as $user) {
            $this->info('User #' . $user->id . ' eligible for promotion preliminary notification "' . $promotion->name . '"');
            if ($card = $user->primary_card()) {
                $this->info('Primary card: ' . $card->number);
                
                $user->promotions()->attach($promotion, [
                    'is_preliminary' => true,
                    'card_id' => $card->id,
                    'promoted_at' => $promotionDate,
                ]);
                
                $card->notify(new PromotionNotification($promotion, true));
            } else {
                $this->info('User doesn\'t have primary card');
            }
        }
    }
}
