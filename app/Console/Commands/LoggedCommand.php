<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;

abstract class LoggedCommand extends Command
{
    protected $logger;

    public function __construct()
    {
        parent::__construct();
    
        $streamHandler = new StreamHandler(storage_path('logs/command.log'));
        $streamHandler->setFormatter(new LineFormatter(null, null, true, true));

        $this->logger = new Logger('command');
        $this->logger->pushHandler($streamHandler);
    }

    public function info($string, $verbosity = null)
    {
        $this->logger->info($string);
        return parent::info($string, $verbosity);
    }

    public function comment($string, $verbosity = null)
    {
        $this->logger->info($string);
        return parent::comment($string, $verbosity);
    }

    public function question($string, $verbosity = null)
    {
        $this->logger->info($string);
        return parent::question($string, $verbosity);
    }

    public function error($string, $verbosity = null)
    {
        $this->logger->error($string);
        return parent::error($string, $verbosity);
    }

    public function warn($string, $verbosity = null)
    {
        $this->logger->warn($string);
        return parent::warn($string, $verbosity);
    }
}
