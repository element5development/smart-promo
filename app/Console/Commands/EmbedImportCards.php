<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Card;
use App\Guest;
use App\Visit;
use App\Location;
use App\Lib\Embed\EmbedWrapper;
use App\Services\Cards\EmbedCardProvider;
use App\Services\GuestService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use DB;
use GuzzleHttp\Command\Exception\CommandClientException;

class EmbedImportCards extends BaseEmbedImport
{
    const ROWS_PER_PAGE = 100;

    protected $signature = 'embed:import:cards';

    protected $description = 'Import card history and convert into visits';
 
    public function __construct(EmbedCardProvider $embedCardProvider)
    {
        parent::__construct('import_from_date', 'cards_imported_to_date', $embedCardProvider);
    }

    public function import()
    {
        $cards = $this->getChangedCards();
        $this->importCards($cards);
    }

    private function importCards($cards)
    {
        foreach ($cards as $number => $cardData) {
            if (count($cardData['guests'])) {
                $card = $this->getCard($cardData);

                $this->attachGuests($card, $cardData['guests']);

                if ($card->user_id) {
                    $history = $this->getHistoryItems($card);
        
                    foreach ($history as $item) {
                        $this->processHistoryItem($card, Carbon::parse($item['date_time']));
                    }

                    $this->updateCardLocation($card, $history);

                    $card->next_history_import_from = $this->toDateTime;
                    $card->save();
                }
            }
        }
    }

    private function getChangedCards()
    {
        $allCards = [];

        foreach (Location::all() as $location) {
            $cards = $this->getLocationChangedCards($location);

            foreach ($cards as $id => $card) {
                if (isset($allCards[$id])) {
                    if ($card['last_modified'] > $allCards[$id]) {
                        $card['guests'] = $allCards[$id]['guests'];
                        $allCards[$id] = $card;
                    }
                } else {
                    $allCards[$id] = $card;
                    $allCards[$id]['guests'] = [];
                }

                if ($card['guest']) {
                    $allCards[$id]['guests'][$location->id] = $card['guest'];
                }
            }
        }

        $this->info('Overall changed cards: ' . count($allCards));

        return $allCards;
    }

    private function getLocationChangedCards($location)
    {
        $query = [
            'from_datetime' => $this->fromDate->toDateTimeString(),
            'to_datetime' => $this->toDateTime->toDateTimeString(),
            'page' => 0,
            'page_size' => self::ROWS_PER_PAGE,
        ];

        $embed = EmbedWrapper::obtainClient($location->getEmbedConfig());

        $allModifiedCards = [];
        do {
            $this->info('Retrieving page ' . $query['page'] . " for {$location->title}");
            $modifiedCards = $embed->modifiedCards($query);
            
            if (is_array($modifiedCards['results']) && count($modifiedCards['results'])) {
                array_push($allModifiedCards, ...$modifiedCards['results']);
            }

            $query['page']++;

            $this->info('Total pages: ' . $modifiedCards['total_pages'] . " for {$location->title}");
        } while ($modifiedCards['total_pages'] > $query['page']);

        $cardNumbers = array_map(function ($c) { return $c['id']; }, $allModifiedCards);
        $changedCards = array_combine($cardNumbers, $allModifiedCards);

        $this->info("Card changes at {$location->title}: " . count($allModifiedCards));

        $this->info("Cards changed at {$location->title}: " . count($changedCards));

        return $changedCards;
    }

    private function getCard($cardData)
    {
        $this->info('Card: ' . $cardData['id']);

        $card = Card::firstOrNew(['number' => $cardData['id']]);

        $card->barcode = $cardData['barcode'];

        $card->setBalance($cardData);
        
        if (!$card->exists) {
            $this->info('New card!');
        }

        $card->save();

        return $card;
    }

    private function attachGuests($card, $guests)
    {
        foreach ($guests as $locationId => $guestDetails) {
            $this->attachGuest($card, $guestDetails, $locationId);
        }
    }

    private function attachGuest($card, $guestDetails, $locationId)
    {
        $guest = $this->embedCardProvider->getUpdatedGuest($guestDetails, $locationId);

        $card->guests()->syncWithoutDetaching([$guest->id]);

        if ($userId = $guest->cards()->whereNotNull('user_id')->value('user_id')) {
            $card->user_id = $userId;
            $card->save();
        }
    }

    private function getHistoryItems($card)
    {
        $number = $card->number;

        $this->info('Retrieving card history for ' . $number);

        $allHistory = [];

        foreach (Location::all() as $location) {
            $embed = EmbedWrapper::obtainClient($location->getEmbedConfig());

            try {
                $fromDate = Carbon::parse($card->next_history_import_from ? $card->next_history_import_from : config('settings.import_from_date'));
                $history = $embed->cardHistory(['id' => $number, 'from_date' => $fromDate->toDateTimeString()]);
                $history = $history->toArray();
            } catch (CommandClientException $e) {
                $this->error($e->getMessage());
                $history = [];
            }

            foreach ($history as &$item) {
                $item['location_id'] = $location->id;
            }

            $this->info("History items for {$number} ar {$location->title}: " . count($history));

            $storeHistory = [];
            foreach ($history as $item) {
                if ($this->isStoreHistoryItem($item)) {
                    $storeHistory[] = $item;
                }
            }

            $this->info("Visit-type history items for {$number} ar {$location->title}: " . count($storeHistory));

            if (count($storeHistory)) {
                array_push($allHistory, ...$storeHistory);
            }
        }

        usort($allHistory, function ($a, $b) {
            return $a['date_time'] <=> $b['date_time'];
        });

        $this->info('Overall history items: ' . count($allHistory));

        return $allHistory;
    }

    private function isStoreHistoryItem($item)
    {
        if (in_array($item['type'], ['PAYMENT', 'PLAY'])) {
            return true;
        }

        if (in_array($item['type'], ['SALEC', 'SALECP']) && !starts_with($item['description'], config('settings.online_sale_product_prefix'))) {
            return true;
        }
    }

    private function processHistoryItem($card, $datetime)
    {
        $date = (clone $datetime)->startOfDay();

        $visit = $card->visits()->firstOrNew(['visit_at' => $date]);
        if ($visit->exists) {
            $this->info('Extend existing visit: ' . $date . ' - ' . $datetime);
            $visit->last_at = $datetime;
            $visit->save();
        } else {
            $previousDayVisit = $card->visits()->where(['visit_at' => (clone $date)->subDay()])->first();
            if ($previousDayVisit && $previousDayVisit->last_at->diffInHours($datetime) < config('settings.visit_gap_between_days')) {
                $this->info('Extend previous day visit: ' . $previousDayVisit->visit_at . ' - ' . $datetime);
                $previousDayVisit->last_at = $datetime;
                $previousDayVisit->save();
            } else {
                $this->info('Create new visit: ' . $date . ' - ' . $datetime);
                $visit->first_at = $visit->last_at = $datetime;
                $visit->save();
            }
        }
    }

    private function updateCardLocation($card, $history)
    {
        if (count($history)) {
            $lastLocationId = $history[count($history) - 1]['location_id'];
            if ($lastLocationId != $card->location_id) {
                $card->location_id = $lastLocationId;
                $card->save();
            }

            if ($card->user) {
                $this->info("Registered card {$card->number} location changed, updating guest details");
                $this->embedCardProvider->addGuestDetails($card, $card->user, $card->location_id);
            }
        }
    }
}
