<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class EmbedImport extends Command
{
    protected $signature = 'embed:import';

    protected $description = 'Import card and guest history';

    public function handle()
    {
        if (!config('capabilities.embed')) {
            throw new \Exception('This command is not supported');
        }
        
        $this->info('Import cards');
        $this->call('embed:import:cards');
        $this->info('Import guests');
        $this->call('embed:import:guests');
        $this->info('Import products');
        $this->call('embed:import:products');
    }
}
