<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Lib\Embed\EmbedWrapper;
use App\Guest;
use App\Setting;
use App\Location;

abstract class BasePeriodImport extends LoggedCommand
{
    protected $fromDate;
    protected $toDateTime;

    private $importedToSetting;
    private $importFromSetting;

    public function __construct($importFromSetting, $importedToSetting)
    {
        parent::__construct();
        $this->importedToSetting = $importedToSetting;
        $this->importFromSetting = $importFromSetting;
    }

    protected function initDates($timezone = null)
    {
        if ($importedTo = config('settings.' . $this->importedToSetting)) {
            $fromDate = $importedTo;
        } else {
            if ($fromDate = config('settings.' . $this->importFromSetting)) {
                $this->comment('First time import');
            } else {
                $this->error('Cannot start import process because "' . $this->importFromSetting . '" setting not set');
                exit;
            }
        }

        $fromDate = Carbon::parse($fromDate, $timezone);
        $this->fromDate = $fromDate;

        $this->toDateTime = Carbon::now($timezone);
    }

    protected function finishImport()
    {
        Setting::where('key', $this->importedToSetting)->update(['value' => $this->toDateTime]);
    }
}
