<?php

namespace App\Console\Commands;

use App\Location;
use App\Package;
use App\Product;
use App\Lib\Intercard\IntercardWrapper;

class IntercardImportProducts extends LoggedCommand
{
    protected $signature = 'intercard:import:products';

    protected $description = 'Import products (barcode promo)';

    public function handle()
    {
        if (!config('capabilities.intercard')) {
            throw new \Exception('This command is not supported');
        }
        
        $this->info('Import products from main server');

        $remainingProductIds = $this->refreshProducts();
        $removedProductIds = $this->removeProducts($remainingProductIds);
        $this->invalidatePackages($removedProductIds);
    }

    private function refreshProducts()
    {
        $intercard = IntercardWrapper::obtainClient();

        $barcodedPromos = $intercard->getBarcodedPromos();

        $productIds = [];

        foreach ($barcodedPromos as $barcodedPromo) {
            $productIds[] = $barcodedPromo['ID'];

            if (!($product = Product::whereProductId($barcodedPromo['ID'])->first())) {
                $this->info("New product '" . $barcodedPromo['Description'] . "'");
                $product = new Product(['product_id' => $barcodedPromo['ID']]);
            }

            $product->product_name = $barcodedPromo['Description'];
            $product->cost = $barcodedPromo['CashBalance'];
            $product->play_bonus_added = $barcodedPromo['BonusCashBalance'];

            $product->save();
        }

        return $productIds;
    }

    private function removeProducts($remainingProductIds)
    {
        $removedProducts = Product::whereNotIn('product_id', $remainingProductIds);

        $removedProductIds = $removedProducts->pluck('id')->toArray();

        if (count($removedProductIds)) {
            $this->info("Products removed: " . count($removedProductIds));
            $removedProducts->delete();
        }

        return $removedProductIds;
    }

    private function invalidatePackages($removedProductIds)
    {
        $invalidPackages = Package::whereIsEnabled(true)->whereHas('products', function ($q) use ($removedProductIds) {
            $q->whereIn('products.id', $removedProductIds);
        });

        if ($invalidPackages->exists()) {
            $this->info('Packages disabled: ' . $invalidPackages->count());
            $invalidPackages->update('is_enabled', false);
        }
    }
}
