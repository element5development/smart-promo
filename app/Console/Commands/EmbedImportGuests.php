<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Guest;
use App\Visit;
use App\Location;
use App\Lib\Embed\EmbedWrapper;
use App\Services\Cards\EmbedCardProvider;

class EmbedImportGuests extends BaseEmbedImport
{
    protected $signature = 'embed:import:guests';

    protected $description = 'Import guest history and changes';


    public function __construct(EmbedCardProvider $embedCardProvider)
    {
        parent::__construct('cards_imported_to_date', 'guests_imported_to_date', $embedCardProvider);
    }

    public function import()
    {
        foreach (Location::all() as $location) {
            $changedGuests = $this->getChangedGuests($location);

            foreach ($changedGuests as $guestDetails) {
                $this->info('Guest: ' . $guestDetails['id']);
                $guest = $this->embedCardProvider->getUpdatedGuest($guestDetails, $location->id);
            }
        }
    }

    private function getChangedGuests($location)
    {
        $embed = EmbedWrapper::obtainClient($location->getEmbedConfig());

        $changedGuests = $embed->listGuests(['from_datetime' => $this->fromDate->toDateTimeString()]);
        $changedGuests = $changedGuests->toArray();

        $this->info("Guest changes at {$location->title}: " . count($changedGuests));

        usort($changedGuests, function ($a, $b) {
            return $a['last_modified'] <=> $b['last_modified'];
        });

        $uniqueGuests = [];
        foreach ($changedGuests as $guest) {
            $uniqueGuests[$guest['id']] = $guest;
        }

        $this->info("Unique guests changed at {$location->title}: " . count($uniqueGuests));

        return $uniqueGuests;
    }
}
