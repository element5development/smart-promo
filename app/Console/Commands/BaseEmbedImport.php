<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Lib\Embed\EmbedWrapper;
use App\Guest;
use App\Setting;
use App\Location;

abstract class BaseEmbedImport extends BasePeriodImport
{
    const MAX_DAYS = 10;

    protected $fromDate;

    protected $toDateTime;

    protected $embed;

    protected $embedCardProvider;

    const ROWS_PER_PAGE = 100;

    public function __construct($importFromSetting, $importedToSetting, $embedCardProvider)
    {
        parent::__construct($importFromSetting, $importedToSetting);

        $this->embedCardProvider = $embedCardProvider;
    }

    public function handle()
    {
        if (!config('capabilities.embed')) {
            throw new \Exception('This command is not supported');
        }

        $fromDate = Carbon::parse($fromDate, config('services.embed.timezone'));
        $toDateTime = Carbon::now(config('services.embed.timezone'));

        try {
            $this->embed = EmbedWrapper::obtainClient();

            do {
                if ($fromDate->diffInDays($toDateTime) > self::MAX_DAYS) {
                    $this->toDateTime = (clone $fromDate)->addDays(self::MAX_DAYS);
                } else {
                    $this->toDateTime = $toDateTime;
                }
                $this->fromDate = $fromDate;

                $this->info('From date: ' . $fromDate);
                $this->info('To date: ' . $this->toDateTime);
                $this->import();

                Setting::where('key', $this->importedToSetting)->update(['value' => $this->toDateTime]);
                $fromDate = $this->toDateTime;
            } while ($fromDate < $toDateTime);
        } catch (\Exception $e) {
            \Log::error($e);
            throw $e;
        } finally {
            EmbedWrapper::releaseAllClients();
        }
    }
}
