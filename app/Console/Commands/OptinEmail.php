<?php

namespace App\Console\Commands;

use App\Promotion;
use Carbon\Carbon;
use App\Guest;
use App\Notifications\OptinNotification;

class OptinEmail extends LoggedCommand
{
    protected $signature = 'promo:optin';

    protected $description = 'Find new guests and send optin emails.';

    public function handle()
    {
        if (!config('capabilities.opt_in')) {
            throw new \Exception('This command is not supported');
        }

        if (config('settings.is_opt_in_email_enabled')) {
            $notification = new OptinNotification();

            $emails = Guest::whereNull('optin_sent_at')
                ->whereNotNull('email')
                ->whereNotIn('email', function ($query) {
                    $query->select('email')->from('users');
                })
                ->whereNotIn('email', function ($query) {
                    $query->select('email')->from('guests')
                        ->whereNotNull('email')
                        ->whereNotNull('optin_sent_at');
                })
                ->select('email')
                ->distinct()
                ->pluck('email');

            foreach ($emails as $email) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $this->info('Sending opt-in email to ' . $email);
                    try {
                        $guest = Guest::whereEmail($email)->latest('last_modified')->first();
                        
                        $guest->saveVerificationToken();
                        $guest->notify($notification);

                        $guest->update(['optin_sent_at' => Carbon::now()]);
                    } catch (\Exception $e) {
                        $this->error($e->getMessage());
                    }
                }
            }
        } else {
            $this->comment('Opt-in email sending is disabled');
        }
    }
}
