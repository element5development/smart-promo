<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Promotion;

class ListPromotions extends Command
{
    protected $signature = 'promo:list';

    protected $description = 'List all promotions and their IDs.';

    public function handle()
    {
        if (!config('capabilities.promotions')) {
            throw new \Exception('This command is not supported');
        }
        
        $promotions = Promotion::all(['id', 'name'])->toArray();
        $this->table(['ID', 'Name'], $promotions);
    }
}
