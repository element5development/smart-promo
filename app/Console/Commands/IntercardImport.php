<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class IntercardImport extends Command
{
    protected $signature = 'intercard:import';

    protected $description = 'Import barcode promos';

    public function handle()
    {
        if (!config('capabilities.intercard')) {
            throw new \Exception('This command is not supported');
        }
        
        $this->info('Import products');
        $this->call('intercard:import:products');
    }
}
