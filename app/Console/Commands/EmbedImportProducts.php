<?php

namespace App\Console\Commands;

use App\Product;
use App\Location;
use App\Package;
use App\Promotion;
use App\Lib\Embed\EmbedWrapper;

class EmbedImportProducts extends LoggedCommand
{
    protected $signature = 'embed:import:products';

    protected $description = 'Import products (mobile templates)';

    public function handle()
    {
        if (!config('capabilities.embed')) {
            throw new \Exception('This command is not supported');
        }
        
        try {
            $this->info('Import products from main server');
            $this->handleMainServer();

            foreach (Location::all() as $location) {
                $this->info("Import products from {$location->title}");
                $this->handleLocationServer($location);
            }
        } finally {
            EmbedWrapper::releaseAllClients();
        }
    }

    private function handleMainServer()
    {
        $embed = EmbedWrapper::obtainClient();
        $productScope = Product::whereNull('location_id');
        
        $remainingProductIds = $this->refreshProducts($embed, $productScope);
        $this->removeProducts($productScope, $remainingProductIds);
    }

    private function handleLocationServer($location)
    {
        $embed = EmbedWrapper::obtainClient($location->getEmbedConfig());
        $productScope = Product::whereLocationId($location->id);
        
        $remainingProductIds = $this->refreshProducts($embed, $productScope, $location->id);
        $this->removeProducts($productScope, $remainingProductIds);
    }

    private function refreshProducts($embed, $productScope, $locationId = null)
    {
        $templates = $embed->availableProducts();

        $productIds = [];

        $prefix = config('settings.online_sale_product_prefix');

        foreach ($templates as $template) {
            foreach ($template['buttons'] as $button) {
                $embedProduct = $button['product'];

                if (empty($prefix) || starts_with($embedProduct['product_name'], $prefix)) {
                    if ($embedProduct['active']) {
                        $productIds[] = $embedProduct['id'];

                        if (!($product = (clone $productScope)->whereProductId($embedProduct['id'])->first())) {
                            $this->info("New product '" . $embedProduct['product_name'] . "'");
                            $product = new Product(['location_id' => $locationId, 'product_id' => $embedProduct['id']]);
                        }

                        $product->product_name = $embedProduct['product_name'];
                        $product->product_type = $embedProduct['product_type'];
                        $product->cost = $embedProduct['product_cost']['value'];
                        $product->play_bonus_added = isset($embedProduct['play_bonus_added']) ? $embedProduct['play_bonus_added']['currency'] : 0;
                        $product->tax = isset($embedProduct['product_tax']) ? $embedProduct['product_tax']['value'] : 0;

                        $product->save();
                    }
                }
            }
        }

        return $productIds;
    }

    private function removeProducts($productScope, $remainingProductIds)
    {
        $removedProducts = (clone $productScope)->whereNotIn('product_id', $remainingProductIds);

        $removedProductIds = $removedProducts->pluck('id')->toArray();

        if (count($removedProductIds)) {
            $this->info("Products removed: " . count($removedProductIds));
            $removedProducts->delete();
        }

        $this->invalidatePackages($removedProductIds);

        $this->invalidatePromotions($removedProductIds);

        return $removedProductIds;
    }

    private function invalidatePackages($removedProductIds)
    {
        $invalidPackages = Package::whereIsEnabled(true)->whereHas('products', function ($q) use ($removedProductIds) {
            $q->whereIn('products.id', $removedProductIds);
            $q->whereOr('products.cost', '<=', 0);
        });

        if ($invalidPackages->exists()) {
            $this->info('Packages disabled: ' . $invalidPackages->count());
            $invalidPackages->update('is_enabled', false);
        }
    }

    private function invalidatePromotions($removedProductIds)
    {
        $invalidPromotions = Promotion::whereIsEnabled(true)->whereHas('product', function ($q) use ($removedProductIds) {
            $q->whereIn('products.id', $removedProductIds);
            $q->whereOr('products.cost', '>', 0);
        });

        if ($invalidPromotions->exists()) {
            $this->info('Promotions disabled: ' . $invalidPromotions->count());
            $invalidPromotions->update('is_enabled', false);
        }
    }
}
