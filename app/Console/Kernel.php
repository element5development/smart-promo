<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\RunPromotions;
use App\Console\Commands\ListPromotions;
use App\Console\Commands\EmbedImport;
use App\Console\Commands\EmbedImportCards;
use App\Console\Commands\EmbedImportGuests;
use App\Console\Commands\EmbedImportProducts;
use App\Console\Commands\IntercardImport;
use App\Console\Commands\IntercardImportProducts;
use App\Console\Commands\IntercardImportRedeemedBarcodes;
use App\Console\Commands\ReducePromotions;
use App\Console\Commands\OptinEmail;
use App\Console\Commands\Daily;
use App\Console\Commands\EveryMinute;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        RunPromotions::class,
        ListPromotions::class,
        EmbedImport::class,
        EmbedImportCards::class,
        EmbedImportGuests::class,
        EmbedImportProducts::class,
        IntercardImport::class,
        IntercardImportProducts::class,
        IntercardImportRedeemedBarcodes::class,
        ReducePromotions::class,
        OptinEmail::class,
        Daily::class,
        EveryMinute::class,
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule
            ->command('daily')
            ->dailyAt('6:00');

        $schedule
            ->command('every:minute')
            ->everyMinute();
    }

    protected function commands()
    {
        // $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
