<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $fillable = ['card_id', 'visit_at', 'first_at', 'last_at'];
    
    protected $dates = ['visit_at', 'first_at', 'last_at'];

    public function card()
    {
        return $this->belongsTo(Card::class);
    }
}
