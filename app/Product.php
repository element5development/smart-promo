<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Product extends Model
{
    protected $fillable = ['location_id', 'product_id', 'product_name', 'product_type', 'cost', 'play_bonus_added', 'tax'];

    public static function firstBonusProduct()
    {
        return self::find(config('settings.first_bonus_product_id'));
    }

    public function scopeNoLocation($query)
    {
        return $query->whereNull('location_id');
    }
}
