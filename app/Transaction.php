<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Transaction extends Model
{
    use CrudTrait;
    
    protected $fillable = ['user_id', 'transactionable_type', 'transactionable_id', 'amount', 'transaction_id', 'account_type', 'cc_number', 'response_data'];

    protected $casts = [
        'response_data' => 'array',
    ];

    public function transactionable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function creationData($user, $data, $amount, $digits)
    {
        return [
            'user_id' => $user ? $user->id : null,
            'amount' => $amount,
            'transaction_id' => $data['transaction_id'],
            'account_type' => $data['account_type'],
            'cc_number' => substr($digits, -4),
            'response_data' => [
                'response_code' => $data['response_code'],
                'auth_code' => $data['auth_code'],
                'message_code' => $data['message_code'],
                'message_description' => $data['message_description'],
            ]
        ];
    }
}
