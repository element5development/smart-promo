<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use CrudTrait;

    protected $fillable = ['value'];

    public function capabilities()
    {
        return $this->belongsToMany(Capability::class, 'capability_settings');
    }
}
