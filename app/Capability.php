<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Capability extends Model
{
    use CrudTrait;

    protected $fillable = ['key', 'name', 'description', 'is_enabled'];

    public function scopeEnabled($query)
    {
        return $query->where('is_enabled', true);
    }

    public function settings()
    {
        return $this->belongsToMany(Setting::class, 'capability_settings');
    }
}
