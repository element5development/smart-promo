<?php

namespace App\Lib\Intercard;

use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\Description;
use GuzzleHttp\Command\Guzzle\GuzzleClient;

class IntercardWrapper
{
    protected static $issuedClients = [];

    public static function obtainClient($config = null)
    {
        if (!$config) {
            $config = self::getDefaultConfig();
        }

        $connectionKey = base64_encode($config['uri']);

        if (isset(self::$issuedClients[$connectionKey])) {
            self::$issuedClients[$connectionKey]['count']++;
            return self::$issuedClients[$connectionKey]['client'];
        }

        $client = self::getClient();

        $description = new Description([
            'baseUri' => $config['uri'],
            'operations' => [
                'memberByEmail' => [
                    'httpMethod' => 'GET',
                    'uri' => 'WS_ProfileManagement/api/Customer/{corpid}/Email/{email}',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'corpid' => ['location' => 'uri', 'type' => 'string',
                            'default' => config('services.intercard.corpid')],
                        'email' => ['location' => 'uri', 'type' => 'string', 'required' => true],
                    ]
                ],
                'getBarcodedPromos' => [
                    'httpMethod' => 'GET',
                    'uri' => 'WS_ProfileManagement/api/BarcodedPromo/{corpid}/BarcodedPromos',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'corpid' => ['location' => 'uri', 'type' => 'string',
                            'default' => config('services.intercard.corpid')],
                    ]
                ],
                'getBarcodes' => [
                    'httpMethod' => 'GET',
                    'uri' => 'WS_ProfileManagement/api/BarcodePromo/{cid}/BarcodePromoID/{barcodepromoid}/Barcodes/Qty/{qty}',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'cid' => ['location' => 'uri', 'type' => 'string',
                            'default' => config('services.intercard.cid')],
                        'barcodepromoid' => ['location' => 'uri', 'type' => 'string', 'required' => true],
                        'qty' => ['location' => 'uri', 'type' => 'string', 'default' => 1],
                    ]
                ],
                'getRedeemedBarcodes' => [
                    'httpMethod' => 'GET',
                    'uri' => 'WS_ProfileManagement/api/BarcodedPromo/{corpid}/BarcodesRedeemed/BarcodedPromoID/{barcodepromoid}/Barcodes?Start={from}&End={to}',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'corpid' => ['location' => 'uri', 'type' => 'string',
                            'default' => config('services.intercard.corpid')],
                        'barcodepromoid' => ['location' => 'uri', 'type' => 'string', 'required' => true],
                        'from' => ['location' => 'uri', 'type' => 'string', 'required' => true],
                        'to' => ['location' => 'uri', 'type' => 'string', 'required' => true],
                    ]
                ],
                'getMemberByAccountNumber' => [
                    'httpMethod' => 'GET',
                    'uri' => 'WS_ProfileManagement/api/Customer/{corpid}/Account/{account_number}',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'corpid' => ['location' => 'uri', 'type' => 'string',
                            'default' => config('services.intercard.corpid')],
                        'account_number' => ['location' => 'uri', 'type' => 'string', 'required' => true],
                    ]
                ],
                'oneStepNewMember' => [
                    'httpMethod' => 'POST',
                    'uri' => 'WS_ProfileManagement/api/Customer',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'corpid' => ['location' => 'json', 'type' => 'string',
                            'default' => config('services.intercard.corpid'), 'sentAs' => 'CorpID'],
                        'user' => ['location' => 'json', 'type' => 'array', 'required' => true, 'sentAs' => 'User'],
                    ]
                ],
                'addCard' => [
                    'httpMethod' => 'POST',
                    'uri' => 'WS_ProfileManagement/api/Customer/{customer_id}/AddCard',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'customer_id' => ['location' => 'uri', 'type' => 'string', 'required' => true],
                        'corpid' => ['location' => 'json', 'type' => 'string',
                            'default' => config('services.intercard.corpid'), 'sentAs' => 'CorpID'],
                        'account_number' => ['location' => 'json', 'type' => 'string', 'required' => true, 'sentAs' => 'Account'],
                    ]
                ],
            ],
            'models' => [
                'getResponse' => [
                    'type' => 'object',
                    'additionalProperties' => [
                        'location' => 'json'
                    ]
                ]
            ],
        ]);
        
        $client = new GuzzleClient($client, $description);
        $client->connectionKey = $connectionKey;

        self::$issuedClients[$connectionKey] = ['count' => 1, 'client' => $client];

        return $client;
    }

    public static function getDefaultConfig()
    {
        return [
            'uri' => config('services.intercard.base_uri'),
        ];
    }

    private static function getClient()
    {
        return new Client([
            'verify' => config('services.intercard.ssl_verify'),
            // 'debug' => config('app.debug'),
        ]);
    }
}
