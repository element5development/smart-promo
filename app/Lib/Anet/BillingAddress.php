<?php

namespace App\Lib\Anet;

class BillingAddress
{
    public $address;
    public $city;
    public $state;
    public $zip;
    public $country;

    public static function fromRequest($request)
    {
        $ba = new BillingAddress();
        $ba->address = $request->address;
        $ba->city = $request->city;
        $ba->state = $request->state;
        $ba->zip = $request->zip;
        $ba->country = $request->country;
        return $ba;
    }
}
