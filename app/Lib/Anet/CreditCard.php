<?php

namespace App\Lib\Anet;

class CreditCard
{
    public $number;
    public $expirationYear;
    public $expirationMonth;
    public $code;
    public $firstName;
    public $lastName;

    public static function fromRequest($request)
    {
        $cc = new CreditCard();
        $cc->number = $request->cc_number;
        $cc->expirationYear = $request->cc_expiration_year;
        $cc->expirationMonth = $request->cc_expiration_month;
        $cc->code = $request->cc_code;
        $cc->firstName = $request->cc_first_name;
        $cc->lastName = $request->cc_last_name;
        return $cc;
    }
}
