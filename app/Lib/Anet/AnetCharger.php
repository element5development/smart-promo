<?php

namespace App\Lib\Anet;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use net\authorize\api\constants\ANetEnvironment;

class AnetCharger
{
    public static function charge(CreditCard $creditCard, BillingAddress $billingAddress, $email, $amount)
    {
        if ($amount <= 0) {
            throw new \Exception('Amount must be greater than zero');
        }

        define('AUTHORIZENET_LOG_FILE', storage_path('logs/authorize-net.log'));

        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(config('services.anet.login_id'));
        $merchantAuthentication->setTransactionKey(config('services.anet.transaction_key'));
        
        $creditCardType = new AnetAPI\CreditCardType();
        $creditCardType->setCardNumber($creditCard->number);
        $ccExpiration = $creditCard->expirationYear . '-' . str_pad($creditCard->expirationMonth, 2, '0', STR_PAD_LEFT);

        $creditCardType->setExpirationDate($ccExpiration);
        $creditCardType->setCardCode($creditCard->code);

        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCardType);

        $order = new AnetAPI\OrderType();
        // $order->setInvoiceNumber("10101");
        // $order->setDescription("Golf Shirts");
        
        $customerAddress = new AnetAPI\CustomerAddressType();
        $customerAddress->setFirstName($creditCard->firstName);
        $customerAddress->setLastName($creditCard->lastName);
        // $customerAddress->setCompany("Souveniropolis");
        $customerAddress->setAddress($billingAddress->address);
        $customerAddress->setCity($billingAddress->city);
        $customerAddress->setState($billingAddress->state);
        $customerAddress->setZip($billingAddress->zip);
        $customerAddress->setCountry($billingAddress->country);

        $customerData = new AnetAPI\CustomerDataType();
        $customerData->setType('individual');
        // $customerData->setId("99999456654");
        $customerData->setEmail($email);

        // $duplicateWindowSetting = new AnetAPI\SettingType();
        // $duplicateWindowSetting->setSettingName("duplicateWindow");
        // $duplicateWindowSetting->setSettingValue("60");

        // $merchantDefinedField1 = new AnetAPI\UserFieldType();
        // $merchantDefinedField1->setName("customerLoyaltyNum");
        // $merchantDefinedField1->setValue("1128836273");
        // $merchantDefinedField2 = new AnetAPI\UserFieldType();
        // $merchantDefinedField2->setName("favoriteColor");
        // $merchantDefinedField2->setValue("blue");

        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setOrder($order);
        $transactionRequestType->setPayment($paymentOne);
        $transactionRequestType->setBillTo($customerAddress);
        $transactionRequestType->setCustomer($customerData);
        // $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
        // $transactionRequestType->addToUserFields($merchantDefinedField1);
        // $transactionRequestType->addToUserFields($merchantDefinedField2);

        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        // $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);

        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(config('services.anet.production') ? ANetEnvironment::PRODUCTION : ANetEnvironment::SANDBOX);

        if ($response != null) {
            $tresponse = $response->getTransactionResponse();
            if ($response->getMessages()->getResultCode() == 'Ok') {
                if ($tresponse != null && $tresponse->getMessages() != null) {
                    return [
                        'transaction_id' => $tresponse->getTransId(),
                        'response_code' => $tresponse->getResponseCode(),
                        'auth_code' => $tresponse->getAuthCode(),
                        'message_code' => $tresponse->getMessages()[0]->getCode(),
                        'message_description' => $tresponse->getMessages()[0]->getDescription(),
                        'account_type' => $tresponse->getAccountType(),
                    ];
                } else {
                    if ($tresponse != null && $tresponse->getErrors() != null) {
                        $errorMessage = $tresponse->getErrors()[0]->getErrorText();
                    } {
                        $errorMessage = 'Unknown payment error';
                    }
                }
            } else {
                if ($tresponse != null && $tresponse->getErrors() != null) {
                    $errorMessage = $tresponse->getErrors()[0]->getErrorText();
                } else {
                    $errorMessage = $response->getMessages()->getMessage()[0]->getText();
                }
            }

            if ($tresponse) {
                \Log::warning(print_r($tresponse, true));
            }
        } else {
            $errorMessage = 'Unknown payment error';
        }

        throw new \Exception($errorMessage);
    }
}
