<?php

namespace App\Lib;

class Lock
{
    const SleepMicro = 100000;

    public static function acquire($name, $timeout = 10)
    {
        $filename = storage_path("app/locks/$name.lock");
        
        if (!file_exists($filename)) {
            file_put_contents($filename, '');
        }

        $lockFile = fopen($filename, 'w+');

        if ($timeout < 1) {
            $locked = flock($lockFile, LOCK_EX);
        } else {
            $timePassed = 0;
            $timeout *= 1000000;
            $locked = true;
            while (!flock($lockFile, LOCK_EX | LOCK_NB, $blocking)) {
                $timePassed += self::SleepMicro;
                if ($blocking && $timePassed <= $timeout) {
                    usleep(self::SleepMicro);
                } else {
                    fclose($lockFile);
                    return false;
                }
            }
        }

        return $lockFile;
    }

    public static function release($lockFile)
    {
        if ($lockFile) {
            flock($lockFile, LOCK_UN);
            fclose($lockFile);
        }
    }
}
