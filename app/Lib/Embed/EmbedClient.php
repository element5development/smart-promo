<?php

namespace App\Lib\Embed;

use GuzzleHttp\Command\Guzzle\GuzzleClient;

class EmbedClient extends GuzzleClient
{
    public $lockFile;

    public function tryVerifyCvc($card, $cvc)
    {
        if (!config('services.embed.disable_cvv_check')) {
            $credentials = ['cvc' => $cvc];
            
            if (strlen($card) == 19) {
                $credentials['card_id'] = $card;
            } else {
                $credentials['card_marking'] = $card;
            }
            
            $verificationResponse = $this->verifyCvc(['verify_cvc' => $credentials]);
            $verification = $verificationResponse['verify_cvc'];
            if ($verification['result'] != 'success') {
                throw new \Exception($verification['fail_reason']);
            }
        }
    }

    public function tryCardDetails($card)
    {
        return $this->cardDetails(['id' => $card]);
    }
}
