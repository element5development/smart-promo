<?php

namespace App\Lib\Embed;

use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\Description;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use Cache;
use App;
use Auth;
use App\Lib\Lock;
use App\Thumbprint;
use Carbon\Carbon;

class EmbedWrapper
{
    const AnonymousCashierId = -1;
    const CreditCardTenderType = 2;

    protected static $issuedClients = [];

    public static function obtainClient($config = null)
    {
        if (!$config) {
            $config = self::getDefaultConfig();
        }

        $connectionKey = self::getConnectionKey($config);

        if (isset(self::$issuedClients[$connectionKey])) {
            self::$issuedClients[$connectionKey]['count']++;
            return self::$issuedClients[$connectionKey]['client'];
        }

        $thumbprint = self::getThumbprint($config, $connectionKey);

        self::$issuedClients[$connectionKey] = ['count' => 1, 'thumbprint_id' => $thumbprint->id];

        $accessToken = self::obtainAccessToken($config, $thumbprint->thumbprint, $connectionKey);

        $client = self::getClient('Bearer ' . $accessToken, $thumbprint->thumbprint);

        $description = self::getGuzzleDescription($config);
        
        $client = new EmbedClient($client, $description);
        $client->connectionKey = $connectionKey;

        self::$issuedClients[$connectionKey]['client'] = $client;

        return $client;
    }

    public static function getDefaultConfig()
    {
        return [
            'uri' => config('services.embed.base_uri'),
            'key' => config('services.embed.key'),
            'secret' => config('services.embed.secret'),
            'thumbprints' => config('services.embed.thumbprints'),
        ];
    }

    private static function getConnectionKey($config)
    {
        return md5(base64_encode($config['uri'] . ':' . $config['key'] . ':' . $config['secret']));
    }

    private static function getThumbprint($config, $connectionKey)
    {
        if (!($lockFile = Lock::acquire($connectionKey))) {
            throw new \Exception('Lock timeout for pool of ' . $config['uri']);
        }
    
        try {
            $thumbprints = Thumbprint::whereUri($config['uri'])->where('key', $config['key'])->whereSecret($config['secret'])
                ->whereIn('thumbprint', $config['thumbprints'])
                ->latest('use_count');
    
            $remainingThumbprints = $config['thumbprints'];
            foreach ($thumbprints->get() as $thumbprint) {
                if (is_null($thumbprint->reserved_at)) {
                    $thumbprint->reserved_at = Carbon::now();
                    $thumbprint->use_count++;
                    $thumbprint->save();

                    return $thumbprint;
                }
    
                $index = array_search($thumbprint->thumbprint, $remainingThumbprints);
                unset($remainingThumbprints[$index]);
            }

            if (count($remainingThumbprints)) {
                $thumbprint = Thumbprint::create([
                    'uri' => $config['uri'],
                    'key' => $config['key'],
                    'secret' => $config['secret'],
                    'thumbprint' => array_values($remainingThumbprints)[0],
                    'reserved_at' => Carbon::now(),
                    'use_count' => 1,
                ]);

                return $thumbprint;
            }

            throw new \Exception('No thumbprints are available in the pool of ' . $config['uri']);
        } finally {
            Lock::release($lockFile);
        }
    }

    private static function obtainAccessToken($config, $thumbprint, $connectionKey)
    {
        $cacheKey = "{$connectionKey}:{$thumbprint}";

        if (Cache::has($cacheKey)) {
            $accessToken = Cache::get($cacheKey);
        } else {
            $keySecretBase64 = base64_encode($config['key'] . ':' . $config['secret']);

            $client = self::getClient('Basic ' . $keySecretBase64, $thumbprint);

            $description = new Description([
                'baseUri' => $config['uri'],
                'operations' => [
                    'authRequestToken' => [
                        'httpMethod' => 'POST',
                        'uri' => 'auth/request_token',
                        'responseModel' => 'getResponse',
                        'parameters' => [
                            'grant_type' => [
                                'type' => 'string',
                                'location' => 'formParam',
                                'required' => true,
                                'default' => 'client_credentials'
                            ]
                        ]
                    ],
                ],
                'models' => [
                    'getResponse' => [
                        'type' => 'object',
                        'additionalProperties' => [
                            'location' => 'json'
                        ]
                    ]
                ]
            ]);
            
            $guzzleClient = new GuzzleClient($client, $description);

            $response = $guzzleClient->authRequestToken();

            $accessToken = $response['access_token'];

            Cache::put($cacheKey, $accessToken, config('cache.expire.embed_access_token'));
        }

        return $accessToken;
    }

    private static function getClient($authorization, $thumbprint)
    {
        return new Client([
            'verify' => config('services.embed.ssl_verify'),
            'headers' => [
                'Authorization' => $authorization,
                'API-Application-Thumbprint' => $thumbprint,
            ],
            // 'debug' => config('app.debug'),
        ]);
    }

    public static function releaseAllClients()
    {
        foreach (self::$issuedClients as $connectionKey => $issuedClient) {
            Thumbprint::whereId($issuedClient['thumbprint_id'])
                ->update(['reserved_at' => null]);
            unset(self::$issuedClients[$connectionKey]);
        }
    }

    private static function getGuzzleDescription($config)
    {
        return new Description([
            'baseUri' => $config['uri'],
            'operations' => [
                'cardDetails' => [
                    'httpMethod' => 'GET',
                    'uri' => '1.0/card/{id}',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'id' => ['location' => 'uri', 'type' => 'string', 'required' => true],
                        'skip_global_card_check' => ['location' => 'query', 'type' => 'string'],
                    ]
                ],
                'availableProducts' => [
                    'httpMethod' => 'GET',
                    'uri' => '1.0/mobile_templates',
                    'responseModel' => 'getResponse',
                ],
                'saleAddItem' => [
                    'httpMethod' => 'POST',
                    'uri' => '1.0/sale/add_item',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'product_id' => ['location' => 'json', 'type' => 'string', 'required' => true],
                        'product_type' => ['location' => 'json', 'type' => 'string', 'required' => true],
                        'card_id' => ['location' => 'json', 'type' => 'string', 'required' => true],
                        'extra_cash' => ['location' => 'json', 'type' => 'string', 'required' => true, 'default' => 0],
                    ]
                ],
                'saleTender' => [
                    'httpMethod' => 'POST',
                    'uri' => '1.0/sale/tender',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'cashier_id' => ['location' => 'json', 'type' => 'string', 'required' => true, 'default' => self::AnonymousCashierId],
                        'tenders' => ['location' => 'json', 'type' => 'array', 'required' => true],
                    ]
                ],
                'saleCancel' => [
                    'httpMethod' => 'POST',
                    'uri' => '1.0/sale/cancel',
                    'responseModel' => 'getResponse',
                ],
                'verifyCvc' => [
                    'httpMethod' => 'POST',
                    'uri' => 'interface',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'verify_cvc' => ['location' => 'json', 'type' => 'array', 'required' => true],
                    ]
                ],
                'addGuestDetails' => [
                    'httpMethod' => 'POST',
                    'uri' => '1.0/guest',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'id' => ['location' => 'json', 'type' => 'string', 'required' => true],
                        'tenders' => ['location' => 'json', 'type' => 'string'],
                        'holder_title' => ['location' => 'json', 'type' => 'string'],
                        'holder_firstname' => ['location' => 'json', 'type' => 'string'],
                        'holder_surname' => ['location' => 'json', 'type' => 'string'],
                        'address1' => ['location' => 'json', 'type' => 'string'],
                        'address2' => ['location' => 'json', 'type' => 'string'],
                        'address3' => ['location' => 'json', 'type' => 'string'],
                        'postal_code' => ['location' => 'json', 'type' => 'string'],
                        'email' => ['location' => 'json', 'type' => 'string'],
                        'phone' => ['location' => 'json', 'type' => 'string'],
                        'mobile_phone' => ['location' => 'json', 'type' => 'string'],
                        'dob' => ['location' => 'json', 'type' => 'string'],
                        'registered_date' => ['location' => 'json', 'type' => 'string'],
                    ]
                ],
                'listGuests' => [
                    'httpMethod' => 'GET',
                    'uri' => '1.0/lists/guests',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'from_datetime' => ['location' => 'query', 'type' => 'string', 'required' => true],
                    ]
                ],
                'modifiedCards' => [
                    'httpMethod' => 'GET',
                    'uri' => 'modified_cards',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'from_datetime' => ['location' => 'query', 'type' => 'string', 'required' => true],
                        'to_datetime' => ['location' => 'query', 'type' => 'string'],
                        'include_cards_in_group' => ['location' => 'query', 'type' => 'string'],
                        'page' => ['location' => 'query', 'type' => 'string'],
                        'page_size' => ['location' => 'query', 'type' => 'string'],
                    ]
                ],
                'listCards' => [
                    'httpMethod' => 'GET',
                    'uri' => '1.0/lists/cards',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'from_datetime' => ['location' => 'query', 'type' => 'string', 'required' => true],
                        'include_cards_in_group' => ['location' => 'query', 'type' => 'string'],
                    ]
                ],
                'cardHistory' => [
                    'httpMethod' => 'GET',
                    'uri' => '1.0/lists/card_history/{id}',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'id' => ['location' => 'uri', 'type' => 'string', 'required' => true],
                        'from_date' => ['location' => 'query', 'type' => 'string'],
                        'type' => ['location' => 'query', 'type' => 'string', 'default' => 'all'],
                    ]
                ],
                'adjustBalance' => [
                    'httpMethod' => 'POST',
                    'uri' => '1.0/card/adjust_balance',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'id' => ['location' => 'json', 'type' => 'string', 'required' => true],
                        'bonus_balance' => ['location' => 'json', 'type' => 'number', 'required' => true],
                    ]
                ],
                'cardBalanceReversal' => [
                    'httpMethod' => 'POST',
                    'uri' => 'interface',
                    'responseModel' => 'getResponse',
                    'parameters' => [
                        'reverse' => ['location' => 'json', 'type' => 'array', 'required' => true],
                        'content_type' => ['location' => 'header', 'sentAs' => 'Content-Type', 'default' => 'application/json'],
                    ],
                ],
            ],
            'models' => [
                'getResponse' => [
                    'type' => 'object',
                    'additionalProperties' => [
                        'location' => 'json'
                    ]
                ]
            ],
        ]);
    }
}
