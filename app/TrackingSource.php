<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class TrackingSource extends Model
{
    use CrudTrait;
    
    protected $fillable = ['source'];
    
    public $timestamps = false;
}
