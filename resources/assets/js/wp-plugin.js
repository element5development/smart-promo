
import { Vue, store, i18n } from '~/core'

import router from '~/router/wp-router'

import axiosInit from '~/plugins/axios'
axiosInit(router)

import ReloadRouterLink from '~/components/ReloadRouterLink'
Vue.component(ReloadRouterLink.name, ReloadRouterLink)

window.gamePoint = {
  async init(el) {
    if (!store.getters.authCheck && store.getters.authToken) {
      try {
        await store.dispatch('fetchUser')
      } catch (e) { }
    }

    new Vue({
      el,
      store,
      router,
      i18n
    })
  }
}
