import Vue from 'vue'
import vSelect from 'vue-select'
import Icon from './Icon'
import Card from './Card'
import Child from './Child'
import Button from './Button'
import Checkbox from './Checkbox'
import FormGroupRow from './FormGroupRow'
import FormInput from './FormInput'
import PaymentForm from './PaymentForm'
import BuyCards from './BuyCards'
import { HasError, AlertError, AlertSuccess } from 'vform'
import Login from './Login'
import CheckBalance from './CheckBalance'
import Modal from './Modal'
import RegistrationForm from './RegistrationForm'
import Register from './Register'
import PackagePaymentForm from './PackagePaymentForm'
import FormSuccessModal from './FormSuccessModal'
import Profile from './Profile'
import Playcards from './Playcards'
import AddPlaycardButton from './AddPlaycardButton'
import AddPackageButton from './AddPackageButton'
import ReloadCard from './ReloadCard'
import ReloadCardInitForm from './ReloadCardInitForm'
import RegisterShadow from './RegisterShadow'
import LogoutButton from './LogoutButton'
import EmailVerification from './EmailVerification'
import PasswordReset from './PasswordReset'
import UnsubscribeButton from './UnsubscribeButton'
import RegisterCard from './RegisterCard'

Vue.component('v-select', vSelect)

Vue.component(Icon.name, Icon)
Vue.component(Card.name, Card)
Vue.component(Child.name, Child)
Vue.component(Button.name, Button)
Vue.component(Checkbox.name, Checkbox)
Vue.component(FormGroupRow.name, FormGroupRow)
Vue.component(FormInput.name, FormInput)
Vue.component(PaymentForm.name, PaymentForm)
Vue.component(BuyCards.name, BuyCards)
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.component(AlertSuccess.name, AlertSuccess)
Vue.component(Login.name, Login)
Vue.component(CheckBalance.name, CheckBalance)
Vue.component(Modal.name, Modal)
Vue.component(Register.name, Register)
Vue.component(RegistrationForm.name, RegistrationForm)
Vue.component(PackagePaymentForm.name, PackagePaymentForm)
Vue.component(FormSuccessModal.name, FormSuccessModal)
Vue.component(Profile.name, Profile)
Vue.component(Playcards.name, Playcards)
Vue.component(AddPlaycardButton.name, AddPlaycardButton)
Vue.component(AddPackageButton.name, AddPackageButton)
Vue.component(ReloadCard.name, ReloadCard)
Vue.component(ReloadCardInitForm.name, ReloadCardInitForm)
Vue.component(RegisterShadow.name, RegisterShadow)
Vue.component(LogoutButton.name, LogoutButton)
Vue.component(EmailVerification.name, EmailVerification)
Vue.component(PasswordReset.name, PasswordReset)
Vue.component(UnsubscribeButton.name, UnsubscribeButton)
Vue.component(RegisterCard.name, RegisterCard)

import './new-user-promotion'
import './what-is-this'
