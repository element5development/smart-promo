import Vue from 'vue'

import Link from './Link'
import NumberComponent from './Number'
import CVV from './CVV'

Vue.component(Link.name, Link)
Vue.component(NumberComponent.name, NumberComponent)
Vue.component(CVV.name, CVV)
