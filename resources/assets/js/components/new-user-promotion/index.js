import Vue from 'vue'

import Main from './Main'
import Form from './Form'
import Email from './Email'
import ResendButton from './ResendButton'

Vue.component(Main.name, Main)
Vue.component(Form.name, Form)
Vue.component(Email.name, Email)
Vue.component(ResendButton.name, ResendButton)
