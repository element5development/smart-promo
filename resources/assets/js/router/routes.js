export default ({ authGuard, guestGuard }) => {
  const pathMap = window.config.pathMap
  return [
    // Authenticated routes.
    ...authGuard([
      { path: '/my-check-balance', name: 'my-check-balance', component: require('~/pages/check-balance.vue'), meta: { heading: 'check_balance' } },
      { path: pathMap.my_cards, name: 'cards', component: require('~/pages/cards/index.vue'), meta: { heading: 'cards' } },
      { path: '/my-profile', name: 'profile', component: require('~/pages/profile.vue'), meta: { heading: 'profile' } }
    ]),

    // Guest routes.
    ...guestGuard([
      { path: '/check-balance', name: 'check-balance', component: require('~/pages/welcome.vue'), meta: { heading: 'welcome' } },
      { path: pathMap.sign_in, name: 'sign-in', component: require('~/pages/welcome.vue'), meta: { heading: 'welcome', scrollOffsetY: 757 } },
      { path: pathMap.register, name: 'register', component: require('~/pages/auth/register.vue'), meta: { heading: 'register', plainLayout: true } },
      { path: pathMap.register_shadow, name: 'register-shadow', component: require('~/pages/auth/register-shadow.vue'), meta: { heading: 'register' } },
      { path: '/account-verification/verified', name: 'email_verification.verified', component: require('~/pages/auth/email-verification/message.vue'), meta: { message: 'email_verification_verified' } },
      { path: '/account-verification/success', name: 'email_verification.success', component: require('~/pages/auth/email-verification/message.vue'), meta: { message: 'email_verification_succeeded' } },
      { path: '/account-verification/failure', name: 'email_verification.failure', component: require('~/pages/auth/email-verification/message.vue'), meta: { message: 'email_verification_failed' } },
      { path: '/password/reset/:token', name: 'password.reset', component: require('~/pages/auth/password/reset.vue'), meta: { heading: 'password_reset' } },
      { path: '/guest-registration/:token', name: 'optin', component: require('~/pages/auth/optin.vue'), meta: { heading: 'optin' } }
    ]),

    { path: '/', redirect: { name: 'check-balance' } },
    { path: pathMap.buy_cards, name: 'cards.buy', component: require('~/pages/cards/buy.vue'), meta: { heading: 'buy_cards' } },
    { path: '/playcard-reload', name: 'reload-card', component: require('~/pages/reload-card.vue'), meta: { heading: 'reload' } },
    { path: pathMap.reload_card, name: 'cards.reload', component: require('~/pages/cards/reload.vue'), props: true, meta: { heading: 'reload' } },

    { path: '*', component: require('~/pages/errors/404.vue') }
  ]
}
