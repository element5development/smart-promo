import Vue from 'vue'
import Rollbar from 'vue-rollbar'

if (process.env.MIX_APP_ENV != 'local') {
  Vue.use(Rollbar, {
    accessToken: 'f525844a306d4e3d8a0b0ee420dd2c27',
    captureUncaught: true,
    captureUnhandledRejections: true,
    payload: {
      environment: process.env.MIX_APP_ENV
    }
  });

  Vue.config.errorHandler = (err, vm, info) => {
    Vue.rollbar.error(err);
    console.error(err);
  }
}
