import axios from 'axios'
import store from '~/store'
import { loadProgressBar } from 'axios-progress-bar'

export default ({ router }) => {
  loadProgressBar({ showSpinner: false });

  axios.interceptors.request.use(request => {
    if (store.getters.authToken) {
      request.headers.common['X-Authorization'] = `Bearer ${store.getters.authToken}`
    }

    // request.headers['X-Socket-Id'] = Echo.socketId()

    return request
  })

  axios.interceptors.response.use(response => response, error => {
    const { status } = error.response

    if (status === 401 && store.getters.authCheck) {
      store.dispatch('logout').then(() => router.push({ name: 'sign-in' }));
    }

    return Promise.reject(error)
  })
}
