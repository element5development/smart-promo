import axios from 'axios';

export default {
  async index() {
    const { data } = await axios.get(`${window.config.baseUri}/api/package`);
    return data;
  },

  async get(id) {
    const { data } = await axios.get(`${window.config.baseUri}/api/package/${id}`);
    return data;
  }
}
