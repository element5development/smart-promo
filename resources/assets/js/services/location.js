import axios from 'axios';

export default {
  async index() {
    const { data } = await axios.get(`${window.config.baseUri}/api/location`);
    return data;
  }
}
