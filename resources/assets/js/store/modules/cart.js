import Vue from 'vue'
import Cookies from 'js-cookie'
import * as types from '../mutation-types'

// state
export const state = {
  packages: Cookies.get('packages') ? JSON.parse(Cookies.get('packages')) : {}
}

function sanitizeAndStore() {
  Cookies.set('packages', JSON.stringify(state.packages))
}

// mutations
export const mutations = {
  [types.ADD_PACKAGE] (state, { packageId, quantity }) {
    if (packageId in state.packages) {
      Vue.set(state.packages, packageId, parseInt(state.packages[packageId]) + parseInt(quantity));
    } else {
      Vue.set(state.packages, packageId, parseInt(quantity));
    }

    sanitizeAndStore();
  },

  [types.REMOVE_PACKAGE] (state, { packageId }) {
    Vue.delete(state.packages, packageId);
    sanitizeAndStore();
  },

  [types.CLEAR_PACKAGES] (state) {
    state.packages = {};
    sanitizeAndStore();
  },

  [types.DECREASE_PACKAGE] (state, { packageId }) {
    state.packages[packageId]--;
    if (state.packages[packageId] <= 0) {
      Vue.delete(state.packages, packageId);
    }
    sanitizeAndStore();
  }
}

// actions
export const actions = {
  addPackage ({ commit }, payload) {
    commit(types.ADD_PACKAGE, payload)
  },

  removePackage ({ commit }, payload) {
    commit(types.REMOVE_PACKAGE, payload)
  },

  clearPackages ({ commit }) {
    commit(types.CLEAR_PACKAGES)
  },

  decreasePackage ({ commit }, payload) {
    commit(types.DECREASE_PACKAGE, payload)
  }
}

// getters
export const getters = {
  packages: state => state.packages,
}
