import axios from 'axios'
import Cookies from 'js-cookie'
import * as types from '../mutation-types'

const VISIT_COOKIE_NAME = 'new-user-promotion-visit';

export const state = {
  trackingStarted: false,
  visit: Cookies.get(VISIT_COOKIE_NAME)
}

export const mutations = {
  [types.TRACK_START] (state) {
    state.trackingStarted = true;
  },

  [types.TRACK_VISIT] (state, { visit }) {
    state.visit = visit;
  }
}

export const actions = {
  async trackVisit ({ commit }, { source }) {
    if (!state.trackingStarted) {
      commit(types.TRACK_START);
  
      const { data } = await axios.post(`${window.config.baseUri}/api/tracking/visit`, { source, visit: state.visit });
      const visit = data.visit;
  
      Cookies.set(VISIT_COOKIE_NAME, visit, { expires: 365 });
  
      commit(types.TRACK_VISIT, { visit })
    }
  }
}

export const getters = {
  visit: state => state.visit
}
