import axios from 'axios'
import Cookies from 'js-cookie'
import * as types from '../mutation-types'

// state
export const state = {
  user: null,
  token: Cookies.get('token'),
  shadow_token: Cookies.get('shadow_token')
}

// mutations
export const mutations = {
  [types.SAVE_TOKEN] (state, { token, remember }) {
    state.token = token
    Cookies.set('token', token, { expires: remember ? 365 : null })
  },

  [types.FETCH_USER_SUCCESS] (state, { user }) {
    state.user = user
  },

  [types.FETCH_USER_FAILURE] (state) {
    state.token = null
    Cookies.remove('token')
  },

  [types.LOGOUT] (state) {
    state.user = null
    state.token = null

    Cookies.remove('token')
  },

  [types.UPDATE_USER] (state, { user }) {
    state.user = user
  },

  [types.SAVE_SHADOW_TOKEN] (state, { shadow_token }) {
    state.shadow_token = shadow_token
    Cookies.set('shadow_token', shadow_token)
  }
}

// actions
export const actions = {
  saveToken ({ commit }, payload) {
    commit(types.SAVE_TOKEN, payload)
  },

  async fetchUser ({ commit }) {
    try {
      const { data } = await axios.get(`${window.config.baseUri}/api/user`)

      commit(types.FETCH_USER_SUCCESS, { user: data })
    } catch (e) {
      commit(types.FETCH_USER_FAILURE)
    }
  },

  updateUser ({ commit }, payload) {
    commit(types.UPDATE_USER, payload)
  },

  async logout ({ commit }) {
    try {
      await axios.post(`${window.config.baseUri}/api/logout`)
    } catch (e) { }

    commit(types.LOGOUT)
  },

  saveShadowToken ({ commit }, payload) {
    commit(types.SAVE_SHADOW_TOKEN, payload)
  },

  authGuard({ commit }, payload) {
    if (!state.user) {
      payload.$router.push({name: 'sign-in'});
    }
  }
}

// getters
export const getters = {
  authUser: state => state.user,
  authToken: state => state.token,
  authCheck: state => state.user !== null,
  shadowToken: state => state.shadow_token
}
