
import { Vue, store, i18n } from '~/core'

import router from '~/router'

import axiosInit from '~/plugins/axios'
axiosInit(router)

import App from '~/components/App'

new Vue({
  i18n,
  store,
  router,
  ...App
})
