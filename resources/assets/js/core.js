
import Vue from 'vue'
import store from '~/store'
import 'bootstrap'
import i18n from '~/plugins/vue-i18n'
import '~/plugins/rollbar'
import '~/plugins/disable-autocomplete'
import Vue2Filters from 'vue2-filters'

import '~/components'

Vue.config.productionTip = false

Vue.use(Vue2Filters)

export { Vue, store, i18n }
