<?php

return [
    'frequency' => 'Frequency',
    'time_since_last_visit' => 'Time since last visit',
    'specific_date' => 'Specific date',
    'birthday' => 'Birthday',
    'day' => 'Day',
    'week' => 'Week',
    'month' => 'Month',
];
