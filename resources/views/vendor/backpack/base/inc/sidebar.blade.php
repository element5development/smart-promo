@if (Auth::guard('admin')->check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="{{ backpack_avatar_url(Auth::guard('admin')->user()) }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::guard('admin')->user()->name }}</p>
            <small><small><a href="{{ route('backpack.account.info') }}"><span><i class="fa fa-user-circle-o"></i> {{ trans('backpack::base.my_account') }}</span></a> &nbsp;  &nbsp; <a href="{{ backpack_url('logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></small></small>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          {{-- <li class="header">{{ trans('backpack::base.administration') }}</li> --}}
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <!-- <li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li> -->

          @hasanyrole(['operator', 'superadmin'])
          <li><a href="{{ url('admin/voucher') }}"><i class="fa fa-ticket"></i> <span>Vouchers</span></a></li>
          @endhasanyrole

          @hasanyrole(['superadmin'])
          <li><a href="{{ url('admin/member') }}"><i class="fa fa-user"></i> <span>Customers</span></a></li>
          <li><a href="{{ url('admin/card') }}"><i class="fa fa-credit-card"></i> <span>Cards</span></a></li>
          @if (config('capabilities.payments'))
          <li><a href="{{ url('admin/transaction') }}"><i class="fa fa-usd"></i> <span>Payments</span></a></li>
          @endif
          @if (config('capabilities.promotions'))
          <li><a href="{{ url('admin/promotion') }}"><i class="fa fa-gift"></i> <span>Promotions</span></a></li>
          @endif
          <li><a href="{{ url('admin/package') }}"><i class="fa fa-archive"></i> <span>Packages</span></a></li>
          @if (config('capabilities.locations'))
          <li><a href="{{ url('admin/location') }}"><i class="fa fa-map-marker"></i> <span>Locations</span></a></li>
          @endif
          @endhasanyrole

          @hasanyrole(['superadmin', 'maintainer'])
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
          @if (config('capabilities.file_manager'))
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/elfinder') }}"><i class="fa fa-files-o"></i> <span>File manager</span></a></li>
          @endif
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/setting') }}"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
          @endhasanyrole

          @hasanyrole(['maintainer'])
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/capability') }}"><i class="fa fa-unlock-alt"></i> <span>Capabilities</span></a></li>
          @if (config('capabilities.embed'))
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/thumbprint') }}"><i class="fa fa-key"></i> <span>Thumbprints</span></a></li>
          @endif
          <li class="treeview">
            <a href="#"><i class="fa fa-eye"></i> <span>Tracking</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/tracking-summary') }}"><i class="fa fa-table"></i> <span>Summary</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/tracking-visit') }}"><i class="fa fa-user-secret"></i> <span>Visits</span></a></li>
            </ul>
          </li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/log') }}"><i class="fa fa-terminal"></i> <span>Logs</span></a></li>
          @endhasanyrole

          <!-- ======================================= -->
          {{-- <li class="header">Other menus</li> --}}
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
