@include('crud::fields.select_from_array')

@push('crud_fields_scripts')
<script>
  $(function() {
    var showHide = function(selected) {
      var customFields = $('[class^="{{ $field['class_prefix'] }}"],[class*=" {{ $field['class_prefix'] }}"]');

      customFields.hide();

      $('.{{ $field['class_prefix'] }}' + selected).show();
      
      var clearExcludeClasses = ['datepicker-range-start', 'datepicker-range-end'];

      customFields.each(function (i, field) {
        field = $(field);
        if (field.css('display') == 'none') {
          field.find('input, select').each(function (i, input) {
            input = $(input);
            var excludeClear = false;
            for (var i in clearExcludeClasses) {
              if (input.hasClass(clearExcludeClasses[i])) {
                excludeClear = true;
              }
            }
            if (!excludeClear) {
              input.val('');
            }
          });
        }
      });
    };

    var selectField = $('#{{ $field['attributes']['id'] }}');
    selectField.change(function (e) {
      showHide(e.target.value);
    });

    showHide(selectField.val());
  });
</script>
@endpush
