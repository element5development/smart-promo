<!-- textarea -->
<div @include('crud::inc.field_wrapper_attributes') >
    <p class="help-block text-right">Remaining characters: <strong><span id="{{ $field['ckeditor'] }}RemainingCharacters"></span></strong></p>
    <p class="help-block text-right">Messages: <strong><span id="{{ $field['ckeditor'] }}Messages"></span></strong></p>
</div>

@push('crud_fields_scripts')
<script>
  $(function() {
    function updateCount(editor) {
      var html = editor.getData();
      var tmp = document.createElement('div');
      tmp.innerHTML = html;
      var text = tmp.textContent || tmp.innerText || '';

      var length = text.length;
      var singleLength = length > 160 ? 153 : 160;
      var messages = Math.ceil(length / singleLength);
      var remainingCharacters = (messages ? messages : 1) * singleLength - length;
      
      $("#{{ $field['ckeditor'] }}RemainingCharacters").text(remainingCharacters);
      $("#{{ $field['ckeditor'] }}Messages").text(messages);
    }

    var editor = CKEDITOR.instances["ckeditor-{{ $field['ckeditor'] }}"];

    updateCount(editor);
    
    editor.on('change', function(e) {
      updateCount(e.editor);
    });
  });
</script>
@endpush
