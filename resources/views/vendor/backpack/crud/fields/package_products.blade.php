<div @include('crud::inc.field_wrapper_attributes') >
    <label class="m-b-0">{!! $field['label'] !!}</label>

    @foreach (App\Location::all() as $location)

    @php
    $productId = old('product_id.' . $location->id) ?? (isset($entry) ? $entry->products()->where('products.location_id', $location->id)->select('products.id')->value('id') : null);
    @endphp

    <div class="m-t-10">
        <label class="text-muted">{{ $location->title }}</label>
        <select name="products[{{ $location->id }}]" @include('crud::inc.field_attributes') >
            <option value="">Select product...</option>
            @foreach ($location->products()->where('cost', '>', 0)->orderBy('product_name')->get() as $product)
            <option value="{{ $product->id }}"{{ $product->id == $productId ? ' selected' : '' }}>{{ $product->product_name }}</option>
            @endforeach
        </select>
    </div>
    @endforeach
</div>
