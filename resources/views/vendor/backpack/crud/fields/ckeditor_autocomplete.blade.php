<div @include('crud::inc.field_wrapper_attributes') >
    "@{{ $property }}" is replaced with dynamic value; supported properties:
    @foreach ($field['options'] as $property => $title)
        <div>
            <a href="#" class="ckeditor_autocomplete-{{ $field['name'] }}" data-property="{{ $property }}">{{ $property }}</a> - {{ $title }}
        </div>
    @endforeach
</div>

@push('crud_fields_scripts')
<script>
    $(function() {
        $('.ckeditor_autocomplete-{{ $field['name'] }}').click(function () {
            var property = $(this).data('property');
            CKEDITOR.instances["ckeditor-{{ $field['ckeditor'] }}"].insertText('@{{ $' + property + ' }}');
            return false;
        });
    });
</script>
@endpush
