<!-- CKeditor -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')
    <textarea
    	id="ckeditor-{{ $field['name'] }}"
        name="{{ $field['name'] }}"
        @include('crud::inc.field_attributes', ['default_class' => 'form-control ckeditor'])
    	>{{ old($field['name']) ? old($field['name']) : (isset($field['value']) ? $field['value'] : (isset($field['default']) ? $field['default'] : '' )) }}</textarea>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>


{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
        <script src="{{ asset('vendor/backpack/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('vendor/backpack/ckeditor/adapters/jquery.js') }}"></script>
    @endpush

@endif

{{-- FIELD JS - will be loaded in the after_scripts section --}}
@push('crud_fields_scripts')

@php
$plainText = isset($field['plain_text']) && $field['plain_text'];
@endphp
<script>
    jQuery(document).ready(function($) {
        $('textarea[name="{{ $field['name'] }}"].ckeditor').ckeditor({
            filebrowserBrowseUrl: "{{ url(config('backpack.base.route_prefix').'/elfinder/ckeditor') }}",
            extraPlugins : 'placeholder{{ isset($field['extra_plugins']) ? ',' . implode(',', $field['extra_plugins']) : '' }}',
            removePlugins: 'image,scayt,wsc,about,table,tabletools,specialchar,horizontalrule,removeformat,blockquote{{ $plainText ? ',sourcearea,elementspath' : '' }}',
            removeButtons: 'Underline,Subscript,Superscript,Anchor',
            toolbarGroups: [
                {name: 'clipboard', groups: ['clipboard', 'undo']},
                @if (!$plainText)
                {name: 'basicstyles', groups: ['basicstyles']},
                {name: 'links'},
                @endif
                {name: 'insert'},
                @if (!$plainText)
                {name: 'paragraph', groups: ['list', 'indent']},
                {name: 'styles'},
                {name: 'document', groups: ['mode']},
                {name: 'tools'}
                @endif
            ],
            @if ($plainText)
            forcePasteAsPlainText: true,
            enterMode: CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_BR,
            contentsCss: [
                '{{ asset('css/ckeditor.plain.css') }}'
            ],
            @else
            allowedContent: true,
            removeFormatAttributes: '',
            contentsCss: [
                '{{ asset('css/email.css') }}',
                'https://fonts.googleapis.com/css?family=Open+Sans:400,700',
                'https://fonts.googleapis.com/css?family=Montserrat:400,600,700'
            ],
            stylesSet: [],
            @endif
            placeholderItems: {!! isset($field['placeholder_items']) ? json_encode($field['placeholder_items']) : '[]' !!}
        });
    });
</script>
@endpush

{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
