<!-- textarea -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')
    <textarea
    	name="{{ $field['name'] }}"
        @include('crud::inc.field_attributes')

    	>{{ old($field['name']) ? old($field['name']) : (isset($field['value']) ? $field['value'] : (isset($field['default']) ? $field['default'] : '' )) }}</textarea>

    <p class="help-block text-right">Remaining characters: <strong><span id="{{ $field['attributes']['id'] }}RemainingCharacters"></span></strong></p>
    <p class="help-block text-right">Messages: <strong><span id="{{ $field['attributes']['id'] }}Messages"></span></strong></p>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>

@push('crud_fields_scripts')
<script>
  $(function() {
    function updateCount(textarea) {
      var length = $(textarea).val().length;
      var singleLength = length > 160 ? 153 : 160;
      var messages = Math.ceil(length / singleLength);
      var remainingCharacters = (messages ? messages : 1) * singleLength - length;
      
      $("#{{ $field['attributes']['id'] }}RemainingCharacters").text(remainingCharacters);
      $("#{{ $field['attributes']['id'] }}Messages").text(messages);
    }

    var textarea = $("#{{ $field['attributes']['id'] }}");

    updateCount(textarea);

    textarea.keyup(function(){
      updateCount(this);
    });
  });
</script>
@endpush
