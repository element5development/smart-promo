<td>
    @php
        if (isset($column['entity'])) {
            $entity = $entry->{$column['entity']};
        } elseif (isset($column['function_name'])) {
            $entity = $entry->{$column['function_name']}();
        }
    @endphp
    @if ($entity)
    <a href="{{ url(config('backpack.base.route_prefix') . '/' . $column['path'] . '/' . $entity->id) }}">
        {{ $entity->{$column['attribute']} }}
    </a>
    @endif
</td>
