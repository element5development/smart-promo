<td>
  <input type="checkbox" data-toggle="toggle" id="{{ $column['name'] }}_{{ $entry->id }}" {{ ($entry->{$column['name']}) ? 'checked' : '' }} />
</td>

<script>
  var toggle = $('#{{ $column["name"] }}_{{ $entry->id }}');
  
  toggle.bootstrapToggle({
    size: 'mini',
    on: 'Yes',
    off: 'No'
  });

  toggle.change(function () {
    var data = { id: {{ $entry->id }} };
    if ($(this).prop('checked')) {
      data.on = true;
    }
    $.post('{{ $column["handler_url"] }}', data, function (data) {
      if (data.error) {
        toggle.bootstrapToggle('off')

        new PNotify({
          text: data.error,
          type: 'error',
          icon: false
        });
      }
    });
  });
</script>
