@if ($entry->claimedPackages()->count())
    <td style="padding: 0;">
        @include('admin.partial.claimed_cards', ['style' => 'margin: 0;'])
    </td>
@else
    <td>No cards processed</td>
@endif
