@if ($crud->hasAccess('claim') && !$entry->is_claimed)
	<a href="{{ url($crud->route.'/'.$entry->getKey().'/claim') }}" class="btn btn-sm btn-default"><i class="fa fa-address-card-o"></i> Claim</a>
@endif
