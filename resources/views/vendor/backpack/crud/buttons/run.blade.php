@if ($crud->hasAccess('run'))
	<a href="{{ url($crud->route.'/'.$entry->getKey().'/run') }}" class="btn btn-sm btn-default"><i class="fa fa-paper-plane"></i> Run</a>
@endif
