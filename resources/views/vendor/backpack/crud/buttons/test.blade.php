@if ($crud->hasAccess('test'))
	<a href="{{ url($crud->route.'/'.$entry->getKey().'/test') }}" class="btn btn-sm btn-default"><i class="fa fa-check-square-o"></i> Test</a>
@endif
