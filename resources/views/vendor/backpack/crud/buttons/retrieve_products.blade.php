@if ($crud->hasAccess('create'))
	<a href="{{ url($crud->route.'/refresh') }}" class="btn btn-primary ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-refresh"></i> Retrieve products</span></a>
@endif