@if ($crud->hasAccess('check_balance'))
	<a href="{{ url($crud->route.'/'.$entry->getKey().'/check-balance') }}" class="btn btn-sm btn-default"><i class="fa fa-refresh"></i> Check balance</a>
@endif
