@if ($crud->buttons->where('stack', $stack)->count())
<div class="button-stack">
	@foreach ($crud->buttons->where('stack', $stack) as $button)
	  @if ($button->type == 'model_function')
		{!! $entry->{$button->content}($crud); !!}
		@else
		@include($button->content)
	  @endif
	@endforeach
</div>
@endif
