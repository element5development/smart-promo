(function () {
  var nodes = document.getElementsByClassName('game-point-switch-{{ $authenticated ? 'authenticated' : 'guest' }}');
  for (var i = 0; i < nodes.length; i++) {
    var node = nodes[i];
    node.style.setProperty('display', 'block', 'important');
  }
})();
