@if ($authenticated)

document.write('<a href="{{ url('widget/logout') }}" class="smallnavlink">Sign-out</a>');

@else

document.write('<a href="{{ url('sign-in') }}" class="smallnavlink">Sign-in</a>');

@endif
