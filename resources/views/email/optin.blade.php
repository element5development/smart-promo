<center>
  <p>
    Dear Client,<br/>
    Welcome to the Big Thrill Factory!<br/>
    <br/>
    In order to gain the full benefits of membership with BTF, please finalize your registration by clicking on the following button:<br/>
    <br/>
    <br/>
  </p>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table border="0" cellspacing="0" cellpadding="0" class="button">
          <tr><td><a href="{{$optin_url}}">verify your account</a></td></tr>
        </table>
      </td>
    </tr>
  </table>

  <p>
    <br/>
    <br/>
    Registration takes 1 minute and you will then be receiving bonuses on your swipe card, special promotions and more that are only available to registered members!
  </p>
</center>
