<center>
  <p>
    Dear Client,<br/>
    Thanks for registering with us! To complete your registration, please click the following button:<br/>
    <br/>
    <br/>
  </p>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table border="0" cellspacing="0" cellpadding="0" class="button">
          <tr><td><a href="{{$verification_url}}">Verify your account</a></td></tr>
        </table>
      </td>
    </tr>
  </table>

  <p>
    <br/>
    <br/>
    See you soon!
  </p>
</center>
