<center><h1>HIGH FIVE!</h1></center>
<h3>Your online order has been received.</h3>
<table cellpadding="0" cellspacing="0">
  <tr><td width="250"><h3>Order Confirmation</h3></td><td><h3>{{$voucher_number}}</h3></td></tr>
  <tr><td><h3>Validation Code</h3></td><td><h3>{{$voucher_code}}</h3></td></tr>
  <tr><td><h3>Order Summary</h3></td><td><h3>$ {{$amount}}</h3></td></tr>
</table>
<p>Great news {{$first_name}}, you can now eat, play, and celebrate at Big Thrill Factory - the premiere destination for go karts, bowling, laser tag, trampolines, arcade games, mini golf and more!</p>
<h4>1. Present order confirmation to claim</h4>
<p>Show order confirmation directly from your mobile device or <a href="{{$voucher_pdf}}" target="_blank">print voucher</a> prior to your visit.</p>
<h4>2. Swipe to play games & attractions</h4>
<p>With Swipe Cards there’s no need to rummage for cash and coins!</p>
