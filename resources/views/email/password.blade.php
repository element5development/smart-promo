<center>
  <p>
    Dear Client,<br/>
    You are receiving this email as we received a password reset request for your account.<br/>
    <br/>
    <br/>
  </p>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table border="0" cellspacing="0" cellpadding="0" class="button">
          <tr><td><a href="{{$reset_url}}">Reset Password</a></td></tr>
        </table>
      </td>
    </tr>
  </table>

  <p>
    <br/>
    <br/>
    If you did not request a password reset then please ignore this message.
  </p>
</center>
