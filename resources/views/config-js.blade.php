@php
$config = (new App\Services\ConfigJsService(request()))->get();
@endphp
window.config = {!! json_encode($config); !!};
