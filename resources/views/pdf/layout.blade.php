<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700" rel="stylesheet">
  <style>
    .wrapper {
      background-color: black;
      height: 100%;
      width: 100%;
      font-family: 'Open Sans', Arial, sans-serif;
    }
    .wrapper a {
      color: #282828;
      text-decoration: none;
      font-family: 'Open Sans', Arial, sans-serif;
    }
    .logo {
      background-color: #f1f2f8;
    }
    .menu {
      color: white;
      font-size: 20px;
      padding-right: 20px;
    }
    .menu span {
      padding-left: 15px;
    }
    .menu a {
      color: white;
      text-decoration: none;
      font-family: 'Open Sans', Arial, sans-serif;
      padding-left: 15px
    }
    .menu a:hover {
      color: #ffe000;
    }
    .footer {
      color: #666666;
    }
    .get-social {
      color: #ffffff;
      font-size: 19px;
      font-weight: bold;
      padding-top: 48px;
    }
    .social-icons {
      padding-top: 26px;
    }
    .copyright {
      font-size: 8px;
      line-height: 15px;
      padding-top: 30px;
    }
    .copyright a {
      color: #666666;
      text-decoration: none;
      font-family: 'Open Sans', Arial, sans-serif;
    }
    .copyright a:hover {
      text-decoration: underline;
    }
    .unsubscribe {
      font-size: 8px;
      line-height: 15px;
      padding-top: 25px;
      padding-bottom: 10px;
    }
    .unsubscribe a {
      color: #0099cc;
      text-decoration: none;
      padding-bottom: 17px;
      font-family: 'Open Sans', Arial, sans-serif;
    }

    .content-wrapper {
      background-color: white;
      padding: 50px 75px;
    }
    .content a {
      color: #282828;
      font-family: 'Open Sans', Arial, sans-serif;
      border-bottom: 1px solid;
      text-decoration: none;
    }
    h1 {
      color: #00ccff;
      font-family: 'Montserrat', Verdana, sans-serif;
      font-size: 50px;
      font-weight: 700;
      margin: 0 0 44px;
      line-height: 37px;
    }
    h3 {
      color: #282828;
      font-family: 'Montserrat', Verdana, sans-serif;
      font-size: 19px;
      font-weight: 600;
      line-height: 28px;
      margin-bottom: 30px;
    }
    p {
      color: #282828;
      font-size: 15px;
      line-height: 25px;
    }
    h4 {
      margin-top: 38px;
      color: #00ccff;
      font-size: 19px;
      font-weight: bold;
    }
    table.button a {
      color: #fffefe;
      font-family: 'Open Sans', Arial, sans-serif;
      font-size: 16px;
      font-weight: 700;
      text-transform: uppercase;
      text-decoration: none;
    }
    table.button td {
      background-color: #fe4743;
      padding: 15px 70px;
    }
  </style>

</head>
<body style="margin: 0; font-family: Arial, sans-serif, 'Open Sans';">
  <table class="wrapper" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center" valign="top">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td class="logo">
              <a href="https://www.bigthrillfactory.com"><img src="images/email-logo.png" width="193" height="90"></a>
            </td>
          </tr>
          <tr>
            <td class="menu" align="right" height="64">
              <span><a href="https://www.bigthrillfactory.com/eat">MENU</a></span>
              <span><a href="https://www.bigthrillfactory.com/play">THINGS TO DO</a></span>
              <span><a href="https://www.bigthrillfactory.com/play">PARTIES & EVENTS</a></span>
              <span><a href="https://www.bigthrillfactory.com/contact">LOCATIONS</a></span>
              <span><a href="https://www.bigthrillfactory.com/specials">SPECIALS</a></span>
              <span><a href="http://smartpromo.archahosting.com/sign-in">SIGN IN</a></span>
            </td>
          </tr>
          <tr>
            <td class="content-wrapper" width="100%" cellpadding="0" cellspacing="0" align="left">
              <table class="content" cellpadding="0" cellspacing="0">
                <tr><td>{!! $content !!}</td></tr>
              </table>
            </td>
          </tr>
          <tr>
            <td class="footer" align="center">
              <table cellpadding="0" cellspacing="0" width="100%">
                <tr><td class="get-social" align="center">Get social with us.</td></tr>
                <tr><td class="social-icons" align="center">
                  <a href="https://www.facebook.com/BigThrillFactory"><img src="images/email-fb.png" width="9" height="20"></a>
                  <a href="https://twitter.com/BTFactory"><img src="images/email-twiter.png" width="23" height="19"></a>
                  <a href="https://www.instagram.com/bigthrillfactory/"><img src="images/email-instagram.png" width="19" height="19"></a>
                  <a href="https://www.youtube.com/user/BigThrillFactory"><img src="images/email-youtube.png" width="26" height="19"></a>
                </td></tr>
                <tr><td class="copyright" align="center">
                  Copyright © 2018 Big Thrill Factory, LLC. All rights reserved.<br/>
                  <a href="https://www.google.com/maps/dir/''/Big+Thrill+Factory+17585+Highway+7+Minnetonka,+MN+55345/@44.9117805,-93.536965,13z/data=!3m1!4b1!4m9!4m8!1m0!1m5!1m1!1s0x87f61dd67f68388d:0xb6ed3a9b06e7190d!2m2!1d-93.5019457!2d44.9117859!3e0">17585 Hwy 7, Minnetonka, MN<br/></a>
                  <a href="https://www.google.com/maps/dir//7053+10th+Street+North,+Oakdale,+MN+55128/@44.9601832,-92.9984023,13z/data=!3m1!4b1!4m9!4m8!1m0!1m5!1m1!1s0x87f7d7d8c42fb603:0x3e9bd5614cd65d15!2m2!1d-92.963383!2d44.9601886!3e0">7053 10th St. N., Oakdale MN</a><br/>
                  <a href="https://www.bigthrillfactory.com">BigThriIIFactory.com</a>
                </td></tr>
                <tr><td class="unsubscribe" align="center">
                  Want to change how you receive these emails?<br/>
                  You can update your preferences or <a href="https://shop.bigtf.com">unsubscribe</a> from this list
                </td></tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table></body></html>
  