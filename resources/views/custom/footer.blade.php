<footer class="footer">
  <div class="red-section">
    <div class="w-container">
      <h3 class="h3">The best deals start here</h3>
      <a class="link whitelink" href="/playcard-registration">Register for instant savings and awesome extras! »</a>
    </div>
  </div>
  <div class="footercontent w-container">
    <div class="footerdiv">
      <div class="footerlinkwrap nomargin">
        <div class="footerlink notalink">Big Thrill Factory Minnetonka</div>
      </div>
      <div class="footerlinkwrap"><a href="https://www.google.com/maps/dir/&#x27;&#x27;/Big+Thrill+Factory+17585+Highway+7+Minnetonka,+MN+55345/@44.9117805,-93.536965,13z/data=!3m1!4b1!4m9!4m8!1m0!1m5!1m1!1s0x87f61dd67f68388d:0xb6ed3a9b06e7190d!2m2!1d-93.5019457!2d44.9117859!3e0" class="footerlink">17585 Highway 7<br> Minnetonka, MN 55345</a></div>
      <div class="footerlinkwrap nomargin">
        <div class="footerlink notalink">Big Thrill Factory Oakdale</div>
      </div>
      <div class="footerlinkwrap"><a href="https://www.google.com/maps/dir//7053+10th+Street+North,+Oakdale,+MN+55128/@44.9601832,-92.9984023,13z/data=!3m1!4b1!4m9!4m8!1m0!1m5!1m1!1s0x87f7d7d8c42fb603:0x3e9bd5614cd65d15!2m2!1d-92.963383!2d44.9601886!3e0" class="footerlink">7053 10th Street North<br>Oakdale, MN 55128</a></div>
      <div class="footerlinkwrap">
        <div class="footerlink notalink">Hours: Sun-Thu 10am to 9pm | Fri 10am to 11pm | Sat 9am to 11pm<br>Holiday Hours: <br>Christmas Eve (Dec 24): 10am to 4pm<br>Christmas (Dec 25): Closed</div>
      </div>
    </div>
    <div class="footerdiv center w-clearfix">
      <div class="footerlinkwrap nomargin right"><a href="https://www.bigthrillfactory.com/contact" class="footerlink">Contact</a></div>
      <div class="footerlinkwrap nomargin right"><a href="https://www.bigthrillfactory.com/blog" class="footerlink">News</a></div>
      <div class="footerlinkwrap nomargin right"><a href="https://www.bigthrillfactory.com/careers" class="footerlink">Careers</a></div>
      <div class="footerlinkwrap nomargin right"><a href="https://www.bigthrillfactory.com/privacy-policy" class="footerlink">Privacy Policy</a></div>
      <div class="footerlinkwrap nomargin newsletterdiv right">
        <div class="newsletterfooter">Join Our Newsletter</div>
      </div>
      <div class="footerlinkwrap signup">
        <div class="w-embed w-script">
          <script type="text/javascript" src="https://form.jotform.com/jsform/62716598580165"></script>
        </div>
      </div>
    </div>
  </div>
  <div class="footeroffooter">
    <div class="w-container">
      <div class="footerdiv">
        <div class="footerlinkwrap nomargin">
          <div class="footerlink notalink">© Big Thrill Factory, LLC. All rights reserved.</div>
        </div>
      </div>
      <div class="footerdiv socialfooter"><a href="https://www.facebook.com/BigThrillFactory" target="_blank" class="footersociallinkblocks w-inline-block"><img src="https://uploads-ssl.webflow.com/57741c0fc8d072ad1d615206/5775480da8c056f420dbcab0_fb.png" width="64"></a><a href="https://twitter.com/BTFactory" target="_blank" class="footersociallinkblocks w-inline-block"><img src="https://uploads-ssl.webflow.com/57741c0fc8d072ad1d615206/57754821db2e3be04741e7e3_twitter.png" width="64"></a><a href="https://www.instagram.com/bigthrillfactory/" target="_blank" class="footersociallinkblocks w-inline-block"><img src="https://uploads-ssl.webflow.com/57741c0fc8d072ad1d615206/577548a3db2e3be04741eb5e_new-insta-white.png" width="64"></a><a href="https://www.youtube.com/user/BigThrillFactory" target="_blank" class="footersociallinkblocks w-inline-block"><img src="https://uploads-ssl.webflow.com/57741c0fc8d072ad1d615206/577548ab3654d2b7708acc4c_yt.png" width="64"></a></div>
    </div>
  </div>
</footer>