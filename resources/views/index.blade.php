<!DOCTYPE html>
<html data-wf-page="5a32e2c887a8f50001225b91" data-wf-site="5a31a17c0672a700015af037" lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta content="{{ config('app.name') }}" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">

  <title>{{ config('app.name') }}</title>

  {!! config('settings.head_template') !!}

  <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
  {!! config('settings.menu_template') !!}

  <div id="app"></div>

  {!! config('settings.footer_template') !!}

  @include('scripts')
</body>
</html>
