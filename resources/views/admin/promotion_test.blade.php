@extends('backpack::layout')

@section('content')
<a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>

@include('crud::inc.grouped_errors')

{!! Form::open(array('url' => $crud->route.'/'.$entry->getKey().'/test', 'method' => 'post')) !!}
    <div class="box">
        <div class="box-header with-border">
        <h3 class="box-title">
            Test
            <span class="text-lowercase">{{ $crud->entity_name }}</span>
        </h3>
    </div>
    <div class="box-body row">
        <div @include('crud::inc.field_wrapper_attributes') >
            <label>Type</label>
            <select name="type" id="typeSelect" @include('crud::inc.field_attributes')>
                <option value="">Select notification type...</option>
                @foreach ($types as $type => $label)
                @if (!empty($entry->{$type . '_template'}))
                <option value="{{ $type }}"{{ old('type') == $type ? ' selected' : '' }}>{{ $label }}</option>
                @endif
                @endforeach
            </select>
        </div>
        <div @include('crud::inc.field_wrapper_attributes') id="emailBlock">
            <label>Email</label>
            <input type="email" name="email" value="{{ old('email') ? old('email') : '' }}" @include('crud::inc.field_attributes')>
        </div>
        <div @include('crud::inc.field_wrapper_attributes') id="phoneBlock">
            <label>Phone number</label>
            <input type="textr" name="phone" value="{{ old('phone') ? old('phone') : '' }}" @include('crud::inc.field_attributes')>
        </div>
        <div @include('crud::inc.field_wrapper_attributes') >
            <label>First name</label>
            <input type="text" name="first_name" value="{{ old('first_name') ? old('first_name') : $faker->firstName }}" @include('crud::inc.field_attributes')>
        </div>
        <div @include('crud::inc.field_wrapper_attributes') >
            <label>Last name</label>
            <input type="text" name="last_name" value="{{ old('last_name') ? old('last_name') : $faker->lastName }}" @include('crud::inc.field_attributes')>
        </div>
        <div @include('crud::inc.field_wrapper_attributes') >
            <label>Card number</label>
            <input type="text" name="card_number" value="{{ old('card_number') ? old('card_number') : '000011' . $faker->isbn13 }}" @include('crud::inc.field_attributes')>
        </div>
    </div>
    <div class="box-footer">
        <div id="saveActions" class="form-group">
        <button type="submit" class="btn btn-success">
            <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
            Send
        </button>
        <a href="{{ url($crud->route) }}" class="btn btn-default"><span class="fa fa-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a>
    </div>
    </div>
</div>
{!! Form::close() !!}
@endsection

@section('after_scripts')
<script>
$(function() {
  var showHide = function(selected) {
    $('#emailBlock, #phoneBlock').hide();;
    
    if (selected.length) {
      var type = selected.split('_')[1];
      
    console.log(type);
      if (type == 'email') {
        $('#emailBlock').show();
      }
      
      if (type == 'sms') {
        $('#phoneBlock').show();
      }
    }
  };

  var selectField = $('#typeSelect');
  
  selectField.change(function (e) {
    showHide(e.target.value);
  });

  showHide(selectField.val());
});
</script>
@endsection
