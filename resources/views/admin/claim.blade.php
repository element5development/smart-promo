@extends('backpack::layout')

@section('content')
    <a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>

    @include('crud::inc.grouped_errors')

    {!! Form::open(array('url' => $crud->route.'/'.$entry->getKey().'/claim', 'method' => 'post')) !!}
        <div class="box">
            <div class="box-header with-border">
            <h3 class="box-title">
                Claim
                <span class="text-lowercase">{{ $crud->entity_name }}</span>
            </h3>
        </div>
        <div class="box-body row">
            @if ($entry->distinctCards()->count())
                <div class="col-md-12">
                    <label>Processed cards</label>
                    @include('admin.partial.claimed_cards')
                </div>
            @endif

            <div @include('crud::inc.field_wrapper_attributes') >
            @php
                $unclaimedPackages = $entry->unclaimedPackages()->get();
            @endphp
            @if (config('smartpromo.claim_multiple_packages'))
                <label>Packages</label>
                <p class="help-block">Packages to apply on new card</p>
                @if ($unclaimedPackages->count() == 1)
                    <div class="checkbox" @include('crud::inc.field_wrapper_attributes')>
                        <label>
                            <input type="checkbox" name="package_id[{{ $unclaimedPackages[0]->pivot->id }}]"
                                value="{{ $unclaimedPackages[0]->pivot->id }}" checked readonly> {{ $unclaimedPackages[0]->title }}
                            @if ($unclaimedPackages[0]->pivot->qty > 1)
                                <span class="text-muted">({{ $unclaimedPackages[0]->pivot->qty }})</span>
                            @endif
                        </label>
                    </div>
                @else
                    @foreach ($unclaimedPackages as $package)
                    <div class="checkbox" @include('crud::inc.field_wrapper_attributes')>
                        <label>
                            <input type="checkbox" name="package_id[{{ $package->pivot->id }}]"
                                value="{{ $package->pivot->id }}" {{ old('package_id.' . $package->pivot->id) ? 'checked' : null }}> {{ $package->title }}
                            @if ($package->pivot->qty > 1)
                                <span class="text-muted">({{ $package->pivot->qty }})</span>
                            @endif
                        </label>
                    </div>
                    @endforeach
                @endif
            @else
                <label>Package</label>
                <p class="help-block">Package to apply on new card</p>
                @if ($unclaimedPackages->count() == 1)
                    <select name="package_id[]" @include('crud::inc.field_attributes') readonly>
                        <option value="{{ $unclaimedPackages[0]->pivot->id }}" selected>{{ $unclaimedPackages[0]->title }}</option>
                    </select>
                @else
                    <select name="package_id[]" @include('crud::inc.field_attributes')>
                        <option value="">Select package</option>
                        @foreach ($unclaimedPackages as $package)
                        <option value="{{ $package->pivot->id }}"{{ old('package_id.0') == $package->pivot->id ? ' selected' : '' }}>{{ $package->title }}</option>
                        @endforeach
                    </select>
                @endif
            @endif
            </div>

            <div @include('crud::inc.field_wrapper_attributes') >
                <label>Validation code</label>
                <input type="text" name="code" value="{{ old('code') ? old('code') : '' }}" @include('crud::inc.field_attributes')>
                <p class="help-block">Code from the voucher</p>
            </div>
            <div @include('crud::inc.field_wrapper_attributes') >
                <label>Card number</label>
                <input type="text" name="card_number" value="{{ old('card_number') ? old('card_number') : '' }}" @include('crud::inc.field_attributes')>
                <p class="help-block">New card number</p>
            </div>
        </div>
        <div class="box-footer">
            <div id="saveActions" class="form-group">
            <button type="submit" class="btn btn-success">
                <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
                Claim
            </button>
            <a href="{{ url($crud->route) }}" class="btn btn-default"><span class="fa fa-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a>
        </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection
