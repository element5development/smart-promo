@prepend('crud_fields_scripts')
<script>
  $(function() {
    $("#form_tabs a[href='#tab_notificationemail']").parent()
        .addClass('promotion-type-specific_date promotion-type-birthday');
    $("#form_tabs a[href='#tab_notificationsms']").parent()
        .addClass('promotion-type-specific_date promotion-type-birthday');
  });
</script>
@endprepend
