<table class="table table-striped table-hover" style="{{ isset($style) ? $style : null}}">
    <thead>
        <tr><th>Card number</th><th>Packages</th></tr>
    </thead>
    <tbody>
        @foreach ($entry->distinctCards as $card)
            <tr><th colspav="2">{{ $card->number }}</th></tr>
            @foreach ($card->packages()->whereVoucherId($entry->id)->get() as $package)
                <tr><td>{{ $package->title }}</td><td>{{ $package->pivot->qty }}</td></tr>
            @endforeach
        @endforeach
    </tbody>
</table>