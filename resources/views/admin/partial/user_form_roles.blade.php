<i>Operator</i> has access to vouchers. <i>Superadmin</i> has access to everything.
@if($field['is_maintainer'])
    <i>Maintainer</i> has access to users, settings and logs.
@endif
