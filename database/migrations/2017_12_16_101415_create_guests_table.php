<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestsTable extends Migration
{
    public function up()
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->nullable()->unsigned()->index();
            $table->string('guest_id');
            $table->string('holder_title')->nullable();
            $table->string('holder_firstname')->nullable();
            $table->string('holder_surname')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('address3')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable()->index();
            $table->date('dob')->nullable();
            $table->datetime('registered_date')->nullable();
            $table->datetime('member_since')->nullable();
            $table->datetime('last_modified')->nullable();
            $table->string('verification_token')->nullable();
            $table->timestamp('optin_sent_at')->nullable();
            $table->timestamps();

            $table->unique(['location_id', 'guest_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('guests');
    }
}
