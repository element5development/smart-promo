<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned()->index();
            $table->string('transactionable_type');
            $table->integer('transactionable_id');
            $table->decimal('amount', 8, 2);
            $table->string('transaction_id');
            $table->string('account_type');
            $table->string('cc_number');
            $table->text('response_data');
            $table->timestamps();
            $table->index(['transactionable_type', 'transactionable_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
