<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVouchersTable extends Migration
{
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsingned()->index();
            $table->string('number');
            $table->string('code');
            $table->integer('location_id')->unsingned()->nullable()->index();
            $table->integer('qty')->unsigned();
            $table->integer('transaction_id')->nullable()->index();
            $table->boolean('is_claimed')->nullable()->default(false);
            $table->integer('claimer_user_id')->nullable()->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
