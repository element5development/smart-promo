<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->integer('user_order')->unsigned()->nullable();
            $table->boolean('is_archived')->default(false);
            $table->string('number');
            $table->string('barcode');
            $table->timestamp('balance_retrieved_at')->nullable();
            $table->decimal('balance_play_total_currency', 8, 2)->nullable();
            $table->decimal('balance_play_total_points', 8, 2)->nullable();
            $table->decimal('balance_play_value_currency', 8, 2)->nullable();
            $table->decimal('balance_play_value_points', 8, 2)->nullable();
            $table->decimal('balance_play_bonus_currency', 8, 2)->nullable();
            $table->decimal('balance_play_bonus_points', 8, 2)->nullable();
            $table->integer('tickets')->nullable();
            $table->integer('location_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
