<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPromotionsTable extends Migration
{
    public function up()
    {
        Schema::create('user_promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('promotion_id')->unsigned()->index();
            $table->boolean('is_preliminary')->default(false);
            $table->integer('card_id')->unsigned()->index();
            $table->timestamp('promoted_at')->nullable();
            $table->decimal('promoted_balance', 8, 2)->nullable();
            $table->timestamp('expire_at')->nullable();
            $table->boolean('is_expired')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_promotions');
    }
}
