<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageProductTable extends Migration
{
    public function up()
    {
        Schema::create('package_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();

            $table->unique(['package_id', 'product_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('package_product');
    }
}
