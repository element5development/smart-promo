<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingSourcesTable extends Migration
{
    public function up()
    {
        Schema::create('tracking_sources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('source')->unique();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tracking_sources');
    }
}
