<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapabilitySettingsTable extends Migration
{
    public function up()
    {
        Schema::create('capability_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('capability_id')->unsigned()->index();
            $table->integer('setting_id')->unsigned()->index();

            $table->unique(['capability_id', 'setting_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('capability_settings');
    }
}
