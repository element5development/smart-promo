<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThumbprintsTable extends Migration
{
    public function up()
    {
        Schema::create('thumbprints', function (Blueprint $table) {
            $table->charset = 'ascii';
            $table->collation = 'ascii_bin';
            $table->increments('id');
            $table->string('uri', 96);
            $table->string('key', 64);
            $table->string('secret', 64);
            $table->string('thumbprint', 32);
            $table->datetime('reserved_at')->nullable();
            $table->integer('use_count')->default(0);
            $table->timestamps();

            $table->unique(['uri', 'key', 'secret', 'thumbprint']);
            $table->index(['uri', 'key', 'secret']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('thumbprints');
    }
}
