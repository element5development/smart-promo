<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('type');
            $table->text('type_options');
            $table->integer('product_id')->unsigned()->index();
            $table->date('start_at')->nullable();
            $table->date('expire_at')->nullable();
            $table->boolean('is_enabled')->default(false);
            $table->string('valid_for_range');
            $table->integer('valid_for');
            $table->string('preliminary_email_subject')->nullable();
            $table->text('preliminary_email_template')->nullable();
            $table->string('promoted_email_subject')->nullable();
            $table->text('promoted_email_template')->nullable();
            $table->text('preliminary_sms_template')->nullable();
            $table->text('promoted_sms_template')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
