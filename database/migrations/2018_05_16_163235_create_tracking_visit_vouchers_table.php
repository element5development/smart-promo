<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingVisitVouchersTable extends Migration
{
    public function up()
    {
        Schema::create('tracking_visit_vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tracking_visit_id')->unsigned()->index();
            $table->integer('voucher_id')->unsigned()->index();

            $table->unique(['tracking_visit_id', 'voucher_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('tracking_visit_vouchers');
    }
}
