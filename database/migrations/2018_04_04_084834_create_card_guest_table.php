<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardGuestTable extends Migration
{
    public function up()
    {
        Schema::create('card_guest', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('card_id')->unsigned()->index();
            $table->integer('guest_id')->unsigned()->index();

            $table->unique(['card_id', 'guest_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('card_guest');
    }
}
