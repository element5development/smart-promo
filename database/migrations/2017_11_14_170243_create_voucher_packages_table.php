<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherPackagesTable extends Migration
{
    public function up()
    {
        Schema::create('voucher_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('voucher_id')->unsigned()->index();
            $table->integer('package_id')->unsigned()->index();
            $table->integer('qty')->unsigned()->default(1);
            $table->integer('card_id')->unsigned()->nullable()->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('voucher_packages');
    }
}
