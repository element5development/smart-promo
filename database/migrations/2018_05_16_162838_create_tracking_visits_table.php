<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingVisitsTable extends Migration
{
    public function up()
    {
        Schema::create('tracking_visits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tracking_source_id')->unsigned()->index();
            $table->datetime('registered_at')->nullable()->index();
            $table->datetime('claimed_at')->nullable()->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tracking_visits');
    }
}
