<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned()->nullable()->index();
            $table->integer('product_id')->unsigned()->index();
            $table->string('product_name');
            $table->integer('product_type')->nullable();
            $table->decimal('cost', 8, 2);
            $table->decimal('play_bonus_added', 8, 2);
            $table->decimal('tax', 8, 2)->nullable();
            $table->timestamps();

            $table->unique(['location_id', 'product_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
