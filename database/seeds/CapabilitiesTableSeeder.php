<?php

use Illuminate\Database\Seeder;
use App\Capability;
use App\Setting;

class CapabilitiesTableSeeder extends Seeder
{
    public function run()
    {
        $this->createCapability(
            'promotions',
            'Promotions',
            'Promotion admin panel, promotion cron jobs.',
            [
                'visit_gap_between_days',
            ]
        );

        $this->createCapability(
            'new_user_promotion',
            'New user promotion',
            'New user bonus promotion component.',
            [
                'new_user_promotion_package_id',
            ]
        );

        $this->createCapability(
            'first_card_bonus',
            'First card bonus',
            'Bonus given to fisrt card.',
            [
                'first_bonus_product_id',
            ]
        );

        $this->createCapability(
            'opt_in',
            'Opt-in',
            'Opt-in email and registration support.',
            [
                'is_opt_in_email_enabled',
                'optin_email_subject',
                'optin_email_template',
                'email_optin_url',
            ]
        );

        $this->createCapability(
            'authentication',
            'Authentication',
            'Full authentication support, login and password recovery endpoints.',
            [
                'password_email_subject',
                'password_email_template',
                'email_password_url',
            ]
        );

        $this->createCapability('frontend', 'Frontend', 'Integrated VueJs based frontned.', [
            'head_template',
            'menu_template',
            'footer_template',
            'scripts_template',
            'shared.heading.background_image',
            'shared.heading.cards',
            'shared.heading.profile',
            'shared.heading.welcome',
            'shared.heading.register',
            'shared.heading.email_verification_verified',
            'shared.heading.email_verification_succeeded',
            'shared.heading.email_verification_failed',
            'shared.heading.password_reset',
            'shared.heading.reload',
            'shared.heading.optin',
            'shared.heading.buy_cards',
            'shared.heading.check_balance',
        ]);
        
        $this->createCapability('payments', 'Payments', 'Payment admin panel.');

        $this->createCapability('file_manager', 'File manager', 'File manager admin panel.');

        $this->createCapability('embed', 'Embed', 'Embed support.', [
            'import_from_date',
            'cards_imported_to_date',
            'guests_imported_to_date',
            'is_opt_in_email_enabled',
            'optin_email_subject',
            'optin_email_template',
            'cashier_name',
            'visit_gap_between_days',
            'is_opt_in_email_enabled',
            'optin_email_subject',
            'optin_email_template',
            'email_optin_url',
            'online_sale_product_prefix',
        ]);

        $this->createCapability('intercard', 'Intercard', 'Intercard support.', [
            'redeemed_barcodes_imported_to_date',
        ]);

        $this->createCapability('locations', 'Locations', 'Multiple location servers and their settings.');
    }

    private function createCapability($key, $name, $description, $settings = [])
    {
        $capability = Capability::firstOrNew(['key' => $key]);
        
        $capability->fill([
            'name' => $name,
            'description' => $description,
        ]);

        if (!$capability->exists) {
            $capability->is_enabled = true;
        }

        $capability->save();

        if (count($settings)) {
            $settingIds = Setting::whereIn('key', $settings)->pluck('id');
            $capability->settings()->sync($settingIds);
        }
    }
}
