<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminsTableSeeder extends Seeder
{
    public function run()
    {
        $admin = Admin::create([
            'name' => 'Admin',
            'email' => 'admin@test.com',
            'password' => Hash::make('admin'),
        ]);
        $admin->assignRole('superadmin');

        $operator = Admin::create([
            'name' => 'Operator',
            'email' => 'operator@test.com',
            'password' => Hash::make('operator'),
        ]);
        $operator->assignRole('operator');

        $maintainer = Admin::create([
            'name' => 'Maintainer',
            'email' => 'maintainer@test.com',
            'password' => Hash::make('maintainer'),
        ]);
        $maintainer->assignRole('maintainer');
    }
}
