<?php

use Illuminate\Database\Seeder;
use App\Location;
use App\Lib\Embed\EmbedWrapper;

class LocationsTableSeeder extends Seeder
{
    public function run()
    {
        $config = EmbedWrapper::getDefaultConfig();

        Location::create(array_merge($config, ['title' => 'Minnetonka']));
        Location::create(array_merge($config, ['title' => 'Oakdale']));
        Location::create(array_merge($config, ['title' => 'Shakopee Canterbury Park']));
    }
}
