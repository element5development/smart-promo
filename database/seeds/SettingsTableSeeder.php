<?php

use Illuminate\Database\Seeder;
use App\Setting;
use App\Product;
use App\Package;
use App\Http\Requests\Admin\PromotionUpdateCrudRequest;

class SettingsTableSeeder extends Seeder
{
    public function run()
    {
        $this->createSetting(
            'import_from_date',
            'Import from date',
            'Date from which to import cards and guests. This date is used once at the first import. Import process will start only if this date is set.',
            null,
            array_merge($this->getSimpleField('date'), ['attributes' => ['readonly' => true, 'disabled' => true]])
        );

        $this->createSetting(
            'cards_imported_to_date',
            'Cards imported to date',
            'Date till which cards are already imported. This date is set by import process and should not be manually changed.',
            null,
            array_merge($this->getSimpleField('datetime'), ['attributes' => ['readonly' => true, 'disabled' => true]])
        );

        $this->createSetting(
            'guests_imported_to_date',
            'Guests imported to date',
            'Date till which guests are already imported. This date is set by import process and should not be manually changed.',
            null,
            array_merge($this->getSimpleField('datetime'), ['attributes' => ['readonly' => true, 'disabled' => true]])
        );

        $this->createSetting(
            'redeemed_barcodes_imported_to_date',
            'Redeemed barcodes imported to date',
            'Date till which redeemed barcodes are already imported. This date is set by import process and should not be manually changed.',
            null,
            array_merge($this->getSimpleField('datetime'), ['attributes' => ['readonly' => true, 'disabled' => true]])
        );

        $this->createSetting(
            'visit_gap_between_days',
            'Visit gap between days',
            'Gap in hours between visits on two days. Used while importing visits from Embed.',
            6,
            $this->getSimpleField('number')
        );

        $this->createSetting(
            'first_bonus_product_id',
            'First bonus product',
            'Product added to first card added to account (added existing card by user or added by operator during claim).',
            null,
            [
                'label' => 'Value',
                'type' => 'select2_model',
                'name' => 'value',
                'model' => Product::class,
                'scope' => 'noLocation',
                'attribute' => 'product_name',
            ]
        );

        $this->createSetting(
            'new_user_promotion_package_id',
            'New user promotion package',
            'Package added on the card given to new user registration (user retrieves voucher during promotional registration and exchanges it to card with free bonus on it).',
            null,
            [
                'label' => 'Value',
                'type' => 'select2_model',
                'name' => 'value',
                'model' => Package::class,
                'scope' => 'enabled',
                'attribute' => 'title',
            ]
        );

        $this->createSetting(
            'is_opt_in_email_enabled',
            'Opt-in email enabled',
            'Is opt-in emails sending enabled?',
            false,
            ['label' => 'Enabled', 'type' => 'checkbox', 'name' => 'value']
        );

        $this->createEmailSettings('verification', 'verification', 'email.verification', ['verification_url', 'first_name'], true);
        $this->createEmailSettings('password', 'password', 'email.password', ['reset_url'], true);
        $this->createEmailSettings('optin', 'opt-in', 'email.optin', ['optin_url'], true);
        $this->createEmailSettings('voucher', 'voucher', 'email.voucher', ['voucher_number', 'voucher_code', 'voucher_pdf', 'amount', 'first_name']);

        $this->createSetting(
            "head_template",
            'Head template',
            'Template used for styles and scripts placed into HEAD tag.',
            file_get_contents(view('custom.head')->getPath()),
            $this->getSimpleField('textarea')
        );

        $this->createSetting(
            "menu_template",
            'Menu template',
            'Template used for menu.',
            file_get_contents(view('custom.menu')->getPath()),
            $this->getSimpleField('textarea')
        );

        $this->createSetting(
            "footer_template",
            'Footer template',
            'Template used for footer.',
            file_get_contents(view('custom.footer')->getPath()),
            $this->getSimpleField('textarea')
        );

        $this->createSetting(
            "scripts_template",
            'Scripts template',
            'Template used for scripts placed at the bottom of the page.',
            file_get_contents(view('custom.scripts')->getPath()),
            $this->getSimpleField('textarea')
        );

        $this->createSetting(
            "admin.head",
            'Admin panel head HTML',
            'HTML included at the very top of the HEAD tag.',
            null,
            $this->getSimpleField('textarea')
        );

        $this->createSetting(
            "admin_styles",
            'Admin panel CSS styles',
            'CSS styles included as last style block in the head of every admin panel page.',
            null,
            $this->getSimpleField('textarea')
        );

        $this->createSetting(
            'shared.heading.background_image',
            'Heading background image',
            'Large image used under heading.',
            '/images/covers/BTFCover.jpg',
            $this->getSimpleField('text')
        );

        $headings = [
            'cards' => 'My Swipe Cards',
            'profile' => 'Edit profile',
            'welcome' => 'Sign In / Check Balance',
            'register' => 'Register your account',
            'email_verification_verified' => '',
            'email_verification_succeeded' => '',
            'email_verification_failed' => '',
            'password_reset' => 'Reset password',
            'reload' => 'Reload swipe card',
            'optin' => 'Register your account',
            'buy_cards' => 'Purchase Swipe Cards',
            'check_balance' => 'Check balance',
        ];
        foreach ($headings as $heading => $text) {
            $this->createSetting(
                "shared.heading.{$heading}",
                ucfirst(str_replace('_', ' ', $heading)) . ' page title',
                'Large title used on ' . str_replace('_', ' ', $heading) . ' page.',
                $text,
                $this->getSimpleField('text')
            );
        }

        $this->createSetting(
            'cashier_name',
            'Cashier name',
            'Cashier name used in Embed\'s card balance reversal requests.',
            'Test',
            $this->getSimpleField('text')
        );

        $this->createSetting(
            'admin.logo_lg',
            'Admin panel large logo',
            'HTML used in admin panel as large logo.',
            '<img src="/images/BigThrillFactoryLogo.png">',
            $this->getSimpleField('text')
        );

        $this->createSetting(
            'admin.logo_mini',
            'Admin panel small logo',
            'HTML used in admin panel as small logo.',
            '<img src="/images/BigThrillFactoryLogo.png">',
            $this->getSimpleField('text')
        );

        $this->createSetting(
            "email_layout_template",
            'Email layout template',
            'Layout template used for emails sent to users.',
            file_get_contents(view('email.layout')->getPath()),
            $this->getSimpleField('textarea')
        );

        $this->createSetting(
            "pdf_layout_template",
            'PDF layout template',
            'Layout template used for PDF.',
            file_get_contents(view('pdf.layout')->getPath()),
            $this->getSimpleField('textarea')
        );

        $this->createSetting(
            "voucher_pdf_template",
            'Voucher PDF template',
            'Template used for voucher PDF.',
            file_get_contents(view('email.voucher')->getPath()),
            $this->getSimpleField('textarea')
        );

        $this->createSetting(
            'online_sale_product_prefix',
            'Online sale product prefix',
            'Prefix used in ther name of product used in online sales and promotions.',
            '[!snap-online!]',
            $this->getSimpleField('text')
        );
    }

    private function createSetting($key, $name, $description, $value, $field)
    {
        if (!Setting::where('key', $key)->exists()) {
            $this->command->info("Creating setting {$key}");
            Setting::create([
                'key' => $key,
                'name' => $name,
                'description' => $description,
                'value' => $value,
                'field' => json_encode($field),
                'active' => true
            ]);
        }
    }

    private function getSimpleField($type)
    {
        return [
            'label' => 'Value',
            'type' => $type,
            'name' => 'value',
        ];
    }

    private function createEmailSettings($tag, $title, $view, $placeholderItems = [], $urlSetting = false)
    {
        $this->createSetting(
            "{$tag}_email_subject",
            ucfirst($title) . ' email subject',
            'Subject used for ' . strtolower($title) . ' email.',
            ucfirst($title) . ' email subject',
            $this->getSimpleField('text')
        );

        $this->createSetting(
            "{$tag}_email_template",
            ucfirst($title) . ' email template',
            'Template used for ' . strtolower($title) . ' email.',
            file_get_contents(view($view)->getPath()),
            array_merge($this->getSimpleField('ckeditor_min'), ['placeholder_items' => $placeholderItems])
        );

        if ($urlSetting) {
            $this->createSetting(
                "email_{$tag}_url",
                ucfirst($title) . ' page URL',
                'Custom URL for ' . strtolower($title) . ' page.',
                '',
                $this->getSimpleField('url')
            );
        }
    }
}
