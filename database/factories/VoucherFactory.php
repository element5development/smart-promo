<?php

use Faker\Generator as Faker;
use App\Voucher;
use Illuminate\Support\Facades\Hash;

$factory->define(Voucher::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'number' => $faker->randomNumber(8),
        'code' => Hash::make('test'),
        'location_id' => 0,
        'qty' => $faker->numberBetween(1, 2),
        'address' => $faker->address,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'transaction_id' => str_random(20),
    ];
});
