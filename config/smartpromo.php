<?php

return [
    'location_at_purchase' => env('LOCATION_AT_PURCHASE', false),
    'claim_multiple_packages' => env('CLAIM_MULTIPLE_PACKAGES', false),
    'register_with_purchase' => env('REGISTER_WITH_PURCHASE', false),
    'web_enabled' => env('WEB_ENABLED', true),
    'encode_voucher_code' => env('ENCODE_VOUCHER_CODE', true),
    'verification_email_after_claim' => env('VERIFICATION_EMAIL_AFTER_CLAIM', false),
    'disable_admin_claim' => env('DISABLE_ADMIN_CLAIM', false),
    'enable_newsletter_subscribe' => env('ENABLE_NEWSLETTER_SUBSCRIBE', true),
];
