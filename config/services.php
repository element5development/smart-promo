<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'embed' => [
        'base_uri' => env('EMBED_BASE_URI'),
        'key' => env('EMBED_KEY'),
        'secret' => env('EMBED_SECRET'),
        'thumbprints' => explode(',', env('EMBED_THUMBPRINTS', 'ahSheL1Fae,la6Phohl9b')),
        'ssl_verify' => env('EMBED_SSL_VERIFY', true),
        'disable_cvv_check' => env('EMBED_DISABLE_CVV_CHECK', false),
        'timezone' => env('EMBED_TIMEZONE', 'Asia/Shanghai'),
    ],

    'intercard' => [
        'base_uri' => env('INTERCARD_BASE_URI'),
        'corpid' => env('INTERCARD_CORPID'),
        'cid' => env('INTERCARD_CID'),
        'ssl_verify' => env('INTERCARD_SSL_VERIFY', true),
    ],

    'rollbar' => [
        'access_token' => env('ROLLBAR_TOKEN', 'ab8daf203a3e49cfa3f1ea4b93e5a75b'),
        'level' => env('ROLLBAR_LEVEL', env('APP_ENV') == 'local' ? 'none' : null),
    ],

    'anet' => [
        'login_id' => env('ANET_LOGIN_ID', '3LdW4E3vge'),
        'transaction_key' => env('ANET_TRANSACTION_KEY', '3y37497w73xU7JZG'),
        'production' => env('ANET_PRODUCTION', false),
    ],

    'twilio' => [
        'auth_token' => env('TWILIO_AUTH_TOKEN', 'a0c266960e656007e7a3ff850152cd34'),
        'account_sid' => env('TWILIO_ACCOUNT_SID', 'AC0c67e126a6d4c8d61a6cf847ce9ac15b'),
        'from' => env('TWILIO_FROM', env('APP_NAME', 'SmartPromo')),
    ],

    'card_provider' => [
        'class' => env('CARD_PROVIDER_CLASS', App\Services\Cards\EmbedCardProvider::class),
    ],

];
