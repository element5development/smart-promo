<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => ['auth:api', 'isVerified']], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('user', 'UserController');

    Route::resource('user/card', 'Cards\UserCardController', ['only' => ['index']]);
    Route::post('user/card/add', 'Cards\UserCardController@add');
    Route::post('user/card/{id}/primary', 'Cards\UserCardController@primary');
    Route::post('user/card/{id}/archive', 'Cards\UserCardController@archive');

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('register/resend', 'Auth\RegisterController@resend');
    Route::post('register/shadow', 'Auth\RegisterController@shadow');
    Route::post('register/promotion', 'Auth\RegisterController@promotion');
    Route::post('register/promotion/resend', 'Auth\RegisterController@promotionResend');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('optin/guest/{token}', 'Auth\OptinController@guest');
    Route::post('optin', 'Auth\OptinController@optin');
});

Route::post('card/check/reload', 'Cards\CheckController@checkReload');
Route::post('card/check/balance', 'Cards\CheckController@checkBalance');
Route::post('card/check/register', 'Cards\CheckController@checkRegister');

Route::post('buy-cards/prevalidate', 'Cards\BuyVoucherController@prevalidate');
Route::post('buy-cards', 'Cards\BuyVoucherController@submit');
Route::post('buy-cards/resend', 'Cards\BuyVoucherController@resend');

Route::post('card/{barcode}/reload', 'Cards\ReloadCardController@reload');

Route::resource('package', 'PackageController', ['only' => ['index', 'show']]);

Route::get('product/first-bonus', 'ProductController@firstBonus');

Route::resource('location', 'LocationController', ['only' => ['index']]);

Route::get('setting', 'SettingController');

Route::get('email-verification/{token}', 'Auth\RegisterController@emailVerification');

Route::post('tracking/visit', 'TrackingController@visit');

Route::post('unsubscribe', 'Settings\ProfileController@unsubscribe');
