<?php

Route::group(['prefix' => 'widget'], function () {
    Route::get('sign-in-out-link.js', 'AuthWidgetController@signInOutLink');
    Route::get('auth-class-switch.js', 'AuthWidgetController@authClassSwitch');
    Route::get('logout', 'AuthWidgetController@logout');
});

Route::get('js/config.js', 'ConfigController@js')->middleware('browserCache');
