<?php

Route::group([
    'middleware' => ['web', 'admin'],
    'namespace' => 'App\Http\Controllers\Admin',
    'prefix' => config('backpack.base.route_prefix')
], function () {
    Route::get('dashboard', 'AdminController@dashboard')->name('backpack.dashboard');
    Route::get('/', 'AdminController@redirect')->name('backpack');
    
    Route::group(['middleware' => 'anyrole:operator,superadmin'], function () {
        CRUD::resource('voucher', 'VoucherCrudController', ['except' => ['store', 'create', 'destroy', 'update', 'edit']]);
        if (!config('smartpromo.disable_admin_claim')) {
            Route::get('voucher/{voucher}/claim', 'VoucherCrudController@claimForm');
            Route::post('voucher/{voucher}/claim', 'VoucherCrudController@claimPost');
        }
    });

    Route::group(['middleware' => 'anyrole:superadmin'], function () {
        CRUD::resource('member', 'MemberCrudController', ['except' => ['store', 'create', 'destroy']]);
        Route::get('member/{user}/check-balance', 'MemberCrudController@check_balance');

        if (config('capabilities.payments')) {
            CRUD::resource('transaction', 'TransactionCrudController', ['except' => ['store', 'create', 'destroy', 'update', 'edit']]);
        }
    
        Route::get('card/ajax-user-options', 'CardCrudController@userOptions')->name('admin.card.ajax-user-options');
        CRUD::resource('card', 'CardCrudController', ['except' => ['store', 'create', 'destroy', 'update', 'edit']]);

        Route::post('package/enable-disable', 'PackageCrudController@enableDisable');
        CRUD::resource('package', 'PackageCrudController');
        
        if (config('capabilities.locations')) {
            CRUD::resource('location', 'LocationCrudController');
        }
    
        if (config('capabilities.promotions')) {
            CRUD::resource('promotion', 'PromotionCrudController');
            Route::get('promotion/{promotion}/run', 'PromotionCrudController@run');
            Route::get('promotion/{promotion}/test', 'PromotionCrudController@testForm');
            Route::post('promotion/{promotion}/test', 'PromotionCrudController@testPost');
            Route::post('promotion/enable-disable', 'PromotionCrudController@enableDisable');
        }
    });

    Route::group(['middleware' => 'anyrole:maintainer'], function () {
        if (config('capabilities.embed')) {
            CRUD::resource('thumbprint', 'ThumbprintCrudController', ['except' => ['store', 'create', 'destroy', 'update', 'edit']]);
        }

        Route::post('capability/enable-disable', 'CapabilityCrudController@enableDisable');
        CRUD::resource('capability', 'CapabilityCrudController', ['only' => ['index']]);

        CRUD::resource('tracking-summary', 'TrackingSummaryCrudController', ['only' => ['index']]);
        CRUD::resource('tracking-visit', 'TrackingVisitCrudController', ['only' => ['index']]);
    });
});
