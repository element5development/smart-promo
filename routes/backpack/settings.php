<?php

/*
|--------------------------------------------------------------------------
| Backpack\Settings Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are
| handled by the Backpack\Settings package.
|
*/

Route::group([
    'middleware' => ['web', 'admin', 'anyrole:superadmin,maintainer'],
    'prefix' => config('backpack.base.route_prefix'),
    'namespace' => 'App\Http\Controllers\Admin'
], function () {
    CRUD::resource('setting', 'SettingCrudController');
});
