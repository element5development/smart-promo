# smart-promo

Game Point backend, admin panel, WP plugin backend and customer web-site.

## Architecture

* [Laravel PHP Framework 5.6](http://laravel.com/docs) for backend.
* [VueJS](https://vuejs.org/) for client-side.
* [ECMAScript 6 via Babel](https://babeljs.io/docs/learn-es2015/) that compiles into JavaScript.
* [Pug (former Jade) template engine](http://jade-lang.com/).

## Environment prerequisites

Install following tools/libs:
 * git - https://git-scm.com/
 * composer - https://getcomposer.org/download/
 * node - https://nodejs.org/ >= 4.0.0 (`$ node -v` to check version)
 * npm - https://nodejs.org/en/ >= 3.0.0 (`$ npm -v` to check version, `$ sudo npm i npm -g` to update NPM)
 * PHP 7 with `mbstring`, `curl` and `dom` extensions.

Cheeck if `git`, `composer`, `node`, `npm` commands are available.

Laravel requirements are described at https://laravel.com/docs/5.6/installation .

## Development

### Preparation

1. Create database.
2. Clone repository and get inside.
3. Create `.env` based on `.env.example` file and change at least database settings.
4. `$ composer install`
5. `$ npm install`
6. Generate keys with run `$ php artisan key:generate`. 
7. Run `$ php artisan migrate` to create database tables.
8. Run `$ php artisan db:seed` to seed database with initial data.

### Running

There are two processes to be run together while in developing.

PHP Laravel backend:
`$ php artisan serve`

After that you can open instance at `http://localhost:8000`.

Frontned build script:
`$ npm run watch`

Frontned build script is used to build assets under `resources/assets`, especially `js` and `sass` ones. `Watch` script will rebuild assets every time you make chanmges to source files.

## Deployment

Deployment is orgaznied using [Deployer](https://deployer.org/).

Deployment consists of two stages: `build` and `deploy`.

Deploy scripts are not tested on Windows operating system. They should be run on Linux or iOS.

### Build

Builds package with all necessary files to run application.

```
$ vendor/bin/dep build development
```

### Deploy

Deploys to remote server previously build package, upacks and confugures server storage.

```
$ vendor/bin/dep deploy development
```

## Error monitoring

[Rollbar](https://rollbar.com/greg-moreno/smart-promo/) is used to track errors.
