const path = require('path');
const webpack = require('webpack');
const mix = require('laravel-mix');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.autoload({});

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/wp-plugin.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/wp-plugin.scss', 'public/css')
    .sass('resources/assets/sass/admin.scss', 'public/css')
    .sourceMaps()
    .disableSuccessNotifications();

if (mix.inProduction()) {
    mix.version()
    
    mix.extract([
        'vue',
        'vform',
        'axios',
        'vuex',
        'vue-i18n',
        'vue-meta',
        'js-cookie',
        'bootstrap',
        'vue-router',
        'vuex-router-sync',
        'moment',
        'rollbar',
        'vue-select',
        'vuejs-datepicker'
    ]);
}

mix.webpackConfig({
    resolve: {
        alias: {
            '~': path.join(__dirname, './resources/assets/js')
        }
    },
    plugins: [
        new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en/),
        // new BundleAnalyzerPlugin({
        //     generateStatsFile: true
        // })
    ]
});
